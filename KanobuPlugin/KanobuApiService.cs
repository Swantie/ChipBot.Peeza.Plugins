﻿using ChipBot.Core;
using ChipBot.Core.Services;
using KanobuPlugin.Models;
using RestEase;
using System;
using System.Net.Http;

namespace KanobuPlugin
{
    public class KanobuApiService : ApiService<IKanobuApi>
    {
        private const string KanobuUrl = "https://kanobu.ru";
        private const string ApiUrn = "/api/v1/";
        private const string KanobuUri = KanobuUrl + ApiUrn;

        public string KanobuBaseUrl => KanobuUrl;

        public KanobuApiService(LoggingService log, ConfigurationProvider provider) : base(log, provider)
        {
            ApiClient = new RestClient(
                new HttpClient(
                    new HttpClientHandler()
                    {
                        AutomaticDecompression = System.Net.DecompressionMethods.GZip
                    })
                {
                    BaseAddress = new Uri(KanobuUri)
                }).For<IKanobuApi>();

            ApiClient.UserAgent = UserAgent;
        }
    }
}
