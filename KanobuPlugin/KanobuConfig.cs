﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KanobuPlugin
{
    public class KanobuConfig
    {
        [JsonProperty("storeLastCount")]
        public byte StoreLastCount { get; set; } = 15;

        [JsonProperty("subList")]
        public Dictionary<ulong, KanobuSubscription> SubList { get; set; }
    }

    public class KanobuSubscription
    {
        [JsonProperty("isSubscribed")]
        public bool IsSubscribed { get; set; }

        [JsonProperty("isTextChannel")]
        public bool IsTextChannel { get; set; }

        [JsonProperty("id")]
        public ulong Id { get; set; }
    }
}
