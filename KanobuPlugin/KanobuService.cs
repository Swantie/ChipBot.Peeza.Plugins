﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanobuPlugin
{
    public class KanobuService : BackgroundSaveableService<List<long>>, IConfigurableService<KanobuConfig>
    {
        private readonly KanobuApiService _kanobuApi;
        private readonly ClockService _clockService;
        private readonly DiscordSocketClient _discord;

        private bool _isWorking = false;
        public override bool IsWorking => _isWorking;

        public ServiceConfigurationProvider<KanobuConfig> ServiceConfigProvider { get; set; }

        public KanobuService(LoggingService log, ConfigurationProvider provider, KanobuApiService api, ClockService clock, DiscordSocketClient discord) : base(log, provider, "Kanobu", "Read.json")
        {
            ServiceConfigProvider = ServiceConfigurationProvider<KanobuConfig>.GetInstance(provider);
            _kanobuApi = api;
            _clockService = clock;
            _discord = discord;
            Data = Load().GetAwaiter().GetResult() ?? new List<long>();
        }

        private async Task GetNewsList()
        {
            var news = (await _kanobuApi.ApiClient.GetNews()).News;

            foreach (var article in news)
            {
                if (Data.Contains(article.Id)) break;
                Data.Add(article.Id);
                if (Data.Count > ServiceConfigProvider.Config.StoreLastCount) Data.RemoveAt(ServiceConfigProvider.Config.StoreLastCount);

                var extended = await _kanobuApi.ApiClient.GetNews(article.Id);
                var subs = new List<KanobuSubscription>(ServiceConfigProvider.Config.SubList.Values);
                var shouldSendTo = new List<KanobuSubscription>();

                foreach (var sub in subs)
                {
                    if (!sub.IsSubscribed) continue;
                    if (true) // pass filter
                    {
                        shouldSendTo.Add(sub);
                    }
                }

                if (!shouldSendTo.Any()) continue;

                var eb = new EmbedBuilder();
                eb.WithAuthor("Kanobu", "https://kanobu.ru/images/0d91381ee3fba214f904bc37320b9dfd.png", _kanobuApi.KanobuBaseUrl)
                    .WithTitle(extended.Title).WithUrl($"{_kanobuApi.KanobuBaseUrl}/news/{extended.Id}");
                // build embed if needed

                // send if needed


            }
        }

        public async override Task StartAsync()
        {
            _isWorking = true;
            _clockService.TenMinutesTick += GetNewsList;
            await GetNewsList();
        }

        public override Task StopAsync()
        {
            _isWorking = false;
            _clockService.TenMinutesTick -= GetNewsList;
            return Task.CompletedTask;
        }
    }
}
