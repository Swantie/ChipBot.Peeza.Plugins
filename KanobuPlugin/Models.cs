﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestEase;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace KanobuPlugin.Models
{
    [Header("Accept-Encoding", "gzip")]
    public interface IKanobuApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("news/")]
        Task<NewsList> GetNews(string to = "", uint limit = 10, string verticals = "");
        [Get("news/{id}/")]
        Task<ExtendedNews> GetNews([Path]long id);
    }

    public class NewsList
    {
        [JsonProperty("results")]
        public List<News> News { get; set; }
    }

    public class News
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("pubdate")]
        public DateTimeOffset PublicationDate { get; set; }

        [JsonProperty("pic")]
        public Uri Pic { get; set; }

        [JsonProperty("thumb")]
        public Uri Thumb { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("comment_count")]
        public long CommentCount { get; set; }

        [JsonProperty("like_count")]
        public long LikeCount { get; set; }

        [JsonProperty("vertical")]
        public Tag Vertical { get; set; }

        [JsonProperty("tag")]
        public Tag Tag { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("links")]
        public Links Links { get; set; }

        [JsonProperty("top_like_type")]
        public TopLikeType TopLikeType { get; set; }

        [JsonProperty("partner_stamp")]
        public PartnerStamp PartnerStamp { get; set; }
    }

    public class Links
    {
        [JsonProperty("detail")]
        public Uri Detail { get; set; }
    }

    public class PartnerStamp
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("colour")]
        public string Colour { get; set; }

        [JsonProperty("link")]
        public Uri Link { get; set; }

        [JsonProperty("logo")]
        public Uri Logo { get; set; }
    }

    public class Tag
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("links")]
        public Links Links { get; set; }
    }

    public class TopLikeType
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("icon")]
        public object Icon { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }
    }

    public partial class ExtendedNews : News
    {
        [JsonProperty("verticals")]
        public List<Tag> Verticals { get; set; }

        [JsonProperty("tags")]
        public List<Tag> Tags { get; set; }

        [JsonProperty("content")]
        public Content Content { get; set; }

        [JsonProperty("source_title")]
        public string SourceTitle { get; set; }

        [JsonProperty("source_link")]
        public string SourceLink { get; set; }

        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("author")]
        public Author Author { get; set; }

        [JsonProperty("modified")]
        public DateTimeOffset Modified { get; set; }

        [JsonProperty("links")]
        public new ExtendedNewsLinks Links { get; set; }

        [JsonProperty("games")]
        public List<Game> Games { get; set; }

        [JsonProperty("is_comments_disabled")]
        public bool IsCommentsDisabled { get; set; }

        [JsonProperty("comments_channel_name")]
        public string CommentsChannelName { get; set; }
    }

    public partial class Author
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("photo")]
        public Uri Photo { get; set; }

        [JsonProperty("links")]
        public AuthorLinks Links { get; set; }
    }

    public partial class AuthorLinks
    {
        [JsonProperty("detail")]
        public Uri Detail { get; set; }

        [JsonProperty("profile_link")]
        public string ProfileLink { get; set; }
    }

    public partial class Content
    {
        [JsonProperty("data")]
        public List<Datum> Data { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("type")]
        public TypeEnum Type { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("file")]
        public File File { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("source")]
        public Source? Source { get; set; }

        [JsonProperty("remote_id")]
        public string RemoteId { get; set; }

        [JsonProperty("custom_size")]
        public bool? CustomSize { get; set; }

        [JsonProperty("caption")]
        public string Caption { get; set; }

        [JsonProperty("images")]
        public List<Uri> DataImages { get; set; }

        [JsonProperty("_images")]
        public List<Uri> Images { get; set; }
    }

    public partial class File
    {
        [JsonProperty("url")]
        public Uri FileUrl { get; set; }

        [JsonProperty("_url")]
        public Uri Url { get; set; }
    }

    public partial class Game
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("links")]
        public Links Links { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("editorial_rating")]
        public double? EditorialRating { get; set; }

        [JsonProperty("user_rating")]
        public double UserRating { get; set; }

        [JsonProperty("cover")]
        public Uri Cover { get; set; }
    }

    public partial class ExtendedNewsLinks
    {
        [JsonProperty("comments")]
        public Uri Comments { get; set; }

        [JsonProperty("likes")]
        public Uri Likes { get; set; }
    }

    public partial class Metadata
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("opengraph")]
        public Opengraph Opengraph { get; set; }
    }

    public partial class Opengraph
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("image-resized")]
        public Uri ImageResized { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }

    public enum Source { Twitter, Unknown };

    public enum TypeEnum { Embed, Gallery, Image, Text, Unknown };

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                SourceConverter.Singleton,
                TypeEnumConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class SourceConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Source) || t == typeof(Source?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "twitter")
            {
                return Source.Twitter;
            }
            return Source.Unknown;
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue != null)
            {
                var value = (Source)untypedValue;
                if (value == Source.Twitter)
                {
                    serializer.Serialize(writer, "twitter");
                    return;
                }
            }

            serializer.Serialize(writer, null);
        }

        public static readonly SourceConverter Singleton = new SourceConverter();
    }

    internal class TypeEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(TypeEnum) || t == typeof(TypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "embed":
                    return TypeEnum.Embed;
                case "gallery":
                    return TypeEnum.Gallery;
                case "image":
                    return TypeEnum.Image;
                case "text":
                    return TypeEnum.Text;
                default:
                    return TypeEnum.Unknown;
            }
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue != null)
            {
                var value = (TypeEnum)untypedValue;
                switch (value)
                {
                    case TypeEnum.Embed:
                        serializer.Serialize(writer, "embed");
                        return;
                    case TypeEnum.Gallery:
                        serializer.Serialize(writer, "gallery");
                        return;
                    case TypeEnum.Image:
                        serializer.Serialize(writer, "image");
                        return;
                    case TypeEnum.Text:
                        serializer.Serialize(writer, "text");
                        return;
                }
            }
            serializer.Serialize(writer, null);
        }

        public static readonly TypeEnumConverter Singleton = new TypeEnumConverter();
    }
}
