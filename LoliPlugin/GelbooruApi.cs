﻿using ChipBot.Core;
using ChipBot.Core.Services;
using RestEase;
using System;
using System.Net;
using System.Net.Http;

namespace LoliPlugin
{
    public class GelbooruApi : ApiService<IGelbooruApi>
    {
        private const string ApiUrl = "https://gelbooru.com/";

        public GelbooruApi(LoggingService log, ConfigurationProvider provider, ProxyService proxy) : base(log, provider)
        {
            var handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip
            };

            var pr = proxy.GetProxy();
            if (pr != null)
            {
                handler.Proxy = pr;
            }

            ApiClient = new RestClient(new HttpClient(handler)
            {
                BaseAddress = new Uri(ApiUrl)
            }).For<IGelbooruApi>();

            ApiClient.UserAgent = UserAgent;
        }
    }
}
