﻿using Newtonsoft.Json;
using RestEase;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoliPlugin
{
    [Header("Accept-Encoding", "gzip")]
    public interface IGelbooruApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("index.php?page=dapi&s=post&json=1&q=index")]
        Task<List<LoliImage>> GetImages(int limit = 100, string tags = "loli");
    }

    public class LoliImage
    {
        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("directory")]
        public string Directory { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("change")]
        public long Change { get; set; }

        [JsonProperty("owner")]
        public string Owner { get; set; }

        [JsonProperty("parent_id")]
        public object ParentId { get; set; }

        [JsonProperty("rating")]
        public string Rating { get; set; }

        [JsonProperty("sample")]
        public bool Sample { get; set; }

        [JsonProperty("sample_height")]
        public long SampleHeight { get; set; }

        [JsonProperty("sample_width")]
        public long SampleWidth { get; set; }

        [JsonProperty("score")]
        public long Score { get; set; }

        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }

        [JsonProperty("file_url")]
        public Uri FileUrl { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
    }
}
