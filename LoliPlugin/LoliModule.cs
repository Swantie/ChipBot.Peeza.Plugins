﻿using ChipBot.Core;
using ChipBot.Core.Preconditions;
using Discord;
using Discord.Commands;
using PointsPlugin.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LoliPlugin
{
    [Name("Loli")]
    [RequireChannel]
    public class LoliModule : ChipModuleBase
    {
        public GelbooruApi Api { get; set; }
        public PointsService Points { get; set; }
        public Random Rng { get; set; }

        [Command("loli"), Summary("Spend 250 points to see cute picture.")]
        public async Task TwoD([Remainder, Summary("(Optional) Tags e.g. loli brown_hair bunny_ears")] string tags = "loli")
        {
            if (!Points.HasEnough(Context.User.Id, 250))
            {
                await ReplyAsync($"{Context.User.Mention} you don't have enough points.");
                return;
            }

            try
            {
                var response = await Api.ApiClient.GetImages(100, tags);
                if (!response.Any())
                {
                    await ReplyAsync($"{Context.User.Mention} no images was found with your tags. Points were returned.");
                    return;
                }

                Points.ChangePoints(Context.User.Id, -250);

                var image = response.ElementAt(Rng.Next(response.Count - 1));

                var eb = new EmbedBuilder().WithAuthor("Gelbooru", "https://gelbooru.com/layout/gcomLogo.png", "https://gelbooru.com/")
                    .WithColor(50, 141, 254).WithImageUrl(image.FileUrl.ToString())
                    .WithDescription($"Post Id: `{image.Id}`\nUploader: `{image.Owner}`").WithUrl($"https://gelbooru.com/index.php?page=post&s=view&id={image.Id}");

                var textChannel = await Context.User.GetOrCreateDMChannelAsync();
                await textChannel.SendMessageAsync(embed: eb.Build());
            }
            catch (Exception ex)
            {
                Log(LogSeverity.Warning, "Couldn't fetch images from gelbooru: " + ex.Message);
                await ReplyAsync($"{Context.User.Mention} something went wrong.");
            }
        }
    }
}
