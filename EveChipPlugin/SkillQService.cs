﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using ESI.NET;
using EveApiPlugin.Services;
using Humanizer;

namespace EveChipPlugin.Services
{
    public class SkillQService : BackgroundSaveableService<List<ulong>>
    {
        private readonly EveApiService _eveApi;
        private readonly EveSsoService _ssoService;
        private readonly ClockService _clockService;
        private readonly DiscordSocketClient _discord;

        public EsiClient ESI => _eveApi.ApiClient;
        public List<string> Scopes => _eveApi.ServiceConfigProvider.Config.Scopes;

        private bool isWorking = false;
        public override bool IsWorking => isWorking;
        private bool notifiedToday = false;
        private int day = DateTime.Now.Day;

        public SkillQService(LoggingService log, ConfigurationProvider provider, EveApiService eveApi, EveSsoService ssoService, ClockService clockService, DiscordSocketClient discord) : base(log, provider, "Eve", "SkillQ.json")
        {
            _eveApi = eveApi;
            _ssoService = ssoService;
            _clockService = clockService;
            _discord = discord;
            Data = Load().GetAwaiter().GetResult() ?? new List<ulong>();
        }

        public async Task ChangeSubscription(ulong userId, bool subscribe)
        {
            if (subscribe && !Data.Contains(userId)) Data.Add(userId);
            if (!subscribe && Data.Contains(userId)) Data.Remove(userId);

            await Save();
        }

        public async override Task StartAsync()
        {
            isWorking = true;

            _clockService.HourTick += _clockService_HourTick;
            await _clockService_HourTick();
        }

        private async Task _clockService_HourTick()
        {
            if (notifiedToday && day == DateTime.Now.Day) return;
            if (DateTime.Now.Hour < 12) return;
            notifiedToday = true;
            day = DateTime.Now.Day;

            Log(LogSeverity.Verbose, "Starting queue check");

            var copy = new List<ulong>(Data);

            foreach (var user in copy)
            {
                Log(LogSeverity.Verbose, $"Gettting user's ({user}) characters");
                var chars = await _ssoService.GetUserData(user);
                if (chars == null || !chars.Any()) continue;

                await Task.Delay(1000);

                var embed = new EmbedBuilder().WithCurrentTimestamp().WithColor(Color.DarkBlue).WithTitle("EveSkillQ").WithThumbnailUrl("https://imageserver.eveonline.com/Corporation/1000213_128.png");
                var eveApiCalls = 0;

                foreach (var character in chars)
                {
                    _eveApi.ApiClient.SetCharacterData(character);

                    if (eveApiCalls > 2)
                    {
                        await Task.Delay(1000);
                        eveApiCalls = 0;
                    }

                    Log(LogSeverity.Verbose, $"Gettting user's ({user}) character's ({character.CharacterName}) skill queue");
                    var q = await _eveApi.ApiClient.Skills.Queue();
                    eveApiCalls++;

                    var field = new EmbedFieldBuilder().WithIsInline(true).WithName(character.CharacterName);

                    var skills = q.Data;

                    if (skills == null || !skills.Any())
                    {
                        field.Value = "No skills in queue!";
                        embed.AddField(field);
                        continue;
                    }

                    skills = skills.Where(s => s.FinishDate != null).ToList();

                    if (!skills.Any())
                    {
                        field.Value = "SP Limit. Plex your account.";
                        embed.AddField(field);
                        continue;
                    }

                    var skillsFinished = skills.Where(s => DateTime.Now > DateTime.Parse(s.FinishDate));
                    var endsAt = skills.Select(s => DateTime.Parse(s.FinishDate)).Max();

                    field.Value = $"`{skills.Count}` skill in queue - `{skillsFinished.Count()}` finished\n{(DateTime.Now > endsAt ? "Queue ended!" : $"Queue ends in {endsAt.Humanize()}")}";
                    embed.AddField(field);
                }

                await (await _discord.GetUser(user).GetOrCreateDMChannelAsync()).SendMessageAsync(embed: embed.Build());
            }
        }

        public override Task StopAsync()
        {
            isWorking = false;

            _clockService.HourTick -= _clockService_HourTick;

            return Task.CompletedTask;
        }
    }
}
