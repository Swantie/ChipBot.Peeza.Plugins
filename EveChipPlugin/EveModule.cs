﻿using ChipBot.Core;
using ChipBot.Core.Preconditions;
using Discord;
using Discord.Commands;
using ESI.NET.Models.Market;
using EveApiPlugin.Services;
using System.Linq;
using System.Threading.Tasks;
using zKillApiPlugin;
using zKillApiPlugin.Services;
using zKillWS_Plugin.Services;

namespace EveChipPlugin.Modules
{
    [Name("Eve")]
    [RequireChannel]
    [Group("eve")]
    [Alias("z")]
    public class EveModule : ChipModuleBase
    {
        public ZWSService Zkb { get; set; }
        public ZKillApiService ZkbApi { get; set; }
        public EveApiService EveApi { get; set; }
        public EveSsoService EveSso { get; set; }

        [Command]
        [Summary("Displays service status")]
        public async Task Default()
        {
            var response = await EveApi.ApiClient.Market.Prices();
            var tqStatus = Zkb.GetStatus();

            if (response.Data == null) Log(LogSeverity.Warning, $"Esi Api: statusCode {response.StatusCode}, message {response.Message}");

            var embed = GetEmbedBase();

            if (tqStatus != null) embed.AddField("🌐 Tranquility", $"Status `{tqStatus.Status}`\nOnline `{ (tqStatus.Online.ToLower() == "offline" ? "0" : tqStatus.Online) }`\nKills last hour `{tqStatus.Kills}`", true);
            if (response.Data != null)
            {
                var plexPrice = response.Data.Where(p => p.TypeId == 44992).FirstOrDefault()?.AveragePrice ?? 0;
                plexPrice /= 1000000;

                embed.AddField("💳 PLEX", $"`500`x`{plexPrice:0.00}`m =`{plexPrice * 500 / 1000:0.00}`b", true);
            }

            embed.AddField("🤖 zWebSocket", $"Status `{(Zkb.IsWorking ? "Online" : "Offline")}`\nWatching:\n{string.Join('\n', Zkb.ServiceConfigProvider.Config.Subs.Select(x => x.Contains(':') ? $"[{x}](https://zkillboard.com/{x.Replace(':', '/')})" : x))}", true);

            await ReplyEmbedAsync(embed);
        }

        [Command("character")]
        [Alias("char", "pilot", "player")]
        [Summary("Searches for a specific character")]
        public async Task Character([Remainder, Summary("Character name or id")]string name)
        {
            Task<Entity<CharacterInfo>> characterTask;

            if (long.TryParse(name, out long id))
                characterTask = ZkbApi.ApiClient.GetCharacterAsync(id.ToString());
            else
            {
                var q = await ZkbApi.ApiClient.SearchCharacterAsync(name);
                if (q is null || q.Count() == 0)
                {
                    await ReplyFailAsync();
                    return;
                }
                else if (q.Count() > 1)
                {
                    await ReplyAsync("Multiple characters found:\n" + string.Join('\n', q.Take(5).Select(x => $"{x.Name} `ID {x.Id}`")));
                    return;
                }

                characterTask = ZkbApi.ApiClient.GetCharacterAsync(q.First().Id.ToString());
            }

            var character = await characterTask;
            if (character.Id == 0)
            {
                await ReplyFailAsync();
                return;
            }

            var embed = new EmbedBuilder()
                .WithAuthor(character.Info.Name, $"https://imageserver.eveonline.com/Character/{character.Id}_64.jpg", $"https://zkillboard.com/character/{character.Id}/")
                .WithCurrentTimestamp().WithColor(Color.DarkBlue)
                .WithThumbnailUrl("https://imageserver.eveonline.com/Corporation/1000213_128.png")
                .WithDescription($"Dangerous `{character.DangerRatio}%`\nGangs `{character.GangRatio}%`\nSec. Status `{character.Info.SecStatus:0.00}`");

            var lastShips = character.TopLists.Ships;
            var lastSystems = character.TopLists.Systems;

            if (lastShips.Count > 0)
            {
                embed.AddField("🚀 Ships", string.Join("\n", lastShips.GroupBy(x => x.GroupName).Select(x => $"{x.Key}s\n{ string.Join("\n", x.Select(y => $"{y.ShipName} `{y.Kills} kills`")) }")), true);
                embed.AddField("🗺️ Regions", string.Join("\n", lastSystems.GroupBy(x => x.RegionName).Select(x => $"{x.Key} `{x.Sum(y => y.Kills)} kills`")), true);
            }

            string f(long i)
            {
                if (i >= 1000000)
                    if (i >= 1000000000)
                        return (i / 1000000000.0).ToString("0.00") + "b";
                    else
                        return (i / 1000000.0).ToString("0.00") + "m";
                else
                    return i.ToString("#,0");
            }

            embed.AddField("📊 Stats", $"ISK Destroyed `{f(character.IskDestroyed)}`\nISK Lost `{f(character.IskLost)}`\nShips Destroyed `{character.ShipsDestroyed}`\nShips Lost `{character.ShipsLost}`\nSolo kills `{character.SoloKills}`", true);

            if (character.Id == 96364388 || character.Id == 93476092)
                embed.Footer = new EmbedFooterBuilder().WithText("a.k.a. member of Osmon Police");

            await ReplyEmbedAsync(embed);
        }

        [Command("link")]
        [Summary("Links EVE account")]
        public async Task Link()
        {
            var dm = await Context.User.GetOrCreateDMChannelAsync();
            await dm.SendMessageAsync(embed: GetEmbedBase().WithDescription($"To link your Eve account please follow this [link]({EveSso.LinkCharacter(Context.User.Id)})\nYou can use it to link how many characters you want.").Build());
        }

        [Command("subscribe")]
        [Alias("sub")]
        [Summary("Subscribes to channel")]
        [RequireOwners]
        public async Task SubscribeAsync([Summary("Subscription link (e.g. 'character:92930794')")]string sub)
        {
            if (Zkb.IsWorking)
            {
                _ = Task.Run(() => Zkb.Subscribe(sub));
                await ReplySuccessAsync();
                return;
            }

            await ReplyFailAsync();
        }

        [Command("toggle")]
        [Summary("Toggles service status")]
        [RequireOwners]
        public async Task ToggleAsync()
        {
            if (Zkb.IsWorking)
                _ = Task.Run(() => Zkb.StopAsync());
            else
                _ = Task.Run(() => Zkb.StartAsync());
            await ReplySuccessAsync();
        }

        private EmbedBuilder GetEmbedBase()
        {
            return new EmbedBuilder()
                .WithTitle("EveChip")
                .WithCurrentTimestamp().WithColor(Color.DarkBlue)
                .WithThumbnailUrl("https://imageserver.eveonline.com/Corporation/1000213_128.png");
        }
    }
}