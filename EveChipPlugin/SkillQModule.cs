﻿using ChipBot.Core;
using ChipBot.Core.Preconditions;
using Discord.Commands;
using EveChipPlugin.Services;
using System.Threading.Tasks;

namespace EveChipPlugin.Modules
{
    [Name("EveSkillQ")]
    [RequireChannel]
    [Group("eveSkill")]
    [Alias("zSkill")]
    public class EveSkillQModule : ChipModuleBase
    {
        public SkillQService SkillQ { get; set; }

        [Command("subscribe")]
        [Alias("sub")]
        [Summary("Subscribes to skillQ notifier")]
        public async Task SubscribeAsync()
        {
            await SkillQ.ChangeSubscription(Context.User.Id, true);
            await ReplySuccessAsync();
        }

        [Command("unsubscribe")]
        [Alias("unsub")]
        [Summary("Unsubscribes from skillQ notifier")]
        public async Task UnsubscribeAsync()
        {
            await SkillQ.ChangeSubscription(Context.User.Id, false);
            await ReplySuccessAsync();
        }

        [Command("toggle")]
        [Summary("Toggles service status")]
        [RequireOwners]
        public async Task ToggleAsync()
        {
            if (SkillQ.IsWorking)
                _ = Task.Run(() => SkillQ.StopAsync());
            else
                _ = Task.Run(() => SkillQ.StartAsync());
            await ReplySuccessAsync();
        }
    }
}
