﻿using Newtonsoft.Json;

namespace EveChipPlugin
{
    public class ZKillPostConfig
    {
        [JsonProperty("ignoreBigKills")]
        public bool IgnoreBigKills { get; set; }

        [JsonProperty("advancedKills")]
        public bool AdvancedKills { get; set; }

        [JsonProperty("zPointToChipRatio")]
        public int ZPointToChipRatio { get; set; }

        [JsonProperty("channel")]
        public ulong Channel { get; set; }
    }
}
