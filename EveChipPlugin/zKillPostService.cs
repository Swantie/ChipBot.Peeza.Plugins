﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using ESI.NET.Models.Killmails;
using EveApiPlugin.Services;
using Humanizer;
using PointsPlugin.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zKillApiPlugin.Services;
using zKillWS_Plugin;
using zKillWS_Plugin.Services;

namespace EveChipPlugin.Services
{
    public class ZKillPostService : BackgroundService, IConfigurableService<ZKillPostConfig>
    {
        private readonly EveSsoService _eveSso;
        private readonly EveApiService _eveApi;
        private readonly ZWSService _zWebSocket;
        private readonly ZKillApiService _zApi;
        private readonly PointsService _points;
        private readonly DiscordSocketClient _discord;

        public ServiceConfigurationProvider<ZKillPostConfig> ServiceConfigProvider { get; set; }
        private ZKillPostConfig PostConfig => ServiceConfigProvider.Config;

        private List<string> FriendlySubs => _zWebSocket.ServiceConfigProvider.Config.Subs;
        private readonly BlockingCollection<object> Queue = new BlockingCollection<object>();

        private bool isWorking = false;
        public override bool IsWorking => isWorking;

        public ZKillPostService(LoggingService log, ConfigurationProvider provider, EveSsoService ssoService, EveApiService eveApi, ZWSService zWebSocket, ZKillApiService zKillApiService, PointsService points, DiscordSocketClient discord) : base(log, provider)
        {
            _eveSso = ssoService;
            _eveApi = eveApi;
            _zApi = zKillApiService;
            _zWebSocket = zWebSocket;
            _points = points;
            _discord = discord;

            ServiceConfigProvider = ServiceConfigurationProvider<ZKillPostConfig>.GetInstance(provider);
        }

        public override Task StartAsync()
        {
            isWorking = true;

            _zWebSocket.SmallKillReceived += AddKillToQueue;
            _zWebSocket.BigKillReceived += AddKillToQueue;

            Task.Run(ProcessQueue);

            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            isWorking = false;

            _zWebSocket.SmallKillReceived -= AddKillToQueue;
            _zWebSocket.BigKillReceived -= AddKillToQueue;

            return Task.CompletedTask;
        }

        private Task AddKillToQueue(object kill)
        {
            Queue.Add(kill);
            return Task.CompletedTask;
        }

        private async Task ProcessQueue()
        {
            while (isWorking)
            {
                var kill = Queue.Take();
                if (kill is BigKill) await ProcessBigKill((BigKill)kill);
                if (kill is SmallKill) await ProcessSmallKill((SmallKill)kill);
                await Task.Delay(1500);
            }
        }

        private async Task ProcessBigKill(BigKill kill)
        {
            if (PostConfig.IgnoreBigKills) return;
            if (PostConfig.Channel <= 0) return;

            await ((ITextChannel)_discord.GetChannel(PostConfig.Channel)).SendMessageAsync(kill.Url.ToString());
        }

        private async Task ProcessSmallKill(SmallKill kill)
        {
            if (PostConfig.Channel <= 0) return;
            var shouldPostUrl = true;

            if (PostConfig.AdvancedKills && kill.ShipTypeId != 670 && kill.ShipTypeId != 33328) // do not parse capsules
            {
                shouldPostUrl = false;

                try
                {
                    Log(LogSeverity.Info, $"Processing kill {kill.KillId}");
                    var zKill = (await _zApi.ApiClient.GetKill(kill.KillId)).First();
                    Log(LogSeverity.Verbose, $"Getting kill {kill.KillId} info from ESI");
                    var eveKill = (await _eveApi.ApiClient.Killmails.Information(zKill.Zkb.Hash, (int)zKill.KillmailId)).Data;
                    if (eveKill == null) throw new Exception("Couldn't get killmail info from ESI");

                    var finalBlow = eveKill.Attackers.Find(a => a.FinalBlow);
                    var topDamage = eveKill.Attackers.OrderByDescending(a => a.DamageDone).First();

                    var idsToResolve = new List<long>()
                    {
                        eveKill.Victim.ShipTypeId,
                        eveKill.Victim.CharacterId,
                        eveKill.Victim.CorporationId,
                        eveKill.Victim.AllianceID,

                        finalBlow.ShipTypeId,
                        finalBlow.CharacterId,
                        finalBlow.CorporationId,
                        finalBlow.AllianceId,

                        topDamage.ShipTypeId,
                        topDamage.CharacterId,
                        topDamage.CorporationId,
                        topDamage.AllianceId,

                        eveKill.SolarSystemId
                    }.Distinct().ToList();

                    var negative = IsNegativeKill(eveKill.Victim);

                    var names = (await _eveApi.GetNamedEntities(idsToResolve)).ToDictionary(kv => kv.Key, kv => kv.Value.Name);

                    string ResolveName(long id) => names.ContainsKey(id) ? names[id] : id.ToString();

                    Log(LogSeverity.Verbose, $"Building embed for kill {kill.KillId}");
                    var embed = new EmbedBuilder()
                        .WithAuthor("zKillboard", "https://zkillboard.com/img/wreck.png", "https://zkillboard.com/")
                        .WithDescription($"[{ResolveName(eveKill.Victim.CharacterId)}](https://zkillboard.com/character/{eveKill.Victim.CharacterId}/)'s [{ResolveName(eveKill.Victim.ShipTypeId)}](https://zkillboard.com/ship/{eveKill.Victim.ShipTypeId}/)" +
                        $" destroyed in [{ResolveName(eveKill.SolarSystemId)}](https://zkillboard.com/system/{eveKill.SolarSystemId}/) by " +
                        $"[{ResolveName(finalBlow.CharacterId)}](https://zkillboard.com/character/{finalBlow.CharacterId}/)'s [{ResolveName(finalBlow.ShipTypeId)}](https://zkillboard.com/ship/{finalBlow.ShipTypeId}/)")
                        .WithThumbnailUrl($"https://imageserver.eveonline.com/Render/{eveKill.Victim.ShipTypeId}_128.png").WithFooter(eveKill.KillmailTime.Humanize()).WithTimestamp(eveKill.KillmailTime)
                        .WithColor(negative ? Color.Red : Color.Green);

                    embed.AddField(VictimToField(ResolveName, "💀 Victim", eveKill.Victim));

                    if (finalBlow == topDamage)
                    {
                        embed.AddField(AttackerToField(ResolveName, "⭐ Final Blow & Top Damage", finalBlow));
                    }
                    else
                    {
                        embed.AddField(AttackerToField(ResolveName, "🐀 Final Blow", finalBlow));
                        embed.AddField(AttackerToField(ResolveName, "⚔️ Top Damage", topDamage));
                    }

                    var ptsAwarded = zKill.Zkb.Points * PostConfig.ZPointToChipRatio;

                    embed.AddField("ℹ️ Details", $"{(zKill.Zkb.Solo ? "**Solo**" : $"Attackers `{eveKill.Attackers.Count()}`")}\n" +
                        $"Damage `{eveKill.Attackers.Select(a => a.DamageDone).Sum()}`\n" +
                        $"Points `{ptsAwarded}`\n" +
                        $"[zKillboard Link](https://zkillboard.com/kill/{kill.KillId}/)");

                    if (_eveSso.IsWorking)
                    {
                        eveKill.Attackers.Select(a => _eveSso.GetUser(a.CharacterId)).Except(new ulong[] { 0 }).Distinct().ToList().ForEach(u => _points.AwardPoints(u, ptsAwarded));
                    }

                    await ((ITextChannel)_discord.GetChannel(PostConfig.Channel)).SendMessageAsync(embed: embed.Build());
                }
                catch(Exception ex)
                {
                    Log(LogSeverity.Error, "Error while processing small kill", ex);
                    shouldPostUrl = true;
                }
            }

            if (shouldPostUrl)
            {
                await ((ITextChannel)_discord.GetChannel(PostConfig.Channel)).SendMessageAsync(kill.Url.ToString());
            }
        }

        private bool IsNegativeKill(Victim victim)
        {
            foreach (var sub in new List<string>(FriendlySubs))
            {
                if (!sub.Contains(':')) continue;

                var split = sub.Split(':');

                switch (split[0])
                {
                    case "character":
                        if (victim.CharacterId.ToString() == split[1]) return true;
                        break;
                    case "corporation":
                        if (victim.CorporationId.ToString() == split[1]) return true;
                        break;
                    case "alliance":
                        if (victim.AllianceID.ToString() == split[1]) return true;
                        break;
                }
            }

            return false;
        }

        private EmbedFieldBuilder VictimToField(Func<long, string> resolveName, string header, Victim victim)
        {
            return new EmbedFieldBuilder().WithIsInline(true).WithName(header)
                .WithValue($"[{resolveName(victim.CharacterId)}](https://zkillboard.com/character/{victim.CharacterId}/)\n" +
                $"Ship [{resolveName(victim.ShipTypeId)}](https://zkillboard.com/ship/{victim.ShipTypeId}/)\n" +
                $"Corp [{resolveName(victim.CorporationId)}](https://zkillboard.com/corporation/{victim.CorporationId}/)" +
                (victim.AllianceID != 0 ? $"\nAli [{resolveName(victim.AllianceID)}](https://zkillboard.com/alliance/{victim.AllianceID}/)" : ""));
        }

        private EmbedFieldBuilder AttackerToField(Func<long, string> resolveName, string header, Attacker attacker)
        {
            return new EmbedFieldBuilder().WithIsInline(true).WithName(header)
                .WithValue($"[{resolveName(attacker.CharacterId)}](https://zkillboard.com/character/{attacker.CharacterId}/)\n" +
                $"Ship [{resolveName(attacker.ShipTypeId)}](https://zkillboard.com/ship/{attacker.ShipTypeId}/)\n" +
                $"Damage `{attacker.DamageDone}`\n" +
                $"Corp [{resolveName(attacker.CorporationId)}](https://zkillboard.com/corporation/{attacker.CorporationId}/)" +
                (attacker.AllianceId != 0 ? $"\nAli [{resolveName(attacker.AllianceId)}](https://zkillboard.com/alliance/{attacker.AllianceId}/)" : ""));
        }
    }
}
