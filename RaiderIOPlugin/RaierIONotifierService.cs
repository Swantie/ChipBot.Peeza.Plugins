﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using RaiderIOPlugin.Models;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RaiderIOPlugin.Services
{
    public class RaiderIONotifierService : BackgroundSaveableService<Dictionary<ulong, List<NotiferSubscription>>>
    {
        private readonly RaiderIOApiService _rioApi;
        private readonly ClockService _clockService;
        private readonly DiscordSocketClient _discord;

        private bool isWorking = false;
        public override bool IsWorking => isWorking;
        private bool notifiedToday = false;
        private int day = DateTime.Now.Day;

        public RaiderIONotifierService(LoggingService log, ConfigurationProvider provider, RaiderIOApiService rioApi, ClockService clockService, DiscordSocketClient discord) : base(log, provider, "RIO", "Notifier.json")
        {
            _rioApi = rioApi;
            _clockService = clockService;
            _discord = discord;
            Data = Load().GetAwaiter().GetResult() ?? new Dictionary<ulong, List<NotiferSubscription>>();
        }

        public async Task ChangeSubscription(ulong userId, string name, string realm, Region region, bool subscribe)
        {
            if (!Data.ContainsKey(userId))
            {
                Data.Add(userId, new List<NotiferSubscription>
                    {
                        new NotiferSubscription() { CharacterName = name, Realm = realm, Region = region, Subscribe = subscribe }
                    });
            }
            else
            {
                var subs = Data[userId];
                var chara = subs.Find(x => x.CharacterName == name && x.Realm == realm && x.Region == region);
                if (chara == null)
                {
                    subs.Add(new NotiferSubscription() { CharacterName = name, Realm = realm, Region = region, Subscribe = subscribe });
                }
                else
                {
                    chara.Subscribe = subscribe;
                }
            }

            await Save();
        }

        public async override Task StartAsync()
        {
            isWorking = true;

            _clockService.HourTick += _clockService_HourTick;
            await _clockService_HourTick();
        }

        private async Task _clockService_HourTick()
        {
            if (notifiedToday && day == DateTime.Now.Day) return;
            if (DateTime.Now.Hour < 12) return;
            notifiedToday = true;
            day = DateTime.Now.Day;

            Log(LogSeverity.Verbose, "Starting queue check");

            var copy = new Dictionary<ulong, List<NotiferSubscription>>(Data);
            var charaCache = new Dictionary<string, CharacterProfile>();
            var prevWeek = DateTime.UtcNow;
            while (prevWeek.DayOfWeek != DayOfWeek.Wednesday) prevWeek = prevWeek.AddDays(-1);
            prevWeek = prevWeek.AddHours(-prevWeek.Hour + 7).AddMinutes(-prevWeek.Minute);

            foreach (var user in copy)
            {
                var subbed = user.Value.Where(c => c.Subscribe);
                if (!subbed.Any()) continue;

                await Task.Delay(5000);
                Log(LogSeverity.Verbose, $"Getting user's ({user.Key}) characters");

                var embed = new EmbedBuilder().WithCurrentTimestamp().WithTitle("RaiderIO Weekly").WithColor(15048740).WithThumbnailUrl("https://cdnassets.raider.io/images/brand/Mark_2ColorWhite.png").WithUrl("https://raider.io/");

                foreach (var chara in subbed)
                {
                    await Task.Delay(1000);
                    var key = $"{chara.Region}|{chara.Realm}|{chara.CharacterName}";
                    CharacterProfile data = null;

                    Log(LogSeverity.Verbose, $"Getting {key}");
                    if (charaCache.ContainsKey(key))
                    {
                        data = charaCache[key];
                    }
                    else
                    {
                        try
                        {
                            data = await _rioApi.ApiClient.GetCharacterProfile(chara.Region, chara.Realm, chara.CharacterName);
                            charaCache.Add(key, data);
                        }
                        catch (Exception ex)
                        {
                            Log(LogSeverity.Verbose, $"Couldn't get {key}. ", ex);
                        }
                    }

                    if (data != null)
                    {
                        var chest = -1;
                        if (data.MythicPlusRecentRuns != null && data.MythicPlusRecentRuns.Any())
                        {
                            var runs = data.MythicPlusRecentRuns.Where(r => r.CompletedAt > prevWeek).OrderBy(r => r.MythicLevel);
                            if (runs.Any())
                            {
                                chest = runs.First().MythicLevel;
                            }
                        }

                        embed.AddField(data.Name, $"{data.Region.ToUpper()} - {data.Realm}\nWeekly Chest: `{(chest == -1 ? "None" : chest.ToString())}`", true);
                    }
                }

                await (await _discord.GetUser(user.Key).GetOrCreateDMChannelAsync()).SendMessageAsync(embed: embed.Build());
            }
        }

        public override Task StopAsync()
        {
            isWorking = false;

            _clockService.HourTick -= _clockService_HourTick;

            return Task.CompletedTask;
        }
    }
}
