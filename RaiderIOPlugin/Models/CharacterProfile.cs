﻿namespace RaiderIOPlugin.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class CharacterProfile
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("race")]
        public string Race { get; set; }

        [JsonProperty("class")]
        public string Class { get; set; }

        [JsonProperty("active_spec_name")]
        public string ActiveSpecName { get; set; }

        [JsonProperty("active_spec_role")]
        public string ActiveSpecRole { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("faction")]
        public string Faction { get; set; }

        [JsonProperty("achievement_points")]
        public long AchievementPoints { get; set; }

        [JsonProperty("honorable_kills")]
        public long HonorableKills { get; set; }

        [JsonProperty("thumbnail_url")]
        public string ThumbnailUrl { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("realm")]
        public string Realm { get; set; }

        [JsonProperty("profile_url")]
        public string ProfileUrl { get; set; }

        [JsonProperty("profile_banner")]
        public string ProfileBanner { get; set; }

        [JsonProperty("mythic_plus_scores_by_season")]
        public List<MythicPlusScoresBySeason> MythicPlusScoresBySeason { get; set; }

        [JsonProperty("mythic_plus_ranks")]
        public MythicPlusRanks MythicPlusRanks { get; set; }

        [JsonProperty("mythic_plus_recent_runs")]
        public List<MythicPlusRun> MythicPlusRecentRuns { get; set; }

        [JsonProperty("mythic_plus_best_runs")]
        public List<MythicPlusRun> MythicPlusBestRuns { get; set; }

        [JsonProperty("mythic_plus_highest_level_runs")]
        public List<MythicPlusRun> MythicPlusHighestLevelRuns { get; set; }

        [JsonProperty("mythic_plus_weekly_highest_level_runs")]
        public List<object> MythicPlusWeeklyHighestLevelRuns { get; set; }

        [JsonProperty("gear")]
        public Gear Gear { get; set; }

        [JsonProperty("raid_progression")]
        public RaidProgression RaidProgression { get; set; }

        [JsonProperty("guild")]
        public Guild Guild { get; set; }
    }

    public partial class Gear
    {
        [JsonProperty("item_level_equipped")]
        public long ItemLevelEquipped { get; set; }

        [JsonProperty("item_level_total")]
        public long ItemLevelTotal { get; set; }

        [JsonProperty("artifact_traits")]
        public long ArtifactTraits { get; set; }
    }

    public partial class Guild
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("realm")]
        public string Realm { get; set; }
    }

    public partial class MythicPlusRun
    {
        [JsonProperty("dungeon")]
        public string Dungeon { get; set; }

        [JsonProperty("short_name")]
        public string ShortName { get; set; }

        [JsonProperty("mythic_level")]
        public int MythicLevel { get; set; }

        [JsonProperty("completed_at")]
        public DateTimeOffset CompletedAt { get; set; }

        [JsonProperty("clear_time_ms")]
        public long ClearTimeMs { get; set; }

        [JsonProperty("num_keystone_upgrades")]
        public int NumKeystoneUpgrades { get; set; }

        [JsonProperty("map_challenge_mode_id")]
        public long MapChallengeModeId { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        [JsonProperty("affixes")]
        public List<AffixDetail> Affixes { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public partial class MythicPlusRanks
    {
        [JsonProperty("overall")]
        public Class Overall { get; set; }

        [JsonProperty("class")]
        public Class Class { get; set; }

        [JsonProperty("faction_overall")]
        public Class FactionOverall { get; set; }

        [JsonProperty("faction_class")]
        public Class FactionClass { get; set; }

        [JsonProperty("dps")]
        public Class Dps { get; set; }

        [JsonProperty("class_dps")]
        public Class ClassDps { get; set; }

        [JsonProperty("faction_dps")]
        public Class FactionDps { get; set; }

        [JsonProperty("faction_class_dps")]
        public Class FactionClassDps { get; set; }
    }

    public partial class Class
    {
        [JsonProperty("world")]
        public long World { get; set; }

        [JsonProperty("region")]
        public long Region { get; set; }

        [JsonProperty("realm")]
        public long Realm { get; set; }
    }

    public partial class MythicPlusScoresBySeason
    {
        [JsonProperty("season")]
        public string Season { get; set; }

        [JsonProperty("scores")]
        public Scores Scores { get; set; }
    }

    public partial class Scores
    {
        [JsonProperty("all")]
        public double All { get; set; }

        [JsonProperty("dps")]
        public double Dps { get; set; }

        [JsonProperty("healer")]
        public long Healer { get; set; }

        [JsonProperty("tank")]
        public long Tank { get; set; }
    }
    public partial class RaidProgression
    {
        [JsonProperty("antorus-the-burning-throne")]
        public AntorusTheBurningThrone AntorusTheBurningThrone { get; set; }

        [JsonProperty("battle-of-dazaralor")]
        public AntorusTheBurningThrone BattleOfDazaralor { get; set; }

        [JsonProperty("the-emerald-nightmare")]
        public AntorusTheBurningThrone TheEmeraldNightmare { get; set; }

        [JsonProperty("the-nighthold")]
        public AntorusTheBurningThrone TheNighthold { get; set; }

        [JsonProperty("tomb-of-sargeras")]
        public AntorusTheBurningThrone TombOfSargeras { get; set; }

        [JsonProperty("trial-of-valor")]
        public AntorusTheBurningThrone TrialOfValor { get; set; }

        [JsonProperty("uldir")]
        public AntorusTheBurningThrone Uldir { get; set; }
    }

    public partial class AntorusTheBurningThrone
    {
        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("total_bosses")]
        public long TotalBosses { get; set; }

        [JsonProperty("normal_bosses_killed")]
        public long NormalBossesKilled { get; set; }

        [JsonProperty("heroic_bosses_killed")]
        public long HeroicBossesKilled { get; set; }

        [JsonProperty("mythic_bosses_killed")]
        public long MythicBossesKilled { get; set; }
    }
}
