﻿using Newtonsoft.Json;

namespace RaiderIOPlugin.Models
{
    public class NotiferSubscription
    {
        [JsonProperty("characterName")]
        public string CharacterName { get; set; }
        [JsonProperty("realm")]
        public string Realm { get; set; }
        [JsonProperty("region")]
        public Region Region { get; set; }
        [JsonProperty("subscribe")]
        public bool Subscribe { get; set; }
    }
}
