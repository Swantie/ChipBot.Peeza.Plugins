﻿using ChipBot.Core.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RaiderIOPlugin.Models
{
    [Header("Accept-Encoding", "gzip")]
    public interface IRIOApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("characters/profile")]
        Task<CharacterProfile> GetCharacterProfile(string region, string realm, string name, string fields);

        [Get("mythic-plus/affixes")]
        Task<MythicAffixes> GetMythicAffixes(string region, string locale);

        // api/v1/guilds/profile

        // api/v1/mythic-plus/runs
        // api/v1/mythic-plus/score-tiers

        // api/v1/raiding/boss-rankings
        // api/v1/raiding/hall-of-fame
        // api/v1/raiding/progression
        // api/v1/raiding/raid-rankings
    }

    // ToDo: cache
    public static class RIOApiExtensions
    {
        public static Task<CharacterProfile> GetCharacterProfile(this IRIOApi api, Region region, string realm, string name, Fields fields = (Fields)511, Seasons seasons = Seasons.current | Seasons.previous)
        {
            if (seasons == 0) return api.GetCharacterProfile(region.ToString(), realm, name, fields.ToString().Replace(" ", ""));

            fields &= ~Fields.mythic_plus_scores_by_season;
            var seasonsString = Fields.mythic_plus_scores_by_season.ToString() + ":" + seasons.ToString().Replace(", ",":") + ",";
            return api.GetCharacterProfile(region.ToString(), realm, name, seasonsString + fields.ToString().Replace(" ", ""));
        }

        public static Task<MythicAffixes> GetMythicAffixes(this IRIOApi api, Region region, Locale locale = Locale.en)
            => api.GetMythicAffixes(region.ToString(), locale.ToString());
    }
}