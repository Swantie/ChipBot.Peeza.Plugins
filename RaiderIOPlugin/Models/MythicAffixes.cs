﻿namespace RaiderIOPlugin.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class MythicAffixes
    {
        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("leaderboard_url")]
        public string LeaderboardUrl { get; set; }

        [JsonProperty("affix_details")]
        public List<AffixDetail> AffixDetails { get; set; }
    }

    public partial class AffixDetail
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("wowhead_url")]
        public string WowheadUrl { get; set; }
    }
}
