﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RaiderIOPlugin.Models
{
    public enum Region
    {
        us, eu, kr, tw
    }

    public enum Locale
    {
        en, ru, ko, cn, pt, it, fr, es, de, tw
    }

    [Flags]
    public enum Fields
    {
        gear = 1,
        guild = 2,
        raid_progression = 4,
        mythic_plus_scores_by_season = 8,
        mythic_plus_ranks = 16,
        mythic_plus_recent_runs = 32,
        mythic_plus_best_runs = 64,
        mythic_plus_highest_level_runs = 128,
        mythic_plus_weekly_highest_level_runs = 256
    }

    [Flags]
    public enum Seasons
    {
        previous = 1,
        current = 2
    }
}