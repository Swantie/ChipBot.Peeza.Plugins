﻿using ChipBot.Core;
using ChipBot.Core.Preconditions;
using Discord;
using Discord.Commands;
using RaiderIOPlugin.Services;
using System.Threading.Tasks;
using RaiderIOPlugin.Models;
using System.Linq;
using System;

namespace RaiderIOPlugin.Modules
{
    [Name("RaiderIO")]
    [RequireChannel]
    [Group("rio")]
    public class RaiderIOModule : ChipModuleBase, IConfigurableService<RaiderIOConfiguration>
    {
        public RaiderIOApiService RIO { get; set; }
        public RaiderIONotifierService RIONotify { get; set; }

        private static ServiceConfigurationProvider<RaiderIOConfiguration> config;
        [DontInject]
        public ServiceConfigurationProvider<RaiderIOConfiguration> ServiceConfigProvider { get { return config; } set { config = value; } }

        public RaiderIOModule(ConfigurationProvider provider)
        {
            if (config == null) ServiceConfigProvider = ServiceConfigurationProvider<RaiderIOConfiguration>.GetInstance(provider);
        }

        [Command]
        [Priority(0)]
        [Summary("Displays service status")]
        public async Task Default(string name, string realm = null, string region = null)
        {
            Region reg;

            if (realm == null) realm = ServiceConfigProvider.Config.DefaultRealm;
            if (region == null)
            {
                reg = ServiceConfigProvider.Config.DefaultRegion;
            }
            else
            {
                if (!Region.TryParse(region, out reg))
                {
                    await ReplyAsync("Couldn't parse region. Available regions are: `us`, `eu`, `kr`, `tw`");
                    return;
                }
            }

            try
            {
                var chara = await RIO.ApiClient.GetCharacterProfile(reg, realm, name);
                var embed = new EmbedBuilder()
                    .WithAuthor("RaiderIO", "https://cdnassets.raider.io/images/brand/Mark_2ColorWhite.png", "https://raider.io/")
                    .WithTitle(chara.Name).WithUrl(chara.ProfileUrl).WithThumbnailUrl(chara.ThumbnailUrl)
                    .WithColor(15048740)
                    .WithDescription($"{chara.Race}\n{chara.ActiveSpecName} {chara.Class} - {chara.ActiveSpecRole}\n{chara.Region.ToUpper()} {chara.Realm}\n`{chara.Gear.ItemLevelEquipped}`ilvl")
                    .AddField("Score", $"Current Season `{chara.MythicPlusScoresBySeason[1].Scores.All}` {ScoreToEmote(chara.MythicPlusScoresBySeason[1].Scores.All)}\n" +
                    $"Previous Season `{chara.MythicPlusScoresBySeason[0].Scores.All}` {ScoreToEmote(chara.MythicPlusScoresBySeason[0].Scores.All)}\n\nRealm `{chara.MythicPlusRanks.FactionOverall.Realm}`", true)
                    .WithCurrentTimestamp(); ;
                if (chara.MythicPlusHighestLevelRuns.Count > 0)
                    embed.AddField("Mythic+", $"Highest Closed {MythicPlusRunToString(chara.MythicPlusHighestLevelRuns[0])}\n\nBest Runs:\n{ string.Join('\n', chara.MythicPlusBestRuns.Take(3).Select(r => MythicPlusRunToString(r))) }", true);
                    
                await ReplyEmbedAsync(embed);
            }
            catch
            {
                await ReplyFailAsync();
            }
        }

        [Command("subscribe")]
        [Priority(1)]
        [Alias("sub")]
        [Summary("Displays service status")]
        public async Task Subscribe(string name, string realm = null, string region = null)
            => await ChangeSubscription(true, name, realm, region);

        [Command("unsubscribe")]
        [Priority(1)]
        [Alias("unsub")]
        [Summary("Displays service status")]
        public async Task Unsubscribe(string name, string realm = null, string region = null)
            => await ChangeSubscription(false, name, realm, region);

        private async Task ChangeSubscription(bool sub, string name, string realm, string region)
        {
            Region reg;

            if (realm == null) realm = ServiceConfigProvider.Config.DefaultRealm;
            if (region == null)
            {
                reg = ServiceConfigProvider.Config.DefaultRegion;
            }
            else
            {
                if (!Enum.TryParse(region, out reg))
                {
                    await ReplyAsync("Couldn't parse region. Available regions are: `us`, `eu`, `kr`, `tw`");
                    return;
                }
            }

            await RIONotify.ChangeSubscription(Context.User.Id, name, realm, reg, sub);
            await ReplySuccessAsync();
        }

        private string ScoreToEmote(double score)
        {
            if (score < 1000) return ServiceConfigProvider.Config.BadScoreEmote.ToString();
            if (score < 2000) return ServiceConfigProvider.Config.MiddleScoreEmote.ToString();
            return ServiceConfigProvider.Config.GoodScoreEmote.ToString();
        }
        private string MythicPlusRunToString(MythicPlusRun run)
        {
            return $"[{run.ShortName} {run.MythicLevel}{new string('*', run.NumKeystoneUpgrades)}]({run.Url})";
        }
    }
}