﻿using ChipBot.Core;
using ChipBot.Core.Services;
using RaiderIOPlugin.Models;
using RestEase;
using System;
using System.Net.Http;

namespace RaiderIOPlugin.Services
{
    public class RaiderIOApiService : ApiService<IRIOApi>
    {
        private const string ApiUrl = "https://raider.io/api/v1/";

        public RaiderIOApiService(LoggingService log, ConfigurationProvider provider) : base(log, provider)
        {
            ApiClient = new RestClient(
                new HttpClient(
                    new HttpClientHandler()
                    {
                        AutomaticDecompression = System.Net.DecompressionMethods.GZip
                    })
                {
                    BaseAddress = new Uri(ApiUrl)
                }).For<IRIOApi>();

            ApiClient.UserAgent = UserAgent;
        }
    }
}
