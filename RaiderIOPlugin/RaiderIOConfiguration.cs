﻿using Discord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RaiderIOPlugin.Models
{
    public class RaiderIOConfiguration
    {
        [JsonProperty("default_region")]
        public Region DefaultRegion { get; set; } = Region.eu;

        [JsonProperty("default_realm")]
        public string DefaultRealm { get; set; } = "soulflayer";

        [JsonProperty("bad_score_emote")]
        public string BadEmote { get; set; } = "";

        [JsonProperty("middle_score_emote")]
        public string MiddleEmote { get; set; } = "";

        [JsonProperty("good_score_emote")]
        public string GoodEmote { get; set; } = "";

        [JsonIgnore]
        public IEmote BadScoreEmote => Emote.TryParse(BadEmote, out Emote emote) ? emote : new Emoji("🐸") as IEmote;
        [JsonIgnore]
        public IEmote MiddleScoreEmote => Emote.TryParse(MiddleEmote, out Emote emote) ? emote : new Emoji("🐢") as IEmote;
        [JsonIgnore]
        public IEmote GoodScoreEmote => Emote.TryParse(GoodEmote, out Emote emote) ? emote : new Emoji("🔥") as IEmote;
    }
}
