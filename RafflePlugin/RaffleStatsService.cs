﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using PointsPlugin.Services;
using RafflePlugin.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RafflePlugin.Services
{
    public class RaffleStatsService : ChipSaveableService<RaffleStats>
    {
        private Random _random;
        private PointsService _points;
        private bool changes = false;

        public RaffleStatsService(ClockService clock, LoggingService log, ConfigurationProvider provider, PointsService points, Random random) : base(log, provider, "Points", "stats.json")
        {
            Data = Load().GetAwaiter().GetResult() ?? new RaffleStats() { PerUserStats = new Dictionary<ulong, PerUserStat>() };

            _random = random;
            _points = points;

            clock.FiveMinutesTick += Clock_FiveMinutesTick;
        }

        public RaffleResult DoRaffle(IUser user, long points, bool manMode)
        {
            var rolled = _random.Next(1, 101);
            var res = new RaffleResult() { Rolled = rolled };

            if (rolled >= 99)
            {
                points *= manMode ? 3 : 2;
                res.Result = manMode ? RaffleResultEnum.Triple : RaffleResultEnum.Double;
                res.CurrentPoints = _points.AwardPoints(user, points);
                LogOperation(user.Id, points, true);
            }
            else if (rolled >= 75)
            {
                res.Result = RaffleResultEnum.Win;
                res.CurrentPoints = _points.AwardPoints(user, points);
                LogOperation(user.Id, points, true);
            }
            else
            {
                res.Result = RaffleResultEnum.Lost;
                res.CurrentPoints = _points.ChangePoints(user, -points);
                LogOperation(user.Id, points, false);
            }

            return res;
        }

        private void LogOperation(ulong userid, long pts, bool won)
        {
            Log(LogSeverity.Verbose, $"Logging user {userid} {(won ? "won" : "lost")} {pts}pts");
            changes = true;
            PerUserStat userStats;
            if (Data.PerUserStats.ContainsKey(userid))
                userStats = Data.PerUserStats[userid];
            else
            {
                userStats = new PerUserStat();
                Data.PerUserStats.Add(userid, userStats);
            }

            if (won)
            {
                Data.TotalWon++;
                userStats.TotalWon++;
                if (Data.BiggestWin < pts)
                {
                    Data.BiggestWin = pts;
                    Data.BiggestWinner = userid;
                }
                userStats.TotalPtsWon += pts;
            }
            else
            {
                Data.TotalLost++;
                userStats.TotalLost++;
                if (Data.BiggestLose < pts)
                {
                    Data.BiggestLose = pts;
                    Data.BiggestLoser = userid;
                }
                userStats.TotalPtsLost += pts;
            }
        }

        public RaffleStats GetData()
            => Data;
        public PerUserStat GetUserData(IUser user) => GetUserData(user.Id);
        public PerUserStat GetUserData(ulong id) => Data.PerUserStats.ContainsKey(id) ? Data.PerUserStats[id] : null;

        private async Task Clock_FiveMinutesTick()
        {
            if (changes)
            {
                await Save();
                changes = false;
            }
        }
    }
}
