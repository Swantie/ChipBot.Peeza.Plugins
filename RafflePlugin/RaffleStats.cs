﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RafflePlugin.Models
{
    public class RaffleStats
    {
        [JsonProperty("totalWon")]
        public long TotalWon { get; set; }
        [JsonProperty("totalLost")]
        public long TotalLost { get; set; }

        [JsonProperty("biggestWinner")]
        public ulong BiggestWinner { get; set; }
        [JsonProperty("biggestWin")]
        public long BiggestWin { get; set; }
        [JsonProperty("biggestLoser")]
        public ulong BiggestLoser { get; set; }
        [JsonProperty("biggestLose")]
        public long BiggestLose { get; set; }

        [JsonProperty("perUserStats")]
        public Dictionary<ulong, PerUserStat> PerUserStats { get; set; }
    }

    public class PerUserStat
    {
        [JsonProperty("totalWon")]
        public long TotalWon { get; set; }
        [JsonProperty("totalLost")]
        public long TotalLost { get; set; }

        [JsonProperty("totalPtsWon")]
        public long TotalPtsWon { get; set; }
        [JsonProperty("totalPtsLost")]
        public long TotalPtsLost { get; set; }
    }
}
