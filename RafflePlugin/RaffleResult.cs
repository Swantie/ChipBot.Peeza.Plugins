﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RafflePlugin
{
    public class RaffleResult
    {
        public int Rolled { get; set; }
        public long CurrentPoints { get; set; }
        public RaffleResultEnum Result { get; set; }
    }

    public enum RaffleResultEnum
    {
        Triple,
        Double,
        Win,
        Lost
    }
}
