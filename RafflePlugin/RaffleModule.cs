﻿using ChipBot.Core;
using ChipBot.Core.Attributes;
using ChipBot.Core.Preconditions;
using Discord.Commands;
using PointsPlugin.Attributes;
using PointsPlugin.Services;
using RafflePlugin.Services;
using System;
using System.Threading.Tasks;

namespace RafflePlugin.Modules
{
    [Group("Raffle")]
    [Alias("r", "rofl")]
    [RequireChannel]
    public class RaffleModule : ChipModuleBase
    {
        public const string BigWinFormat = "{0} WOW! You've rolled {1} and __won__ {2}{3}. You have {4}{3} now!";
        public const string WinFormat = "{0} congrats! You've rolled {1} and __won__ {2}{3}. You have {4}{3} now!";
        public const string LoseFormat = "{0} what a shame. You've rolled only {1} and __lost__ {2}{3}. You have {4}{3} now.";
        public const string BigLoseFormat = "{0} atleast you can call yourself a man now. You've rolled {1}. You lost {2}{3}.";

        public PointsService Points { get; set; }
        public RaffleStatsService Stats { get; set; }

        public string Currency => Points.CurrencyName;

        [Command]
        [Summary("Raffle your points. 10 points min. You need atleast 75 on dice to win. Also there is 2% chance to DOUBLE your bet")]
        public async Task Default([Summary("Points to raffle")][Range(10, long.MaxValue)][HasEnoughPoints]long points)
        {
            var res = Stats.DoRaffle(Context.User, points, false);

            switch (res.Result)
            {
                case RaffleResultEnum.Double:
                    await ReplyAsync(string.Format(BigWinFormat, Context.User.Mention, res.Rolled, points * 2, Currency, res.CurrentPoints));
                    break;
                case RaffleResultEnum.Win:
                    await ReplyAsync(string.Format(WinFormat, Context.User.Mention, res.Rolled, points, Currency, res.CurrentPoints));
                    break;
                case RaffleResultEnum.Lost:
                    await ReplyAsync(string.Format(LoseFormat, Context.User.Mention, res.Rolled, points, Currency, res.CurrentPoints));
                    break;
            }
        }

        [Command("all")]
        [Alias("killme", "fuckme")]
        [Summary("Raffle all of your points. Be a man. You need atleast 75 on dice to win. Also there is 2% chance to TRIPLE your bet")]
        public async Task All()
        {
            var points = Points.GetPoints(Context.User);
            if (points <= 0)
            {
                await ReplyFailAsync();
                return;
            }

            var res = Stats.DoRaffle(Context.User, points, true);

            switch (res.Result)
            {
                case RaffleResultEnum.Triple:
                    await ReplyAsync(string.Format(BigWinFormat, Context.User.Mention, res.Rolled, points * 3, Currency, res.CurrentPoints));
                    break;
                case RaffleResultEnum.Win:
                    await ReplyAsync(string.Format(WinFormat, Context.User.Mention, res.Rolled, points, Currency, res.CurrentPoints));
                    break;
                case RaffleResultEnum.Lost:
                    await ReplyAsync(string.Format(BigLoseFormat, Context.User.Mention, res.Rolled, points, Currency, res.CurrentPoints));
                    break;
            }
        }

        [Command("statistics")]
        [Alias("stats")]
        [Summary("Displays raffle statistics")]
        public async Task Statistics()
        {
            if (Stats.GetData().TotalLost == 0 || Stats.GetData().TotalWon == 0)
                await ReplyFailAsync();
            else
            {
                var reply = $"Biggest winner: <@{Stats.GetData().BiggestWinner}> - `{Stats.GetData().BiggestWin}`{Currency}\n" +
                    $"Biggest loser: <@{Stats.GetData().BiggestLoser}> - `{Stats.GetData().BiggestLose}`{Currency}\n" +
                    $"Total wins/loses: `{Stats.GetData().TotalWon}`/`{Stats.GetData().TotalLost}`";

                if (Stats.GetData().PerUserStats.ContainsKey(Context.User.Id))
                {
                    var userStats = Stats.GetData().PerUserStats[Context.User.Id];
                    reply += $"\n{Context.User.Mention} stats:\n" +
                        $"You've won {userStats.TotalPtsWon}{Currency} over {userStats.TotalWon} wins.\n" +
                        $"You've lost {userStats.TotalPtsLost}{Currency} over {userStats.TotalLost} loses.";
                }
                await ReplyAsync(reply);
            }
        }
    }
}
