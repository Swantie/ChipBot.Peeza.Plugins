﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TwitchPlugin
{
    public enum TriggerType
    {
        Prefix,
        KeyWord
    }

    public class UserCommand
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("aliases")]
        public List<string> Aliases { get; set; }

        [JsonProperty("response")]
        public string Response { get; set; }

        [JsonProperty("count")]
        public uint Count { get; set; }

        [JsonProperty("caseSensitive")]
        public bool CaseSensitive { get; set; }

        [JsonProperty("trigger")]
        public TriggerType TriggerType { get; set; }

        [JsonProperty("argsCount")]
        public uint ArgsCount { get; set; }
    }

    public class TwitchUserSettings
    {
        [JsonProperty("prefix")]
        public string Prefix { get; set; } = "!";

        [JsonProperty("commandList")]
        public List<UserCommand> UserCommands { get; set; } = new List<UserCommand>();
    }
}
