﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;

namespace TwitchPlugin.Services
{
    public class TwitchService : BackgroundService, IConfigurableService<TwitchConfig>
    {
        private readonly DiscordSocketClient _discord;
        private readonly ClockService _clock;

        private readonly ConnectionCredentials credentials;
        public TwitchClient TwitchClient { get; private set; }

        private bool _isWorking = false;
        public override bool IsWorking => _isWorking;

        private SocketGuild MainGuild => _discord.GetGuild(Config.Guilds.MainGuild);
        private List<ulong> RoleIds => Config.AllowedRoles;

        public ServiceConfigurationProvider<TwitchConfig> ServiceConfigProvider { get; set; }

        private Dictionary<ulong, string> watchingUsers;
        private Dictionary<string, ulong> watchingUsersReverse;

        public TwitchService(DiscordSocketClient discord, ConfigurationProvider config, LoggingService logger, ClockService clock) : base(logger, config)
        {
            _discord = discord;
            _clock = clock;

            ServiceConfigProvider = ServiceConfigurationProvider<TwitchConfig>.GetInstance(config);

            credentials = new ConnectionCredentials(ServiceConfigProvider.Config.Username, ServiceConfigProvider.Config.Token);
            TwitchClient = new TwitchClient();
            TwitchClient.Initialize(credentials);

            TwitchClient.OnJoinedChannel += TwitchOnJoinedChannel;

            watchingUsers = new Dictionary<ulong, string>();
            watchingUsersReverse = new Dictionary<string, ulong>();

            Log(LogSeverity.Verbose, "Initialized");
        }

        public override Task StartAsync()
        {
            _isWorking = true;
            TwitchClient.Connect();
            Log(LogSeverity.Verbose, "Connected to Twitch");

            _clock.HalfMinuteTick += _clock_FiveMinutesTick;

            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            _isWorking = false;
            TwitchClient.Disconnect();
            Log(LogSeverity.Verbose, "Disconnected from Twitch");

            _clock.FiveMinutesTick -= _clock_FiveMinutesTick;
            return Task.CompletedTask;
        }

        public ulong ResolveUserId(string channel)
        {
            return watchingUsersReverse.ContainsKey(channel) ? watchingUsersReverse[channel] : 0;
        }

        public void SendMessage(string channel, string msg)
        {
            try
            {
                TwitchClient.SendMessage(TwitchClient.GetJoinedChannel(channel), msg);
            }
            catch(Exception ex)
            {
                Log(LogSeverity.Error, $"Couldn't send message '{msg}' to '{channel}' got an exception '{ex.Message}'");
            }
        }

        private Task _clock_FiveMinutesTick()
        {
            var streamingUsers = MainGuild.Users
                .Where(x => !(x.Activity is null) && x.Activity.Type == ActivityType.Streaming && x.Roles.Any(r => RoleIds.Contains(r.Id)))
                .Where(u => !ServiceConfigProvider.Config.DisableFor.Contains(u.Id)).ToList();

            foreach (var user in streamingUsers)
            {
                if (!watchingUsers.ContainsKey(user.Id))
                {
                    var streamingActivity = (StreamingGame)user.Activity;
                    if (string.IsNullOrEmpty(streamingActivity.Url)) continue;

                    var twitchName = (streamingActivity.Url.EndsWith('/') ? streamingActivity.Url.Substring(0, streamingActivity.Url.Length - 1) : streamingActivity.Url)
                        .Split('/').Last();

                    watchingUsers.Add(user.Id, twitchName);
                    watchingUsersReverse.Add(twitchName, user.Id);

                    Log(LogSeverity.Verbose, "Joining channel: " + twitchName);
                    TwitchClient.JoinChannel(twitchName);
                }
            }

            var notStreaming = watchingUsers.Keys.Except(streamingUsers.Select(x => x.Id)).ToList();

            foreach (var key in notStreaming)
            {
                var channel = watchingUsers[key];
                Log(LogSeverity.Verbose, "Leaving channel: " + channel);
                SendMessage(channel, "FeelsBadMan");
                TwitchClient.LeaveChannel(channel);

                watchingUsersReverse.Remove(channel);
                watchingUsers.Remove(key);
            }

            return Task.CompletedTask;
        }

        private async void TwitchOnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            await Task.Delay(1000);
            SendMessage(e.Channel, "VoHiYo");
        }
    }
}
