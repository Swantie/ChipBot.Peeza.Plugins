﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TwitchPlugin
{
    public class TwitchConfig
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }

        [JsonProperty("disableFor")]
        public List<ulong> DisableFor { get; set; }
    }
}
