﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TwitchPlugin
{
    public class TwitchCommandConfig
    {
        [JsonProperty("userSettings")]
        public Dictionary<ulong, TwitchUserSettings> UserSettings { get; set; } = new Dictionary<ulong, TwitchUserSettings>();
    }
}
