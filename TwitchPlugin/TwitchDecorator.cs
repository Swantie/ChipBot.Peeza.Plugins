﻿using System.Text;
using TwitchLib.Client.Models;

namespace TwitchPlugin
{
    public static class TwitchDecorator
    {
        private const string BotName = "&botName";
        private const string ChannelName = "&channelName";
        private const string UserName = "&userName";
        private const string CmdName = "&cmdName";
        private const string CmdCount = "&cmdCount";
        private const string Args = "&arg{0}";

        public static string Decorate(ChatMessage message, UserCommand command, params string[] args)
        {
            var sb = new StringBuilder(command.Response);

            sb
                .Replace(BotName, message.BotUsername)
                .Replace(ChannelName, message.Channel)
                .Replace(UserName, message.Username)
                .Replace(CmdName, command.Name)
                .Replace(CmdCount, command.Count.ToString());

            if (args != null)
            {
                for (var i = 0; i < args.Length; i++)
                {
                    sb.Replace(string.Format(Args, args[i]), args[i]);
                }
            }

            return sb.ToString();
        }
    }
}
