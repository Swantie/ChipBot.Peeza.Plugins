﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;

namespace TwitchPlugin.Services
{
    public class TwitchCommandHandlingService : BackgroundService, IConfigurableService<TwitchCommandConfig>
    {
        private readonly TwitchService _twitch;
        private readonly ClockService _clock;

        public ServiceConfigurationProvider<TwitchCommandConfig> ServiceConfigProvider { get; set; }
        private Dictionary<ulong, TwitchUserSettings> UserSettings => ServiceConfigProvider.Config.UserSettings;

        private bool isWorking = false;
        public override bool IsWorking => isWorking;

        private bool changedSetting = false;

        private readonly MemoryCache _cache;

        public TwitchCommandHandlingService(LoggingService log, ConfigurationProvider provider, TwitchService twitchService, ClockService clock, MemoryCache cache) : base(log, provider)
        {
            _twitch = twitchService;
            _clock = clock;
            _cache = cache;

            ServiceConfigProvider = ServiceConfigurationProvider<TwitchCommandConfig>.GetInstance(provider);

            _clock.FiveMinutesTick += () =>
            {
                if (changedSetting)
                {
                    ServiceConfigProvider.Save();
                }

                return Task.CompletedTask;
            };
        }

        public override Task StartAsync()
        {
            isWorking = true;

            _twitch.TwitchClient.OnMessageReceived += HandleMessage;

            return Task.CompletedTask;
        }

        public override Task StopAsync()
        {
            isWorking = false;

            _twitch.TwitchClient.OnMessageReceived -= HandleMessage;

            return Task.CompletedTask;
        }

        private Dictionary<string, UserCommand> FillCommandList(ulong userId, TwitchUserSettings settings)
        {
            var cacheKey = $"usrCmds{userId}";
            var userCmds = (Dictionary<string, UserCommand>)_cache.Get(cacheKey);

            if (userCmds == null)
            {
                userCmds = new Dictionary<string, UserCommand>(StringComparer.InvariantCultureIgnoreCase);

                foreach (var userCmd in settings.UserCommands)
                {
                    if (userCmd.TriggerType != TriggerType.Prefix) continue;

                    userCmds.Add(userCmd.Name, userCmd);
                    userCmd.Aliases.ForEach(a =>
                    {
                        if (!userCmds.ContainsKey(a))
                        {
                            userCmds.Add(a, userCmd);
                        }
                    });
                }
            }

            _cache.Set(cacheKey, userCmds, DateTimeOffset.Now.AddMinutes(20));

            return userCmds;
        }

        private void HandleMessage(object sender, OnMessageReceivedArgs e)
        {
            var userId = _twitch.ResolveUserId(e.ChatMessage.Channel);
            if (userId == 0) return;

            if (!UserSettings.ContainsKey(userId))
            {
                changedSetting = true;
                UserSettings.Add(userId, new TwitchUserSettings());
            }

            var userConfig = UserSettings[userId];
            if (userConfig.UserCommands == null || !userConfig.UserCommands.Any()) return;

            var userCmds = FillCommandList(userId, userConfig);

            var split = e.ChatMessage.Message.Split();

            ParseKeyWords(e.ChatMessage, split, userConfig);

            var cmdName = split[0];
            var args = split.Skip(1).ToArray();

            if (!cmdName.StartsWith(userConfig.Prefix)) return;

            cmdName = cmdName.Substring(userConfig.Prefix.Length);

            UserCommand cmd;

            cmd = userCmds.ContainsKey(cmdName) ? userCmds[cmdName] : null;

            if (cmd == null) return;
            if (cmd.CaseSensitive && cmdName != cmd.Name && !cmd.Aliases.Contains(cmdName)) return;
            if (cmd.ArgsCount != args.Length) return;

            cmd.Count++;
            changedSetting = true;
            _twitch.SendMessage(e.ChatMessage.Channel, TwitchDecorator.Decorate(e.ChatMessage, cmd, args));
        }

        private void ParseKeyWords(ChatMessage chatMessage, string[] message, TwitchUserSettings settings)
        {
            UserCommand triggered = null;

            var cmds = settings.UserCommands.Where(c => c.TriggerType == TriggerType.KeyWord).ToList();

            foreach (var word in message)
            {
                foreach (var cmd in cmds)
                {
                    if (cmd.CaseSensitive)
                    {
                        if (cmd.Name == word || cmd.Aliases.Contains(word)) triggered = cmd;
                    }
                    else
                    {
                        if (cmd.Name.ToLower() == word.ToLower() || cmd.Aliases.Select(a => a.ToLower()).Contains(word.ToLower())) triggered = cmd;
                    }

                    if (triggered != null) break;
                }

                if (triggered != null) break;
            }

            triggered.Count++;
            changedSetting = true;
            _twitch.SendMessage(chatMessage.Channel, TwitchDecorator.Decorate(chatMessage, triggered, null));
        }
    }
}
