﻿using ChipBot.Core;
using ChipBot.Core.Attributes;
using ChipBot.Core.Preconditions;
using Discord;
using Discord.Commands;
using PointsPlugin.Services;
using System.Threading.Tasks;

namespace PointsPlugin.Modules
{
    [Group("Points")]
    [Alias("pts", "coins", "p")]
    [Remarks("silent")]
    [RequireChannel]
    [RequireOwners]
    public class PointsAdminModule : ChipModuleBase
    {
        public PointsService Points { get; set; }

        [Command("add")]
        [Summary("Changes points to someone")]
        public async Task Add([Summary("User to give points to")]IUser user, [Summary("Amount of points to give")][Range(long.MinValue, long.MaxValue)]long points)
        {
            Points.ChangePoints(user, points);
            await ReplySuccessAsync();
        }

        [Command("wipe")]
        [Summary("Takes all points away from someone")]
        public async Task Wipe([Summary("User to take points from")]IUser user)
        {
            var p = Points.GetPoints(user);
            if (p > 0)
                Points.ChangePoints(user, -p);
            await ReplySuccessAsync();
        }

        [Command("settings")]
        [Summary("Displays current point settings")]
        public async Task Settings()
        {
            var cfg = Points.ServiceConfigProvider.Config;
            await ReplyAsync($"Points per minute `{cfg.PointsPerMinute}`\nPoints multiplier `{cfg.PointsMultiplier}`\nCurrency name `{cfg.CurrencyName}`");
        }

        [Command("settings")]
        [Summary("Sets current point settings")]
        public async Task Settings(long perMin, long mult, string currency)
        {
            var cfg = Points.ServiceConfigProvider.Config;

            cfg.PointsPerMinute = perMin;
            cfg.PointsMultiplier = mult;
            cfg.CurrencyName = currency;

            await Settings();
        }
    }
}
