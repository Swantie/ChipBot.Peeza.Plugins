﻿using Newtonsoft.Json;

namespace PointsPlugin
{
    public class PointsConfiguration
    {
        [JsonProperty("pointsPerMinute")]
        [JsonRequired]
        public long PointsPerMinute { get; set; } = 1;

        [JsonProperty("pointsMultiplier")]
        [JsonRequired]
        public long PointsMultiplier { get; set; } = 1;

        [JsonProperty("currencyName")]
        [JsonRequired]
        public string CurrencyName { get; set; } = "$";
    }
}
