﻿using Discord.Commands;
using PointsPlugin.Services;
using System;
using System.Threading.Tasks;

namespace PointsPlugin.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
    public class HasEnoughPointsAttribute : ParameterPreconditionAttribute
    {
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            var points = (PointsService)services.GetService(typeof(PointsService));

            if (points.HasEnough(context.User, (long)(value)))
               return Task.FromResult(PreconditionResult.FromSuccess());
            else
               return Task.FromResult(PreconditionResult.FromError("You don't have enough points"));
        }
    }
}
