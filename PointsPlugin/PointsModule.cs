﻿using ChipBot.Core;
using ChipBot.Core.Attributes;
using ChipBot.Core.Preconditions;
using Discord;
using Discord.Commands;
using PointsPlugin.Attributes;
using PointsPlugin.Services;
using System.Threading.Tasks;

namespace PointsPlugin.Modules
{
    [Group("Points")]
    [Alias("pts", "coins", "p")]
    [RequireChannel]
    public class PointsModule : ChipModuleBase
    {
        public PointsService Points { get; set; }

        [Command]
        [Summary("Displays your current amount of points")]
        public Task Default()
            => Default(Context.User);

        [Command]
        [Summary("Displays points amount of specified user")]
        public async Task Default(IUser user)
        {
            await ReplyAsync($"{user.Mention} has {Points.GetUserPointsFormatted(user)}.");
        }

        [Command("gift")]
        [Alias("give")]
        [Summary("Gifts points to someone else")]
        public async Task Gift([Summary("User to gift points to")]IUser user, [Summary("Amount of points to gift")][Range(1, long.MaxValue)][HasEnoughPoints]long points)
        {
            if (Context.User == user)
                await ReplyAsync("You can't gift to yourself.");
            else
            {
                Points.ChangePoints(user, points);
                Points.ChangePoints(Context.User, -points);
                await ReplySuccessAsync();
            }     
        }

        [Command("leaderboard")]
        [Alias("top")]
        [Summary("Displays leaderboard")]
        public async Task Leaderboard([Summary("Amount of people you want to display")]int num = 100)
        {
            var ordered = Points.GetUsers(num);
            var i = 1;
            var result = string.Empty;

            for (var e = ordered.GetEnumerator(); e.MoveNext(); i++)
            {
                var row = $"{i}. <@{e.Current.Key}> - {e.Current.Value}{Points.CurrencyName}\n";
                if (result.Length + row.Length > 2000) break;
                result += row;
            }

            await ReplyAsync(result);
        }
    }
}
