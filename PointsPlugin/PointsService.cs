﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PointsPlugin.Services
{
    public class PointsService : ChipSaveableService<Dictionary<ulong, long>>, IConfigurableService<PointsConfiguration>
    {
        private readonly DiscordSocketClient _discord;

        private long PointsPerMin => ServiceConfigProvider.Config.PointsPerMinute;
        private long PointsMultiplier => ServiceConfigProvider.Config.PointsMultiplier;
        public string CurrencyName => ServiceConfigProvider.Config.CurrencyName;

        public ServiceConfigurationProvider<PointsConfiguration> ServiceConfigProvider { get; set; }

        public PointsService(DiscordSocketClient discord, ClockService clock, ConfigurationProvider config, LoggingService logger) : base(logger, config, "Points", "points.json")
        {
            _discord = discord;

            ServiceConfigProvider = ServiceConfigurationProvider<PointsConfiguration>.GetInstance(config);

            Data = Load().GetAwaiter().GetResult() ?? new Dictionary<ulong, long>();

            clock.MinuteTick += Clock_MinuteTickAsync;
            clock.FiveMinutesTick += Clock_FiveMinutesTick;
        }

        private Task Clock_FiveMinutesTick()
            => Save();

        private async Task Clock_MinuteTickAsync()
        {
            if (_discord.ConnectionState != ConnectionState.Connected) return;

            Log(LogSeverity.Verbose, "Increasing points");

            var guild = _discord.GetGuild(Config.Guilds.MainGuild);
            var pointsToGive = PointsPerMin * PointsMultiplier;

            await guild.DownloadUsersAsync();

            foreach (IGuildUser usr in guild.Users.ToList())
            {
                if (usr.Status == UserStatus.Online)
                {
                    if (Data.ContainsKey(usr.Id))
                    {
                        Data[usr.Id] += pointsToGive;
                    }
                    else
                    {
                        Data.Add(usr.Id, pointsToGive);
                    }
                }
            }

            var keysToRemove = Data.Keys.Except(guild.Users.Select(x => x.Id).ToList()).ToList();

            if (keysToRemove.Count > 0)
                Log(LogSeverity.Verbose, $"Removing: {string.Join(", ", keysToRemove)}");

            keysToRemove.ForEach(key => Data.Remove(key));

            Log(LogSeverity.Verbose, "Increased points");
        }

        public long GetPoints(ulong id)
            => Data.ContainsKey(id) ? Data[id] : 0;
        public long GetPoints(IUser user)
            => GetPoints(user.Id);

        public long ChangePoints(ulong id, long amount)
        {
            if (Data.ContainsKey(id))
                Data[id] += amount;
            else
                Data.Add(id, amount);

            Log(LogSeverity.Verbose, $"Changed {id} on {amount}. Current value {Data[id]}");

            return Data[id];
        }
        public long ChangePoints(IUser user, long amount)
            => ChangePoints(user.Id, amount);

        public long AwardPoints(ulong id, long amount)
            => ChangePoints(id, amount * PointsMultiplier);
        public long AwardPoints(IUser user, long amount)
            => ChangePoints(user.Id, amount * PointsMultiplier);

        public bool HasEnough(ulong id, long amount)
            => Data.ContainsKey(id) ? Data[id] >= amount : false;
        public bool HasEnough(IUser user, long amount)
            => HasEnough(user.Id, amount);

        public string GetFormatted(long points)
            => $"{points}{CurrencyName}";
        public string GetUserPointsFormatted(ulong id)
            => GetFormatted(GetPoints(id));
        public string GetUserPointsFormatted(IUser user)
            => GetFormatted(GetPoints(user.Id));

        public IOrderedEnumerable<KeyValuePair<ulong, long>> GetUsers()
            => Data.Where(u => u.Key != _discord.CurrentUser.Id).OrderByDescending(u => u.Value);

        public IEnumerable<KeyValuePair<ulong, long>> GetUsers(int n)
            => Data.Where(u => u.Key != _discord.CurrentUser.Id).OrderByDescending(x => x.Value).Take(n);
    }
}
