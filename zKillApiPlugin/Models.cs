﻿using ChipBot.Core.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zKillApiPlugin
{
    public class Entity<T>
    {
        [JsonProperty("dangerRatio")]
        public long DangerRatio { get; set; }

        [JsonProperty("gangRatio")]
        public long GangRatio { get; set; }

        [JsonProperty("hasSupers")]
        public bool HasSupers { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("iskDestroyed")]
        public long IskDestroyed { get; set; }

        [JsonProperty("iskLost")]
        public long IskLost { get; set; }

        [JsonProperty("months")]
        [JsonConverter(typeof(DictionaryToListConverter<MonthValue>))]
        public List<MonthValue> Months { get; set; }

        [JsonProperty("pointsDestroyed")]
        public long PointsDestroyed { get; set; }

        [JsonProperty("pointsLost")]
        public long PointsLost { get; set; }

        [JsonProperty("sequence")]
        public long Sequence { get; set; }

        [JsonProperty("shipsDestroyed")]
        public long ShipsDestroyed { get; set; }

        [JsonProperty("shipsLost")]
        public long ShipsLost { get; set; }

        [JsonProperty("soloKills")]
        public long SoloKills { get; set; }

        [JsonProperty("soloLosses")]
        public long SoloLosses { get; set; }

        [JsonProperty("supers")]
        public Supers Supers { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("info")]
        public T Info { get; set; }

        [JsonProperty("topLists")]
        [JsonConverter(typeof(TopListConverter))]
        public TopLists TopLists { get; set; }

        [JsonProperty("topIskKillIDs")]
        public List<long> TopIskKillIDs { get; set; }
    }

    public class MonthValue
    {
        [JsonProperty("year")]
        public long Year { get; set; }

        [JsonProperty("month")]
        public long Month { get; set; }

        [JsonProperty("shipsLost")]
        public long ShipsLost { get; set; }

        [JsonProperty("pointsLost")]
        public long PointsLost { get; set; }

        [JsonProperty("iskLost")]
        public long IskLost { get; set; }

        [JsonProperty("shipsDestroyed")]
        public long ShipsDestroyed { get; set; }

        [JsonProperty("pointsDestroyed")]
        public long PointsDestroyed { get; set; }

        [JsonProperty("iskDestroyed")]
        public long IskDestroyed { get; set; }
    }

    public abstract class EntityInfo
    {
        [JsonProperty("factionID")]
        public long FactionId { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("lastApiUpdate")]
        public LastApiUpdate LastApiUpdate { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class CharacterInfo : EntityInfo
    {
        [JsonProperty("allianceID")]
        public long AllianceID { get; set; }

        [JsonProperty("secStatus")]
        public double SecStatus { get; set; }
    }

    public abstract class MemberedEntity : EntityInfo
    {
        [JsonProperty("memberCount")]
        public long MemberCount { get; set; }

        [JsonProperty("ticker")]
        public string Ticker { get; set; }
    }

    public class CorporationInfo : MemberedEntity
    {
        [JsonProperty("allianceID")]
        public long AllianceID { get; set; }

        [JsonProperty("ceoID")]
        public long CeoID { get; set; }
    }

    public class AllianceInfo : MemberedEntity
    {
        [JsonProperty("corpCount")]
        public long CorpCount { get; set; }

        [JsonProperty("executorCorpID")]
        public long ExecutorCorpId { get; set; }
    }

    public class LastApiUpdate
    {
        [JsonProperty("sec")]
        public long Sec { get; set; }

        [JsonProperty("usec")]
        public long Usec { get; set; }
    }

    public class Supers
    {
        [JsonProperty("titans")]
        public Super Titans { get; set; }

        [JsonProperty("supercarriers")]
        public Super Supercarriers { get; set; }
    }

    public class Super
    {
        [JsonProperty("data")]
        public List<SuperData> Data { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }

    public class SuperData
    {
        [JsonProperty("kills")]
        public long Kills { get; set; }

        [JsonProperty("characterID")]
        public long CharacterId { get; set; }

        [JsonProperty("characterName")]
        public string CharacterName { get; set; }
    }

    public class TopLists
    {
        public List<TopCharacter> Characters { get; set; }
        public List<TopCorporation> Corporations { get; set; }
        public List<TopAlliance> Alliances { get; set; }
        public List<TopShip> Ships { get; set; }
        public List<TopSystem> Systems { get; set; }
    }

    public abstract class TopEntity
    {
        [JsonProperty("kills")]
        public int Kills { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class TopCharacter : TopEntity
    {
        [JsonIgnore]
        public long CharacterID => Id;
        [JsonIgnore]
        public string CharacterName => Name;
    }

    public class TopCorporation : TopEntity
    {
        [JsonIgnore]
        public long CorporationID => Id;
        [JsonIgnore]
        public string CorporationName => Name;
        [JsonProperty("cticker")]
        public string Ticker { get; set; }
    }

    public class TopAlliance : TopEntity
    {
        [JsonIgnore]
        public long AllianceID => Id;
        [JsonIgnore]
        public string AllianceName => Name;
        [JsonProperty("aticker")]
        public string Ticker { get; set; }
    }

    public class TopShip : TopEntity
    {
        [JsonIgnore]
        public long ShipTypeID => Id;
        [JsonIgnore]
        public string ShipName => Name;
        [JsonProperty("groupID")]
        public long GroupID { get; set; }
        [JsonProperty("groupName")]
        public string GroupName { get; set; }
    }

    public class TopSystem : TopEntity
    {
        [JsonIgnore]
        public long SolarSystemID => Id;
        [JsonIgnore]
        public string SolarSystemName => Name;
        [JsonProperty("sunTypeID")]
        public string SunTypeID { get; set; }
        [JsonProperty("solarSystemSecurity")]
        public string SolarSystemSecurity { get; set; }
        [JsonProperty("regionID")]
        public string RegionID { get; set; }
        [JsonProperty("regionName")]
        public string RegionName { get; set; }
        [JsonProperty("constellationID")]
        public string ConstellationID { get; set; }
        [JsonProperty("constellationName")]
        public string ConstellationName { get; set; }
    }

    public class Search
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }
    }

    public class TopListConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            => throw new NotImplementedException();

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            var jToken = JToken.Load(reader);

            var list = new TopLists()
            {
                Characters = jToken.Where(x => x["type"].Value<string>() == "character").First()["values"].Value<JArray>().ToObject<List<TopCharacter>>(),
                Corporations = jToken.Where(x => x["type"].Value<string>() == "corporation").First()["values"].Value<JArray>().ToObject<List<TopCorporation>>(),
                Alliances = jToken.Where(x => x["type"].Value<string>() == "alliance").First()["values"].Value<JArray>().ToObject<List<TopAlliance>>(),
                Ships = jToken.Where(x => x["type"].Value<string>() == "shipType").First()["values"].Value<JArray>().ToObject<List<TopShip>>(),
                Systems = jToken.Where(x => x["type"].Value<string>() == "solarSystem").First()["values"].Value<JArray>().ToObject<List<TopSystem>>()
            };

            return list;
        }

        public override bool CanConvert(Type objectType)
            => objectType == typeof(TopLists);

        public override bool CanRead
            => true;

        public override bool CanWrite
            => false;
    }

    public class KillMail
    {
        [JsonProperty("killmail_id")]
        public long KillmailId { get; set; }

        [JsonProperty("zkb")]
        public ZkbKillInfo Zkb { get; set; }
    }

    public class ZkbKillInfo
    {
        [JsonProperty("locationID")]
        public long LocationId { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("fittedValue")]
        public double FittedValue { get; set; }

        [JsonProperty("totalValue")]
        public double TotalValue { get; set; }

        [JsonProperty("points")]
        public long Points { get; set; }

        [JsonProperty("npc")]
        public bool Npc { get; set; }

        [JsonProperty("solo")]
        public bool Solo { get; set; }

        [JsonProperty("awox")]
        public bool Awox { get; set; }
    }

    public static class ZkbApiExtensions
    {
        public static async Task<Entity<CharacterInfo>> GetCharacterAsync(this IZkbApi api, string id)
            => JsonConvert.DeserializeObject<Entity<CharacterInfo>>(await api.GetAsync("character", id));
        public static async Task<Entity<CorporationInfo>> GetCorporationAsync(this IZkbApi api, string id)
            => JsonConvert.DeserializeObject<Entity<CorporationInfo>>(await api.GetAsync("corporation", id));
        public static async Task<Entity<AllianceInfo>> GetAllianceAsync(this IZkbApi api, string id)
            => JsonConvert.DeserializeObject<Entity<AllianceInfo>>(await api.GetAsync("alliance", id));

        public static async Task<IEnumerable<Search>> SearchCharacterAsync(this IZkbApi api, string query)
            => (await api.SearchAsync(query)).Where(x => x.Type == "character");
    }

    [Header("Accept-Encoding", "gzip")]
    public interface IZkbApi
    {
        [Header("User-Agent")]
        string UserAgent { get; set; }

        [Get("api/stats/{entityType}ID/{entityId}/")]
        Task<string> GetAsync([Path]string entityType, [Path]string entityId);

        [Get("api/killID/{id}/")]
        Task<List<KillMail>> GetKill([Path]long id);

        [Get("autocomplete/{query}/")] //not documented
        Task<List<Search>> SearchAsync([Path]string query);
    }
}
