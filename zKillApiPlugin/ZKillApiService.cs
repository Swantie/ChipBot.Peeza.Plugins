﻿using ChipBot.Core;
using ChipBot.Core.Services;
using RestEase;
using System;
using System.Net.Http;

namespace zKillApiPlugin.Services
{
    public class ZKillApiService : ApiService<IZkbApi>
    {
        private const string ApiUrl = "https://zkillboard.com/";

        public ZKillApiService(LoggingService log, ConfigurationProvider provider) : base(log, provider)
        {
            ApiClient = new RestClient(
                new HttpClient(
                    new HttpClientHandler()
                    {
                        AutomaticDecompression = System.Net.DecompressionMethods.GZip
                    })
                {
                    BaseAddress = new Uri(ApiUrl)
                }).For<IZkbApi>();

            ApiClient.UserAgent = UserAgent;
        }
    }
}
