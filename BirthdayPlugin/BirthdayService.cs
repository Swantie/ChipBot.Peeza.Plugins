﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BirthdayPlugin.Services
{
    public class BirthdayService : BackgroundSaveableService<Dictionary<int, Dictionary<int, List<ulong>>>>, IConfigurableService<BirthdayConfig>
    {
        private readonly ClockService _clockService;
        private readonly DiscordSocketClient _discord;

        private bool notifiedToday = false;
        private int day = DateTime.Now.Day;
        private bool isWorking = false;
        public override bool IsWorking => isWorking;

        public ServiceConfigurationProvider<BirthdayConfig> ServiceConfigProvider { get; set; }
        private SocketGuild MainGuild => _discord.GetGuild(Config.Guilds.MainGuild);
        public BirthdayService(LoggingService log, ConfigurationProvider provider, ClockService clockService, DiscordSocketClient discord) : base(log, provider, "Users", "birthdays.json")
        {
            _clockService = clockService;
            _discord = discord;

            ServiceConfigProvider = ServiceConfigurationProvider<BirthdayConfig>.GetInstance(provider);
            Data = Load().GetAwaiter().GetResult() ?? new Dictionary<int, Dictionary<int, List<ulong>>>();
        }

        public override async Task StartAsync()
        {
            isWorking = true;

            _clockService.HourTick += _clockService_HourTick;
            await _clockService_HourTick();
        }

        public override Task StopAsync()
        {
            isWorking = false;

            _clockService.HourTick -= _clockService_HourTick;

            return Task.CompletedTask;
        }

        private async Task _clockService_HourTick()
        {
            var date = DateTime.Now;

            if (notifiedToday && day == date.Day) return;
            if (date.Hour < 12) return;
            notifiedToday = true;
            day = date.Day;

            Log(LogSeverity.Verbose, "Checking birthdays");

            if (Data.ContainsKey(date.Month) && Data[date.Month].ContainsKey(date.Day))
            {
                var users = new List<ulong>(Data[date.Month][date.Day]);

                foreach (var id in users)
                {
                    var user = MainGuild.GetUser(id);
                    if (user == null) continue;

                    Log(LogSeverity.Info, $"Notifying {user.Nickname} ({id})");

                    if (ServiceConfigProvider.Config.NotifyChannel == 0)
                    {
                        await (await user.GetOrCreateDMChannelAsync())
                            .SendMessageAsync(string.Format(ServiceConfigProvider.Config.Message, user.Mention));
                    }
                    else
                    {
                        await ((ITextChannel)_discord.GetChannel(ServiceConfigProvider.Config.NotifyChannel))
                            .SendMessageAsync(string.Format(ServiceConfigProvider.Config.Message, user.Mention));
                    }

                    await Task.Delay(1000);
                }
            }

            Log(LogSeverity.Verbose, "Checked birthdays");
        }
    }
}
