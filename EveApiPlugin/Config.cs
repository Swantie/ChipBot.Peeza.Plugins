﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EveApiPlugin
{
    public class EveEsiConfig
    {
        [JsonProperty("uri")]
        public string Uri { get; set; } = "https://esi.evetech.net/";

        [JsonProperty("clientId")]
        public string ClientId { get; set; } = "";

        [JsonProperty("secretKey")]
        public string SecretKey { get; set; } = "";

        [JsonProperty("callbackUrl")]
        public string CallbackUrl { get; set; } = "";

        [JsonProperty("listener")]
        public string Listener { get; set; } = "http://127.0.0.1:8081/";

        [JsonProperty("scopes")]
        public List<string> Scopes { get; set; } = new List<string>();
    }
}
