﻿using ESI.NET.Models.SSO;
using Newtonsoft.Json;
using System;

namespace EveApiPlugin
{
    public class LinkedCharacter
    {
        [JsonProperty("token")]
        public SsoToken Token { get; set; }

        [JsonProperty("expriesOn")]
        public DateTime ExpiresOn { get; set; }

        [JsonProperty("characterName")]
        public string CharacterName { get; set; }
        
        [JsonProperty("characterId")]
        public int CharacterId { get; set; }
    }
}
