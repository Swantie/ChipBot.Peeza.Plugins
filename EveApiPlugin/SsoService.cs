﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using ESI.NET.Models.SSO;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EveApiPlugin.Services
{
    public class EveSsoService : BackgroundSaveableService<Dictionary<ulong, List<LinkedCharacter>>>
    {
        public override bool IsWorking => listener != null;

        private HttpListener listener;
        private readonly EveApiService _eveApi;

        private readonly MemoryCache _cache;

        public EveSsoService(LoggingService log, ConfigurationProvider provider, EveApiService eveApi, MemoryCache cache) : base(log, provider, "Eve", "SSO.json")
        {
            _eveApi = eveApi;
            _cache = cache;
        }

        public async override Task StartAsync()
        {
            listener = new HttpListener();

            listener.Prefixes.Add(_eveApi.ServiceConfigProvider.Config.Listener);

            Log(LogSeverity.Verbose, "Starting");
            listener.Start();
            _ = Listen();

            Data = await Load() ?? new Dictionary<ulong, List<LinkedCharacter>>();
        }

        public override Task StopAsync()
        {
            listener.Close();
            listener = null;
            Data = null;

            return Task.CompletedTask;
        }

        public string LinkCharacter(IUser user)
            => LinkCharacter(user.Id);
        public string LinkCharacter(ulong id)
        {
            return AddSalt(_eveApi.ApiClient.SSO.CreateAuthenticationUrl(_eveApi.ServiceConfigProvider.Config.Scopes).Replace(' ', '+'), id);
        }

        public async Task<List<AuthorizedCharacterData>> GetUserData(IUser user)
            => await GetUserData(user.Id);
        public async Task<List<AuthorizedCharacterData>> GetUserData(ulong id)
        {
            if (!Data.ContainsKey(id)) return null;

            var cacheKey = $"evesso_{id}";
            var cached = _cache.Get(cacheKey);
            if (cached != null) return (List<AuthorizedCharacterData>)cached;

            var characters = Data[id];
            var authed = new List<AuthorizedCharacterData>();
            var shouldSave = false;
            var apiCalls = 0;

            foreach (var character in characters)
            {
                if (DateTime.Now > character.ExpiresOn)
                {
                    Log(LogSeverity.Verbose, $"Refreshing token for {character.CharacterName}, userId: {id}");
                    character.Token = await _eveApi.ApiClient.SSO.GetToken(ESI.NET.Enumerations.GrantType.RefreshToken, character.Token.RefreshToken);
                    character.ExpiresOn = DateTime.Now.AddSeconds(character.Token.ExpiresIn);
                    apiCalls++;
                    shouldSave = true;
                }

                if (apiCalls == 3)
                {
                    await Task.Delay(1000);
                    apiCalls = 0;
                }

                Log(LogSeverity.Verbose, $"Verifying token for {character.CharacterName}, userId: {id}");
                authed.Add(await _eveApi.ApiClient.SSO.Verify(character.Token));
                apiCalls++;
            }

            if (shouldSave) await Save();

            _cache.Set(cacheKey, authed, DateTimeOffset.Now.AddHours(1));
            return authed;
        }

        public List<LinkedCharacter> GetLinkedCharacters(IUser user)
            => GetLinkedCharacters(user.Id);
        public List<LinkedCharacter> GetLinkedCharacters(ulong id)
        {
            if (!Data.ContainsKey(id) || !Data[id].Any()) return null;

            return new List<LinkedCharacter>(Data[id]);
        }

        public ulong GetUser(int characterId)
        {
            var user = Data.Where(kv => kv.Value.Select(c => c.CharacterId).Contains(characterId));

            return user.Any() ? user.First().Key : 0;
        }

        private string GetSaltedId(ulong id)
        {
            using (var hasher = new HMACSHA1(Encoding.UTF8.GetBytes(Config.DiscordToken)))
            {
                return Convert.ToBase64String(hasher.ComputeHash(BitConverter.GetBytes(id)));
            }
        }

        private string AddSalt(string authLink, ulong id)
        {
            return $"{authLink}&state={id}:{WebUtility.UrlEncode(GetSaltedId(id))}";
        }

        private async Task Listen()
        {
            while (listener != null)
            {
                var context = await listener.GetContextAsync();

                if (context != null)
                {
                    Log(LogSeverity.Verbose, $"Got {context.Request.HttpMethod} reqest {context.Request.QueryString}");
                    await ResponseHandler(context.Response, await RequestHandler(context.Request));
                }
            }
        }

        private async Task<bool> RequestHandler(HttpListenerRequest request)
        {
            var state = request.QueryString["state"];
            var code = request.QueryString["code"];
            if (code == null || state == null || !state.Contains(':'))
            {
                Log(LogSeverity.Verbose, $"Got request with code='{code}', user='{state}'");
                return false;
            }

            var split = state.Split(":");

            if (!ulong.TryParse(split[0], out ulong userId))
            {
                Log(LogSeverity.Verbose, $"Can't parse to ulong");
                return false;
            }

            if (GetSaltedId(userId) != split[1])
            {
                Log(LogSeverity.Verbose, $"Hash is not correct");
                return false;
            }

            try
            {
                var token = await _eveApi.ApiClient.SSO.GetToken(ESI.NET.Enumerations.GrantType.AuthorizationCode, code);
                await SaveToken(token, userId);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private async Task SaveToken(SsoToken token, ulong userId)
        {
            var character = await _eveApi.ApiClient.SSO.Verify(token);

            if (!Data.ContainsKey(userId))
            {
                Data.Add(userId, new List<LinkedCharacter>());
            }

            Data[userId].Add(new LinkedCharacter()
            {
                Token = token,
                ExpiresOn = character.ExpiresOn,
                CharacterName = character.CharacterName,
                CharacterId = character.CharacterID
            });

            await Save();
        }

        private async Task ResponseHandler(HttpListenerResponse response, bool ok)
        {
            response.StatusCode = ok ? 200 : 400;
            response.ContentType = "application/json";
            string responseString = "{ \"status\": \"" + (ok ? "success" : "error") + "\" }";
            byte[] buffer = Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            using (System.IO.Stream output = response.OutputStream)
            {
                await output.WriteAsync(buffer, 0, buffer.Length);
                await output.FlushAsync();
                output.Close();
                output.Dispose();
            }
        }
    }
}
