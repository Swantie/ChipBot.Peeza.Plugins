﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using ESI.NET;
using ESI.NET.Enumerations;
using ESI.NET.Models.Universe;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EveApiPlugin.Services
{
    public class EveApiService : ApiService<EsiClient>, IConfigurableService<EveEsiConfig>
    {
        private readonly MemoryCache _cache;

        public ServiceConfigurationProvider<EveEsiConfig> ServiceConfigProvider { get; set; }

        public EveApiService(LoggingService log, ConfigurationProvider provider, MemoryCache cache) : base(log, provider)
        {
            _cache = cache;
            ServiceConfigProvider = ServiceConfigurationProvider<EveEsiConfig>.GetInstance(provider);

            if (string.IsNullOrEmpty(ServiceConfigProvider.Config.SecretKey) || string.IsNullOrEmpty(ServiceConfigProvider.Config.ClientId)) throw new Exception("EveEsi: secretKey or clientId is null"); 

            IOptions<EsiConfig> config = Options.Create(new EsiConfig()
            {
                EsiUrl = ServiceConfigProvider.Config.Uri,
                DataSource = DataSource.Tranquility,
                ClientId = ServiceConfigProvider.Config.ClientId,
                SecretKey = ServiceConfigProvider.Config.SecretKey,
                CallbackUrl = ServiceConfigProvider.Config.CallbackUrl,
                UserAgent = UserAgent
            });

            ApiClient = new EsiClient(config);
        }


        private string GetCacheKey(long id) => $"namedEnt{id}";
        public async Task<Dictionary<long, ResolvedInfo>> GetNamedEntities(List<long> ids)
        {
            var knownEntities = new Dictionary<long, ResolvedInfo>();
            var unknownIds = new List<long>();

            foreach (var id in ids)
            {
                var info = _cache.Get(GetCacheKey(id));

                if (info != null)
                {
                    knownEntities.Add(id, (ResolvedInfo)info);
                }
                else
                {
                    unknownIds.Add(id);
                }
            }

            if (unknownIds.Any())
            {
                Log(LogSeverity.Info, $"Resolving names: {string.Join(", ", unknownIds)}");
                var response = await ApiClient.Universe.Names(unknownIds);

                if (response.Data != null)
                {
                    foreach (var resolved in response.Data)
                    {
                        knownEntities.Add(resolved.Id, resolved);
                        _cache.Set(GetCacheKey(resolved.Id), resolved, DateTimeOffset.Now.AddDays(1));
                    }
                }
            }

            return knownEntities;
        }
    }
}
