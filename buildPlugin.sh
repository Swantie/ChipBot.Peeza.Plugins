if  [[ ! "$1" ]]
then
	echo "No plugin name specified to build"
	exit 0
fi

if  [[ ! -d $1 ]]
then
	echo "No plugin found by name" $1
	exit 0
fi

echo "Building" $1

mkdir -p out
dotnet publish $1/

tar czf out/$1.tgz --directory=$1/bin/Debug/netcoreapp2.2 publish/
md5sum out/$1.tgz | awk '{ print $1 }' > out/$1.md5
