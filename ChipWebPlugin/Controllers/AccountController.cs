﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChipWebPlugin.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet("Login"), ResponseCache(NoStore = true)]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated) return Redirect("/Profile");
            return Challenge(new AuthenticationProperties() { RedirectUri = "/Profile" });
        }

        [HttpGet("Logout"), ResponseCache(NoStore = true)]
        public async Task<IActionResult> Logout()
        {
            await AuthenticationHttpContextExtensions.SignOutAsync(HttpContext);
            return Redirect("/");
        }
    }
}
