﻿using ChipBot.Core.Core.Models;
using ChipBot.Core.Services;
using ChipWebPlugin.Core;
using Discord;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChipWebPlugin.Controllers
{
    public class LogsController : ChipController
    {
        private static LoggingService _log;
        static LogsController()
        {
            _log = ChipWebService.GetService<LoggingService>();
        }

        protected void Log(LogSeverity severity, string msg, Exception ex = null)
            => _ = _log.LogAsync(new LogMessage(severity, GetType().Name, msg, ex));

        [HttpGet("ws/log")]
        public async Task<IActionResult> Connect()
        {
            if (!IsAuthenticated) return StatusCode(401);
            if (!ChipWebService.ChipConfiguration.Owners.Contains(DiscordUser.Id)) return StatusCode(403);

            if (HttpContext.WebSockets.IsWebSocketRequest)
            {
                var failCount = 0;

                var clientId = new string(DateTime.Now.Ticks.ToString().TakeLast(5).Select(c => c += (char)('a' - '0')).ToArray());

                Log(LogSeverity.Info, $"Got WebSocket request [{clientId}]");

                var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();

                Func<ChipLogMessage, Task> handler = async (log) =>
                {
                    try
                    {
                        if (webSocket == null) return;
                        await webSocket.SendAsync(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(log)), WebSocketMessageType.Text, true, CancellationToken.None);
                    }
                    catch { }
                };

                _log.OnLogDetailed += handler;

                var src = new CancellationTokenSource();
                var token = src.Token;
                var gotMsg = false;

                _ = Task.Run(async () =>
                {
                    while (webSocket != null)
                    {
                        await Task.Delay(1000 * 30);

                        if (webSocket == null) return;

                        if (!gotMsg)
                        {
                            Log(LogSeverity.Verbose, $"Missing ping from client[{clientId}]: {++failCount}");

                            if (failCount > 2)
                            {
                                Log(LogSeverity.Verbose, $"Closing connection[{clientId}] due to too many missed pings.");

                                try
                                {
                                    await webSocket.CloseAsync(WebSocketCloseStatus.PolicyViolation, "Missed too many pings", CancellationToken.None);
                                }
                                catch { }

                                webSocket?.Dispose();
                                webSocket = null;
                                _log.OnLogDetailed -= handler;
                            }
                        }
                        else
                        {
                            failCount = 0;
                            gotMsg = false;
                        }
                    }
                });

                while (webSocket != null)
                {
                    var buf = new byte[1024];
                    try
                    {
                        var res = await webSocket.ReceiveAsync(buf, token);

                        gotMsg = true;

                        if (res.CloseStatus.HasValue)
                        {
                            Log(LogSeverity.Verbose, $"Connection was closed by client[{clientId}]. Reason: {res.CloseStatusDescription}. Code: {res.CloseStatus.Value}");
                            await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Connection was closed by client", CancellationToken.None);

                            webSocket?.Dispose();
                            webSocket = null;
                            _log.OnLogDetailed -= handler;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log(LogSeverity.Verbose, $"Couldn't close or receive message [{clientId}]", ex);
                    }
                }

                return StatusCode(200);
            }
            else
            {
                return StatusCode(400);
            }
        }
    }
}
