﻿using ChipWebPlugin.Core;
using ChipWebPlugin.Pages;
using Discord.WebSocket;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PointsPlugin.Services;
using RafflePlugin;
using RafflePlugin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChipWebPlugin.Controllers
{
    public class PointsController : ChipController
    {
        private static PointsService _pointsService;
        private static RaffleStatsService _raffleService;
        static PointsController()
        {
            _pointsService = ChipWebService.GetService<PointsService>();
            _raffleService = ChipWebService.GetService<RaffleStatsService>();
        }

        [HttpGet("Leaderboard"), ResponseCache(NoStore = true)]
        public IActionResult Leaderboard()
        {
            return PartialView("LeaderboardPartial", new PointsModel());
        }

        [HttpGet("RaffleStats"), ResponseCache(NoStore = true)]
        public IActionResult RaffleStats()
        {
            return PartialView("RaffleStatsPartial", new PointsModel());
        }

        [HttpGet("UserPoints"), ResponseCache(NoStore = true)]
        public IActionResult UserPoints()
        {
            return Content($"<span>You Have: <span class=\"accent-text\" id=\"pts\">{_pointsService.GetPoints(DiscordUser)}</span>{_pointsService.CurrencyName}</span>", "text/html");
        }

        [HttpPost("Raffle.json"), ResponseCache(NoStore = true)]
        public IActionResult Raffle(long pts, bool manMode)
        {
            var userPts = _pointsService.GetPoints(DiscordUser);
            var res = new RaffleResponse()
            {
                CurrentPoints = userPts,
                Success = true
            };

            if (manMode) pts = userPts;

            if (pts < 10 || pts > userPts)
            {
                res.Success = false;
                res.Message = "Wrong points amount";
            }
            else
            {
                var result = _raffleService.DoRaffle(DiscordUser, pts, manMode);
                res.CurrentPoints = result.CurrentPoints;
                res.Message = $"<span>You've rolled <span class=\"accent-text\">{result.Rolled}</span>: <span class=\"{(result.Result == RaffleResultEnum.Lost ? "red-text" : "green-text")}\">{(result.Result == RaffleResultEnum.Lost ? "-" : "+")}";

                switch (result.Result)
                {
                    case RaffleResultEnum.Triple:
                    case RaffleResultEnum.Double:
                        res.Message += pts * (manMode ? 3 : 2);
                        break;
                    case RaffleResultEnum.Win:
                    case RaffleResultEnum.Lost:
                        res.Message += pts;
                        break;
                }

                res.Message += "</span></span>";
            }

            return Json(res);
        }
        private class RaffleResponse
        {
            [JsonProperty("currentPoints")]
            public long CurrentPoints { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }

            [JsonProperty("success")]
            public bool Success { get; set; }
        }
    }
}
