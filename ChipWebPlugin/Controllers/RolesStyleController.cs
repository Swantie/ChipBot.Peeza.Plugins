﻿using ChipWebPlugin.Core;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChipWebPlugin.Controllers
{
    public class RolesStyleController : Controller
    {
        [HttpGet("css/roles.css"), ResponseCache(Duration = 60 * 60)]
        public IActionResult Roles()
        {
            if (!User.Identity.IsAuthenticated) return StatusCode(401);
            return Content(HtmlHelper.GenerateCssForRoles(ChipWebService.Discord.GetGuild(ChipWebService.ChipConfiguration.Guilds.MainGuild).Roles), "text/css");
        }
    }
}
