using ChipWebPlugin.Middlewares;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using System.IO;

namespace ChipWebPlugin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddLogging(config =>
            {
                config.ClearProviders();
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddAuthentication(a =>
            {
                a.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                a.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                a.DefaultChallengeScheme = "Discord";
            })
            .AddCookie(c => c.LoginPath = new PathString("/"))
            .AddDiscord(d =>
            {
                d.AppId = ChipWebService.ChipConfiguration.DiscordId;
                d.AppSecret = ChipWebService.ChipConfiguration.DiscordSecret;
                d.CallbackPath = ChipWebService.ChipWebConfig.Config.RedirectUrl;
                d.SaveTokens = true;
                ChipWebService.ChipWebConfig.Config.Scopes.ForEach(s => d.Scope.Add(s));
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (ChipWebService.ChipConfiguration.Debug.DebugMode)
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), ChipWebService.ChipWebConfig.Config.RootPath, "wwwroot"))
            });

            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseWebSockets();

            app.UseMiddleware<RedirectMiddleware>();

            app.UseMvc();
        }
    }
}
