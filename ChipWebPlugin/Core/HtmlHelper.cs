﻿using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChipWebPlugin.Core
{
    public static class HtmlHelper
    {
        public static string IsSelected(this IHtmlHelper htmlHelper, string page, string area = "", string cssClass = "active")
        {
            var currentPage = htmlHelper.ViewContext.RouteData.Values["page"] as string ?? "";
            var currentArea = htmlHelper.ViewContext.RouteData.Values["area"] as string ?? "";

            return area == currentArea && currentPage == page ? cssClass : string.Empty;
        }

        private static string RoleToHex(IRole role)
        {
            return "#" + role.Color.RawValue.ToString("X").PadLeft(6, '0');
        }
        public static string RoleToHex(this IHtmlHelper htmlHelper, IRole role) => RoleToHex(role);

        private static string BuildSpan(string content, string classString = "", string styleString = "")
        {
            return $"<span" + (string.IsNullOrEmpty(classString) ? "" : $" class=\"{classString}\"") + (string.IsNullOrEmpty(styleString) ? "" : $" style=\"{styleString}\"") + ">" + content + "</span>";
        }

        public static HtmlString UsernameToSpan(this IHtmlHelper htmlHelper, SocketGuildUser user, bool colored)
        {
            var topRole = user.Roles.OrderByDescending(r => r.Position).First();

            var cls = colored ? $"rc{topRole.Position}" : "accent-text";

            return new HtmlString(BuildSpan(BuildSpan(user.Username, classString: cls) + BuildSpan("#" + user.Discriminator)));
        }

        private static string RoleToClass(IRole role, bool text = false)
        {
            return $"r{(text ? "c" : "")}{role.Position}";
        }
        public static string RoleToClass(this IHtmlHelper htmlHelper, IRole role) => RoleToClass(role);

        public static string GenerateCssForRoles(IEnumerable<IRole> roles)
        {
            var sb = new StringBuilder();

            foreach (var r in roles)
            {
                var hex = r.Color.RawValue == 0 ? "var(--text-accent-color)" : RoleToHex(r);
                var cls = RoleToClass(r);
                sb.Append($".{cls}{{border-color:{hex};}}");
                sb.Append($".{cls}>span{{background-color:{hex};}}");
                sb.Append($".{RoleToClass(r, true)}{{color:{hex};}}");
            }

            return sb.ToString();
        }
        public static HtmlString GenerateCssForRoles(this IHtmlHelper htmlHelper, IEnumerable<IRole> roles) => new HtmlString(GenerateCssForRoles(roles));

        public static string GetAvatarUrl(this IHtmlHelper htmlHelper, IUser user, ushort size = 256)
        {
            return user.GetAvatarUrl(size: size) ?? $"https://cdn.discordapp.com/embed/avatars/{user.DiscriminatorValue % 5}.png";
        }

        public static HtmlString GenerateMenuItem(this IHtmlHelper htmlHelper, string page, string label, string area = "")
        {
            return new HtmlString($"<div class=\"menu\"><a href=\"{area}{page}\"><div class=\"{IsSelected(htmlHelper, page, area)}\">{label}</div></a></div>");
        }
    }
}
