﻿using Discord.WebSocket;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChipWebPlugin.Core
{
    public class ChipController : Controller
    {
        public bool IsAuthenticated => User.Identity.IsAuthenticated;
        public SocketSelfUser BotUser => ChipWebService.Discord.CurrentUser;

        private SocketGuild _mainGuild = null;
        public SocketGuild MainGuild
        {
            get
            {
                if (_mainGuild != null) return _mainGuild;
                return ChipWebService.Discord.GetGuild(ChipWebService.ChipConfiguration.Guilds.MainGuild);
            }
        }
        private SocketGuild _devGuild = null;
        public SocketGuild DevGuild
        {
            get
            {
                if (_devGuild != null) return _devGuild;
                return ChipWebService.Discord.GetGuild(ChipWebService.ChipConfiguration.Guilds.DevGuild);
            }
        }
        private SocketGuildUser _discordUser = null;
        public SocketGuildUser DiscordUser
        {
            get
            {
                if (_discordUser == null && Request.HttpContext.Items.ContainsKey("discordGuildUser"))
                {
                    _discordUser = (SocketGuildUser)Request.HttpContext.Items["discordGuildUser"];
                }
                return _discordUser;
            }
        }

        public ChipController()
        {
        }
    }
}
