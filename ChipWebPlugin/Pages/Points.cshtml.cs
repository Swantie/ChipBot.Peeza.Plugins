using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChipWebPlugin.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PointsPlugin.Services;
using RafflePlugin.Services;

namespace ChipWebPlugin.Pages
{
    public class PointsModel : ChipPage
    {
        private static PointsService _pointsService;
        private static RaffleStatsService _raffleService;
        public PointsService PointsService => _pointsService;
        public RaffleStatsService RaffleService => _raffleService;

        static PointsModel()
        {
            _pointsService = ChipWebService.GetService<PointsService>();
            _raffleService = ChipWebService.GetService<RaffleStatsService>();
        }
        public void OnGet()
        {
        }
    }
}