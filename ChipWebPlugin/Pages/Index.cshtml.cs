﻿using ChipWebPlugin.Core;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ChipWebPlugin.Pages
{
    public class IndexModel : ChipPage
    {
        public void OnGet()
        {
            if (IsAuthenticated) { Response.Redirect("/Profile"); return; }
        }
    }
}
