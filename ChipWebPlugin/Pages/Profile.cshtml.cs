﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChipBot.Core.Models;
using ChipBot.Core.Services;
using ChipWebPlugin.Core;
using Discord.WebSocket;
using EveApiPlugin.Services;
using EveChipPlugin.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PointsPlugin.Services;
using RafflePlugin.Services;

namespace ChipWebPlugin.Pages
{
    public class ProfileModel : ChipPage
    {
        public SocketGuildUser RequestedUser { get; private set; }
        public TrackedUser TrackedUser { get; private set; }

        public PointsService PointsService { get; private set; }
        public RaffleStatsService RaffleService { get; private set; }

        public EveSsoService EveSsoService { get; private set; }
        public EveApiService EveApiService { get; private set; }
        public SkillQService EveSkillQService { get; private set; }

        public bool IsMe => RequestedUser == DiscordUser;

        public async Task<IActionResult> OnGet()
        {
            if (string.IsNullOrEmpty(Request.Query["id"]))
            {
                if (DiscordUser == null) return Redirect("/Users");
                RequestedUser = DiscordUser;
            }
            else
            {
                if (!ulong.TryParse(Request.Query["id"], out ulong uid) || MainGuild.GetUser(uid) == null) return Redirect("/Profile");

                RequestedUser = MainGuild.GetUser(uid);
            }

            var uts = ChipWebService.GetService<UserTrackingService>();
            TrackedUser = uts.GetUserInfo(RequestedUser.Id);

            PointsService = ChipWebService.GetService<PointsService>();
            RaffleService = ChipWebService.GetService<RaffleStatsService>();

            EveSsoService = ChipWebService.GetService<EveSsoService>();
            EveApiService = ChipWebService.GetService<EveApiService>();
            EveSkillQService = ChipWebService.GetService<SkillQService>();

            return await Task.FromResult(Page());
        }
    }
}