using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChipWebPlugin.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ChipWebPlugin.Pages
{
    public class LogsModel : ChipPage
    {
        public void OnGet()
        {
            if (!ChipWebService.ChipConfiguration.Owners.Contains(DiscordUser.Id)) HttpContext.Response.Redirect("/Profile");
        }
    }
}