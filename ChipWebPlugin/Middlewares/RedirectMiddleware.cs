﻿using Discord.WebSocket;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ChipWebPlugin.Middlewares
{
    public class RedirectMiddleware
    {
        private readonly RequestDelegate _next;

        private SocketGuild _mainGuild = null;
        public SocketGuild MainGuild
        {
            get
            {
                if (_mainGuild != null) return _mainGuild;
                return ChipWebService.Discord.GetGuild(ChipWebService.ChipConfiguration.Guilds.MainGuild);
            }
        }

        public RedirectMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var val = context.User.FindFirst(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value;
            SocketGuildUser user = null;
            if (val != null) user = MainGuild.GetUser(ulong.Parse(val));
            context.Items.Add("discordGuildUser", user);

            if (context.User.Identity.IsAuthenticated || context.Request.Path.Value == "/" || context.Request.Path.Value == "/Login")
            {
                await _next.Invoke(context);
            }
            else
            {
                context.Response.Redirect("/");
            }
        }
    }
}
