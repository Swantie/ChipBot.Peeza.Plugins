﻿using ChipBot.Core;
using ChipBot.Core.Configuration;
using ChipBot.Core.Services;
using Discord.WebSocket;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;

namespace ChipWebPlugin
{
    public class ChipWebService : ChipService, IConfigurableService<ChipWebConfig>
    {
        public static IServiceProvider ChipServiceProvider { get; private set; }
        private static ConfigurationProvider ChipConfigurationProvider { get; set; }
        public static Settings ChipConfiguration => ChipConfigurationProvider.Config;
        public static DiscordSocketClient Discord { get; private set; }

        public static ServiceConfigurationProvider<ChipWebConfig> ChipWebConfig { get; private set; }
        public ServiceConfigurationProvider<ChipWebConfig> ServiceConfigProvider { get => ChipWebConfig; set => ChipWebConfig = value; }

        public static T GetService<T>() => (T)ChipServiceProvider.GetService(typeof(T));

        public ChipWebService(LoggingService log, ConfigurationProvider provider, DiscordSocketClient discord, IServiceProvider serviceProvider) : base(log, provider)
        {
            ChipServiceProvider = serviceProvider;
            ChipConfigurationProvider = provider;
            Discord = discord;

            ServiceConfigProvider = ServiceConfigurationProvider<ChipWebConfig>.GetInstance(provider);

            WebHost.CreateDefaultBuilder().UseContentRoot(Path.Combine(Directory.GetCurrentDirectory(), ChipWebConfig.Config.RootPath))
                .UseStartup<Startup>().UseUrls($"http://0.0.0.0:{ServiceConfigProvider.Config.Port}/").Build().RunAsync();
        }
    }
}
