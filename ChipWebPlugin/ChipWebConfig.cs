﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ChipWebPlugin
{
    public class ChipWebConfig
    {
        [JsonProperty("redirectUrl")]
        public string RedirectUrl { get; set; }

        [JsonProperty("scopes")]
        public List<string> Scopes { get; set; }

        [JsonProperty("rootPath")]
        public string RootPath { get; set; }

        [JsonProperty("port")]
        public ushort Port { get; set; }
    }
}
