﻿(() => {
    var socket;

    var state = $("#wsstate");
    var holder = $("#logs");

    function updateState() {
        if (socket) {
            switch (socket.readyState) {
                case WebSocket.CLOSED:
                    state.text("Closed").addClass("red-text").removeClass("green-text");
                    break;
                case WebSocket.CLOSING:
                    state.text("Closing...").addClass("red-text").removeClass("green-text");
                    break;
                case WebSocket.CONNECTING:
                    state.text("Connecting...").removeClass("red-text").addClass("green-text");
                    break;
                case WebSocket.OPEN:
                    state.text("Connected").removeClass("red-text").addClass("green-text");
                    break;
                default:
                    state.text("Unknown WebSocket State: " + socket.readyState).addClass("red-text").removeClass("green-text");
                    break;
            }
        }
    }

    $(document).ready(() => {
        socket = new WebSocket((location.protocol == "https:" ? "wss://" : "ws://") + location.host + "/ws/log");

        setInterval(() => { if (socket.readyState == WebSocket.OPEN) socket.send("ping"); }, 1000 * 20);

        updateState();

        socket.onopen = updateState;
        socket.onclose = updateState;
        socket.onerror = updateState;

        socket.onmessage = function (event) {
            var data = JSON.parse(event.data);
            var row = $("<div class=\"flex\" />");

            if (data.severityId == 0) row.addClass("ls-0");
            row.append($("<div>" + moment(data.date).format("hh:mm:ss") + "</div>").addClass("lt"));
            row.append($("<div>" + data.severity + "</div>").addClass("ls-" + data.severityId).addClass("ls"));
            row.append($("<div>" + data.source + "</div>").addClass("lsrc"));

            var last = $("<div>" + (data.message ? data.message : data.exMessage) + "</div>").addClass("lm");
            row.append(last);

            if (data.exMessage) {
                row.addClass("ls-ex");

                var ex = $("<div class=\"flex col ls-exd\" />");
                last.append(ex);
                ex.append($("<span>Exception Message: " + data.exMessage + "</span>"));
                ex.append($("<span>Exception Source: " + data.exSource + "</span>"));
                ex.append($("<span>Stack Trace:</span>"));
                ex.append($("<div>" + data.exStack + "</div>").addClass("grid"));

                row.click(() => {
                    row.toggleClass("open");
                });
            }

            var shouldScroll = logs.scrollTop + logs.clientHeight == logs.scrollHeight;

            holder.append(row);

            if (shouldScroll) logs.scrollTop = logs.scrollHeight - logs.clientHeight;
        }
    });
})();