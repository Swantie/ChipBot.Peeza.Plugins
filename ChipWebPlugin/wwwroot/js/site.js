﻿$(function () {
    var body = $("body");

    if ($("header").length && $("#logged-as").length) {
        var logged = $("#logged-as");
        var popup = logged.children(".popup");

        body.click(function (e) {
            if (logged.is(e.target) || logged.has(e.target).length) {
                if (logged.is(e.target)) popup.toggleClass("invisible");
            } else {
                popup.addClass("invisible");
            }
        });
    }
});