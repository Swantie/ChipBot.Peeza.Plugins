﻿(() => {
    var leaderboard = $("#ldr-part");
    var raffle = $("#rfl-part");
    var pts = $("#pts-part");

    var log = $("#log");
    var err = $("#err");

    var pt = $("#pts");
    var input = $("#ptsIn");
    var raffleBtn = $("#raffle");
    var raffleAll = $("#raffleAll");

    function refreshPage() {
        leaderboard.load("/Leaderboard");
        raffle.load("/RaffleStats");
        pts.load("/UserPoints");
    }

    var f = () => {
        setTimeout(() => {
            refreshPage();

            f();
        }, 1000 * 20);
    }

    f();

    function doRaffle(points, manMode) {
        $.ajax({
            type: "POST",
            url: "Raffle.json",
            data: {
                pts: points,
                manMode: manMode
            },
            success: (r) => {
                if (r.success) {
                    refreshPage();

                    var ch = log.children();

                    if (ch.length == 6) ch.first().remove();
                    log.append($(r.message));
                } else {
                    pt.text(r.currentPoints);
                    err.text(r.message);
                }
            }
        });
    }

    raffleBtn.click(() => {
        var p = parseInt(input.val());
        if (!p) return;

        doRaffle(p, false);
    });

    raffleAll.click(() => {
        doRaffle(0, true);
    });

    input.keyup(() => {
        err.empty();
    });
})();