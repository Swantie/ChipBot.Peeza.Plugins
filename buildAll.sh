rm -rf out/
mkdir -p out/

for d in */ ; do
	if [[ "$d" = "out/" ]]; then continue; fi
	bash buildPlugin.sh "${d::-1}"
done
