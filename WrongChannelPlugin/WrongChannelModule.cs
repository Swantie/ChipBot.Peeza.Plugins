﻿using ChipBot.Core;
using ChipBot.Core.Preconditions;
using Discord.Commands;
using System.Threading.Tasks;
using WrongChannelPlugin.Services;

namespace WrongChannelPlugin.Modules
{
    [Name("Wrong Channel")]
    [Group("wrongchannel")]
    [Alias("wrong")]
    public class WrongChannelModule : ChipModuleBase
    {
        public WrongChannelService WrongChannel { get; set; }

        private async Task<bool> IsAllowedAsync()
        {
            if (!WrongChannel.IsUserAllowed(Context.User))
            {
                await ReplyFailAsync();
                return false;
            }

            return true;
        }

        [Command]
        [Summary("Puts a picture")]
        public async Task Default()
        {
            if (!await IsAllowedAsync())
                return;

            await Context.Message.DeleteAsync();
            await ReplyAsync(WrongChannel.GetPicture());
        }

        [Command("add")]
        [Summary("Adds a picture")]
        [RequireChannel]
        public async Task AddPicture([Summary("Picture url")]string uri)
        {
            if (!await IsAllowedAsync())
                return;

            await WrongChannel.AddPicture(uri);
            await ReplySuccessAsync();
        }

        [Command("remove")]
        [Summary("Removes picture")]
        [RequireChannel]
        public async Task RemovePicture([Summary("Picture url")]string uri)
        {
            if (!await IsAllowedAsync())
                return;

            await WrongChannel.RemovePicture(uri);
            await ReplySuccessAsync();
        }

        [Command("remove")]
        [Summary("Removes picture")]
        [RequireChannel]
        public async Task RemovePicture([Summary("Picture id")]int id)
        {
            if (!await IsAllowedAsync())
                return;

            await WrongChannel.RemovePicture(id);
            await ReplySuccessAsync();
        }
    }
}
