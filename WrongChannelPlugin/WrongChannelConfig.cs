﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WrongChannelPlugin
{
    public class WrongChannelConfig
    {
        [JsonProperty("allowedUsers")]
        public List<ulong> AllowedUsers { get; set; }
    }
}
