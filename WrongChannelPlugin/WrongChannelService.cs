﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WrongChannelPlugin.Services
{
    public class WrongChannelService : ChipSaveableService<List<string>>, IConfigurableService<WrongChannelConfig>
    {
        private readonly DiscordSocketClient _discord;
        private readonly Random _rng;

        private List<ulong> AllowedUsers => ServiceConfigProvider.Config.AllowedUsers;

        public ServiceConfigurationProvider<WrongChannelConfig> ServiceConfigProvider { get; set; }

        public WrongChannelService(DiscordSocketClient discord, LoggingService log, ConfigurationProvider config, Random rng) : base(log, config, "WrongChannel", "pictures.json")
        {
            _discord = discord;
            _rng = rng;

            ServiceConfigProvider = ServiceConfigurationProvider<WrongChannelConfig>.GetInstance(config);
            Data = Load().GetAwaiter().GetResult() ?? new List<string>();
        }

        public bool IsUserAllowed(IUser user)
        {
            return Config.Owners.Contains(user.Id) || AllowedUsers.Contains(user.Id);
        }

        public async Task AddPicture(string uri)
        {
            if (!Data.Contains(uri))
                Data.Add(uri);

            await Save();
        }

        public async Task RemovePicture(int id)
        {
            if (Data.Count > id)
                Data.RemoveAt(id);

            await Save();
        }

        public async Task RemovePicture(string uri)
        {
            if (Data.Contains(uri))
                Data.Remove(uri);

            await Save();
        }

        public string GetPicture()
        {
            return Data.Any() ? Data[_rng.Next(0, Data.Count)] : "<place for child cop image>";
        }
    }
}
