﻿using ChipBot.Core;
using Discord.Commands;
using RPGPlugin.Services;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGPlugin.Module
{
    [Name("RPG")]
    [Group("rpg")]
    [RequireContext(ContextType.Guild)]
    public class RPGModule : ChipModuleBase
    {
        public RpgCoreService RpgCore { get; set; }
        public HeroProviderService HeroProvider { get; set; }

        [Command("classes")]
        [Summary("Displays a list of all available classes, or info about specified class")]
        [Alias("class", "specs", "spec")]
        public async Task Classes([Summary("Class name")]string name = null)
        {
            var classesTree = "Available classes:\n```\n" +
                "                        ┌─ Crusader\n" +
                "         ┌─ Guardian ───┼─ Vanguard\n" +
                "Warrior ─┤              └─ Paladin\n" +
                "         └─ Berserk ────┬─ Juggernaut\n" +
                "                        └─ Gladiator\n" +
                "                        ┌─ Death Knight\n" +
                "         ┌─ BattleMage ─┴─ Templar\n" +
                "Mage ────┼─ Wizard ───┬─── Necromancer\n" +
                "         └─ Cleric ─┐ └─── Arch Wizard\n" +
                "                    ├───── Enchanter\n" +
                "                    └───── Priest\n" +
                "                      ┌─── Hunter\n" +
                "Rogue ───┬─ Archer ───┴─── Ranger\n" +
                "         └─ Assassin ─┬─── NightBlade\n" +
                "                      └─── Cult Assassin\n" +
                "```\n" +
                $"Use `{Config.Prefix}rpg class rogue` to get information about specific class.";

            if (name == null)
            {
                await ReplyAsync(classesTree);
            }

            if (!RpgCore.Classes.ContainsKey(name))
            {
                await ReplyAsync("No class with specified name found.\n" + classesTree);
            }
            else
            {
                var desc = RpgCore.Classes[name];

                var sb = new StringBuilder();
                sb.AppendLine($"`{desc.Name.ToString()}`");
                if (!desc.BaseClass)
                {
                    sb.AppendLine($"Previous class: `{desc.AdvancedOf.Value.ToString()}`");
                }
                var next = RpgCore.Classes.Values.Where(c => c.AdvancedOf.HasValue && c.AdvancedOf.Value == desc.Name);
                if (next.Any())
                {
                    sb.AppendLine($"Can be advanced to: {string.Join(", ", next.Select(n => $"`{n}`"))}");
                }
                sb.AppendLine($"Bonus stats: {desc.StatsBonuses.ToStringShort(", ")}");
                sb.AppendLine($"Level needed: `{desc.LevelNeeded}`");
                sb.AppendLine($"Description: `{desc.Description}`");
                await ReplyAsync(sb.ToString());
            }
        }

        [Command("races")]
        [Summary("Displays a list of all available races, or info about specified race")]
        [Alias("race")]
        public async Task Races([Summary("Race name")]string name = null)
        {
            var racesTree = "Available races: " + string.Join(", ", RpgCore.Races.Values.Select(r => $"`{r.Name}`"));

            if (name == null)
            {
                await ReplyAsync(racesTree);
            }

            if (!RpgCore.Races.ContainsKey(name))
            {
                await ReplyAsync("No race with specified name found.\n" + racesTree);
            }
            else
            {
                var desc = RpgCore.Races[name];

                var sb = new StringBuilder();
                sb.AppendLine($"`{desc.Name.ToString()}`");
                sb.AppendLine($"Bonus stats: {desc.StatsBonuses.ToStringShort(", ")}");
                sb.AppendLine($"Description: `{desc.Description}`");
                await ReplyAsync(sb.ToString());
            }
        }

        [Command("createhero")]
        [Summary("Creates a hero")]
        public async Task CreateHero([Summary("Hero name")]string name, [Summary("Race name")]string race, [Summary("Class name")]string spec, [Summary("Avatar URL")]string avatar = null)
        {
            if (HeroProvider.GetHero(Context.User) != null)
            {
                await ReplyAsync("You have a hero already");
                return;
            }

            if (!RpgCore.Races.ContainsKey(race))
            {
                await Races(race);
                return;
            }

            if (!RpgCore.Classes.ContainsKey(spec))
            {
                await Classes(spec);
                return;
            }

            await HeroProvider.CreateHero(Context.User.Id, name, avatar, RpgCore.Races[race], RpgCore.Classes[spec]);
            await ReplySuccessAsync();
        }
    }
}
