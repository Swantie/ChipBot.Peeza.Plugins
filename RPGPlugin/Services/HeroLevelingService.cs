﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using RPGPlugin.Core.Classes;

namespace RPGPlugin.Services
{
    public class HeroLevelingService : ChipService
    {
        private readonly RpgCoreService _core;

        public HeroLevelingService(RpgCoreService core, LoggingService log, ConfigurationProvider provider) : base(log, provider)
        {
            _core = core;
        }

        public void GiveExp(Hero hero, ulong exp)
        {
            var curExp = hero.Experience;
            var curLvl = hero.Level;

            var amplExpDbl = (exp * (_core.BaseExpMultiplier + _core.ExpPerReborn * hero.RebornLevel));
            var amplExp = amplExpDbl >= ulong.MaxValue ? ulong.MaxValue : (ulong)amplExpDbl;

            hero.Experience += amplExp;

            if (hero.Experience < curExp)
            {
                hero.Experience = ulong.MaxValue;
            }

            while (hero.ExperienceNeeded <= hero.Experience && hero.Level < _core.MaxLevel)
            {
                hero.ExperienceNeeded += (ulong)(hero.ExperienceNeeded * _core.ExpCurve);
                hero.Level++;
                hero.AttributePoints += _core.AttributesPerLevel;
            }

            Log(LogSeverity.Info, $"Hero {hero.Name} (id:{hero.UserID}) got {exp}({amplExp})exp. {curExp}exp ({curLvl}lvl) -> {hero.Experience}exp ({hero.Level}lvl).");
        }

        public void RefreshLevel(Hero hero)
        {
            if (hero.ExperienceNeeded == 0) hero.ExperienceNeeded = _core.ExpNeeded;

            while (hero.ExperienceNeeded <= hero.Experience && hero.Level < _core.MaxLevel)
            {
                hero.ExperienceNeeded += (ulong)(hero.ExperienceNeeded * _core.ExpCurve);
                hero.Level++;
            }
        }
    }
}
