﻿using ChipBot.Core;
using ChipBot.Core.Services;
using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Config;
using RPGPlugin.Core.Static.Races;
using RPGPlugin.Core.Static.Specs;
using System;
using System.Collections.Generic;

namespace RPGPlugin.Services
{
    public class RpgCoreService : ChipService, IConfigurableService<RPGCoreConfiguration>
    {
        public double BaseDropChanceBonus => ServiceConfigProvider.Config.BaseDropChanceBonus;
        public double DropChancePerReborn => ServiceConfigProvider.Config.DropChancePerReborn;

        public ulong ExpNeeded => ServiceConfigProvider.Config.ExpNeeded;
        public double ExpCurve => ServiceConfigProvider.Config.ExpCurve;
        public uint AttributesPerLevel => ServiceConfigProvider.Config.AttributesPerLevel;
        public uint MaxLevel => ServiceConfigProvider.Config.MaxLevel;
        public double BaseExpMultiplier => ServiceConfigProvider.Config.BaseExpMultiplier;
        public double ExpPerReborn => ServiceConfigProvider.Config.ExpPerReborn;

        public uint MaxPartySize => ServiceConfigProvider.Config.MaxPartySize;
        public uint MinLevelForParty => ServiceConfigProvider.Config.MinLevelForParty;

        public Dictionary<string, SpecDescription> Classes { get; private set; }
        public Dictionary<string, RaceDescription> Races { get; private set; }
        public ServiceConfigurationProvider<RPGCoreConfiguration> ServiceConfigProvider { get; set; }

        public RpgCoreService(LoggingService log, ConfigurationProvider provider) : base(log, provider)
        {
            ServiceConfigProvider = ServiceConfigurationProvider<RPGCoreConfiguration>.GetInstance(provider);

            Classes = new Dictionary<string, SpecDescription>(StringComparer.InvariantCultureIgnoreCase);
            SpecsDescriptions.PopulateList(Classes);
            Races = new Dictionary<string, RaceDescription>(StringComparer.InvariantCultureIgnoreCase);
            RaceDescriptions.PopulateList(Races);
        }
    }
}
