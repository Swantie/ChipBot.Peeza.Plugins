﻿using ChipBot.Core;
using ChipBot.Core.Services;
using RPGPlugin.Core.Classes;
using System;

namespace RPGPlugin.Services
{
    public class ItemProviderService : ChipService
    {
        private RpgCoreService _coreService;
        private Random _rng;
        public ItemProviderService(LoggingService log, ConfigurationProvider provider, RpgCoreService coreService, Random random) : base(log, provider)
        {
            _coreService = coreService;
            _rng = random;
        }

        public Item GenerateItem(int ilvl, double dropPerc)
        {
            return null;
        }
    }
}
