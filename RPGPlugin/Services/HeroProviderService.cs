﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPGPlugin.Services
{
    public class HeroProviderService : ChipSaveableService<Dictionary<ulong, Hero>>
    {
        private HeroLevelingService _levelingService;
        private RpgCoreService _coreService;

        public HeroProviderService(LoggingService log, ConfigurationProvider provider, HeroLevelingService levelingService, RpgCoreService coreService) : base(log, provider, "RPG", "Heroes")
        {
            _levelingService = levelingService;
            _coreService = coreService;

            Data = Load().GetAwaiter().GetResult() ?? new Dictionary<ulong, Hero>();

            if (Data.Any())
            {
                foreach(var hero in Data.Values)
                {
                    LoadRaces(hero);
                    LoadSpecs(hero);
                    _levelingService.RefreshLevel(hero);
                }
            }
        }

        public async Task<Hero> CreateHero(ulong id, string name, string avatar, RaceDescription race, SpecDescription spec)
        {
            var hero = new Hero()
            {
                ExperienceNeeded = _coreService.ExpNeeded,
                Name = name,
                Avatar = avatar,
                RaceDescription = race,
                RaceReference = (Race)Activator.CreateInstance(race.Race),
                SpecDescription = spec,
                SpecReference = (Spec)Activator.CreateInstance(spec.Spec),
                UserID = id
            };

            Data.Add(id, hero);
            await Save();

            return hero;
        }

        public Hero GetHero(IUser user) => GetHero(user.Id);
        public Hero GetHero(ulong userId)
        {
            return Data.ContainsKey(userId) ? Data[userId] : null;
        }

        private void LoadSpecs(Hero hero)
        {
            hero.RaceDescription = _coreService.Races[hero.Race.ToString()];
            hero.RaceReference = (Race)Activator.CreateInstance(hero.RaceDescription.Race);
        }

        private void LoadRaces(Hero hero)
        {
            hero.RaceDescription = _coreService.Races[hero.Race.ToString()];
            hero.RaceReference = (Race)Activator.CreateInstance(hero.RaceDescription.Race);
        }
    }
}
