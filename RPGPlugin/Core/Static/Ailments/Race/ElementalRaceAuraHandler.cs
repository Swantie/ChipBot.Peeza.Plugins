﻿using RPGPlugin.Core.Classes;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool ElementalRaceAuraHandler(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            foreach (var p in args.Party.Where(p => p.IsAlive))
            {
                p.Buffs.Add(new Buff() {
                    Amplify = true,
                    Negative = false,
                    TurnsLeft = 1,
                    Type = BuffType.Attack,
                    Value = 1.5 + hero.HeroReference.Level / 100
                });
            }
            return UniqueHandler(args, null);
        }
    }
}
