﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool NagaRacePassiveHandler(BattleEventArgs args)
        {
            return UniqueHandler(args, new List<Buff>() {
                new Buff() { Amplify = true, Negative = true, TurnsLeft = 1, Type = BuffType.Defense, Value = 0 },
                new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.DodgeChance, Value = args.Self.Defense / 100.0 }
            });
        }
    }
}
