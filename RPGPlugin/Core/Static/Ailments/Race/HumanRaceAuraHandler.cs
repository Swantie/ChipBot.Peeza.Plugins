﻿using RPGPlugin.Core.Classes;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool HumanRaceAuraHandler(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            foreach (var p in args.Party.Where(p => p.IsAlive))
            {
                p.Buffs.Add(new Buff() {
                    Amplify = false,
                    Negative = false,
                    TurnsLeft = 1,
                    Type = BuffType.Accuracy,
                    Value = 0.1
                });
            }
            return UniqueHandler(args, null);
        }
    }
}
