﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool DwarfRacePassiveHandler(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            return UniqueHandler(args, new List<Buff>() {
                new Buff() { Amplify = true, Negative = true, TurnsLeft = 1, Type = BuffType.DodgeChance, Value = 0 },
                new Buff() { Amplify = true, Negative = true, TurnsLeft = 1, Type = BuffType.CritChance, Value = 0.5 },
                new Buff() { Amplify = true, Negative = true, TurnsLeft = 1, Type = BuffType.CritDamage, Value = 1.25 + hero.HeroReference.Level / 100 }
            });
        }
    }
}
