﻿using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private readonly static AilmentType[] AilmentsOrder =
        {
            AilmentType.NagaRacePassive,
            AilmentType.DwarfRacePassive,
            AilmentType.ElementalRaceAura,
            AilmentType.HumanRaceAura,

            AilmentType.JuggernautSpecPassive,

            AilmentType.Bleed,
            AilmentType.Poison,
            AilmentType.Stun
        };
        private readonly static Dictionary<AilmentType, Func<BattleEventArgs, bool>> Handlers = new Dictionary<AilmentType, Func<BattleEventArgs, bool>>
        {
            { AilmentType.NagaRacePassive, NagaRacePassiveHandler },
            { AilmentType.DwarfRacePassive, DwarfRacePassiveHandler },
            { AilmentType.ElementalRaceAura, ElementalRaceAuraHandler  },
            { AilmentType.HumanRaceAura, HumanRaceAuraHandler },

            { AilmentType.JuggernautSpecPassive, JuggernautSpecPassiveHandler },

            { AilmentType.Bleed, BleedHandler },
            { AilmentType.Poison, PoisonHandler},
            { AilmentType.Stun, StunHandler }
        };

        public static bool MainHandler(BattleEventArgs args)
        {
            bool skipTurn = false;

            var clone = (BattleEventArgs)args.Clone();
            var ailments = new List<Ailment>(args.Self.Ailments);

            foreach (var type in AilmentsOrder)
            {
                var typedAilments = ailments.Where(a => a.Type == type);

                if (!typedAilments.Any())
                {
                    continue;
                }

                clone.Data = typedAilments;

                skipTurn |= Handlers[type](clone);

                if (!clone.Self.IsAlive)
                {
                    break;
                }
            }

            if (!skipTurn && !args.Flag)
            {
                args.Self.OnPreAttack.Raise(args);
            }

            args.Self.ProcessBuffs.Raise(args);

            return true;
        }
    }
}
