﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool StunHandler(BattleEventArgs args)
        {
            var ailments = (IEnumerable<Ailment>)args.Data;
            var strongest = ailments.OrderByDescending(a => a.Power).First();

            bool skipTurn = true;

            if (args.Rng.NextDouble() < args.Self.BlockChance - strongest.Power)
            {
                skipTurn = false;
                args.Log($"{args.Self.Name} resists being stunned.");
            }
            else
            {
                args.Log($"{args.Self.Name} is still stunned.");
            }

            strongest.TurnsLeft--;

            if (strongest.TurnsLeft == 0)
            {
                args.Self.Ailments.RemoveAll(a => a.Type == AilmentType.Stun);
            }
            else
            {
                args.Self.Ailments.RemoveAll(a => a.Type == AilmentType.Stun && a != strongest);
            }

            return skipTurn;
        }
    }
}
