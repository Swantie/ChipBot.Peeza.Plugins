﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool BleedHandler(BattleEventArgs args)
        {
            var ailments = (IEnumerable<Ailment>)args.Data;
            var strongest = ailments.OrderByDescending(a => a.Power).First();

            var dmg = (long)(args.Self.Health.Value * strongest.Power);

            args.Self.Health.Value -= dmg;
            args.Log($"{args.Self.Name} is bleeding for {dmg}dmg.");

            bool skipTurn = false;

            args.Target = strongest.Inflicter;
            args.Self.OnHealthChange.Raise(args);
            if (!args.Self.IsAlive)
            {
                args.Flag = true;
                skipTurn = true;
            }

            foreach (var a in ailments)
            {
                a.TurnsLeft--;
            }

            args.Self.Ailments.RemoveAll(a => a.Type == AilmentType.Bleed && a.TurnsLeft == 0);

            return skipTurn;
        }
    }
}
