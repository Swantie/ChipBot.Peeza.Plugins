﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool UniqueHandler(BattleEventArgs args, List<Buff> buffs, bool perInflicter = false)
        {
            var ailments = new List<Ailment>((IEnumerable<Ailment>)args.Data);

            List<Ailment> safe = new List<Ailment>();

            if (perInflicter)
            {
                foreach(var ail in ailments)
                {
                    if (safe.Any(a => a.Inflicter == ail.Inflicter)) continue;
                    safe.Add(ail);
                }
            }
            else
            {
                safe.Add(ailments[0]);
            }

            foreach (var ail in ailments)
            {
                if (safe.Contains(ail)) continue;
                args.Self.Ailments.Remove(ail);
            }

            var ailment = ailments.First();

            if (buffs != null)
            {
                ailment.Buffs = buffs;
            }

            return false;
        }
    }
}
