﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool PoisonHandler(BattleEventArgs args)
        {
            var ailments = (IEnumerable<Ailment>)args.Data;
            var strongest = ailments.OrderByDescending(a => a.Power).First();

            args.Self.Health.Value -= (long)strongest.Power;
            args.Log($"Poison deals {(long)strongest.Power}dmg to {args.Self.Name}.");

            bool skipTurn = false;

            args.Target = strongest.Inflicter;
            args.Self.OnHealthChange.Raise(args);
            if (!args.Self.IsAlive)
            {
                args.Flag = true;
                skipTurn = true;
            }

            strongest.TurnsLeft--;

            args.Self.Ailments.RemoveAll(a => a.Type == AilmentType.Poison && a.TurnsLeft == 0);

            return skipTurn;
        }
    }
}
