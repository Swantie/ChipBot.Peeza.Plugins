﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static
{
    public static partial class AilmentsHandler
    {
        private static bool JuggernautSpecPassiveHandler(BattleEventArgs args)
        {
            var buff = 1.0 / args.Self.Health.Percentage / 2.0;
            var buffList = new List<Buff>();

            if (buff > 2.5) buff = 2.5;
            if (buff > 1.0) buffList.Add(new Buff() { Amplify = true, Negative = true, TurnsLeft = 1, Type = BuffType.Attack, Value = buff });

            return UniqueHandler(args, buffList);
        }
    }
}
