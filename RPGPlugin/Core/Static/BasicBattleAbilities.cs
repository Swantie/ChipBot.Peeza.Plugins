﻿using ChipBot.Core.Extensions;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static class BasicBattleAbilities
    {
        public static bool ProcessBuffs(BattleEventArgs args)
        {
            args.Self.Buffs.ForEach(x => x.TurnsLeft--);
            args.Self.Buffs.RemoveAll(x => x.TurnsLeft < 0);

            return true;
        }
        public static bool OnPreAttackCheckTarget(BattleEventArgs args)
        {
            if (args.Target == null || !args.Target.IsAlive)
            {
                var newTarget = args.Self.PickTarget(args);
                args.Log($"{args.Self.Name} can't pick any target.");
                if (newTarget == null) return true;

                args.Target = newTarget;
            }

            return false;
        }
        public static bool OnPreAttack(BattleEventArgs args)
        {
            bool Hit = args.Rng.NextDouble() < args.Self.Accuracy;

            args.Target.OnPreGetAttack.Raise(args.Reverse());

            if (Hit)
            {
                args.Self.OnAttack.Raise(args);
            }
            else
            {
                args.Self.OnMiss.Raise(args);
            }

            return true;
        }

        public static bool OnMiss(BattleEventArgs args)
        {
            args.Log($"{args.Self.Name} misses completely.");
            return true;
        }

        public static bool OnAttack(BattleEventArgs args)
        {
            for (var attack = 0; attack < args.Self.NumberOfAttacks; attack++)
            {
                var targets = new List<Creature>
                {
                    args.Target
                };

                targets.AddRange(args.Enemies.Except(targets).OrderRandomly(args.Rng).Take(args.Self.NumberOfTargets - 1));

                for (var target = 0; target < targets.Count; target++)
                {
                    bool Crit = args.Rng.NextDouble() < args.Self.CritChance;

                    var attackArgs = args.Reverse();
                    attackArgs.Self = targets[target];
                    attackArgs.Data = args.Self.Attack;

                    if (Crit)
                    {
                        args.Self.OnCrit.Raise(attackArgs.Reverse(true));
                    }
                    else
                    {
                        attackArgs.Self.OnGetAttack.Raise(attackArgs);
                    }
                }
            }

            return true;
        }

        public static bool OnCrit(BattleEventArgs args)
        {
            args.Flag = true;
            args.Data = (long)((long)args.Data * args.Self.CritDamage);

            args.Log($"{args.Self.Name} critically strikes.");

            args.Target.OnGetAttack.Raise(args.Reverse(true));

            return true;
        }

        public static bool OnGetAttackHealth(BattleEventArgs args)
        {
            args.Self.Health.Value -= (long)args.Data;
            args.Log($"{args.Target.Name} deals {(long)args.Data}dmg to {args.Self.Name}.");

            args.Self.OnHealthChange.Raise(args);

            args.Target.OnAfterAttack.Raise(args.Reverse(true));
            return true;
        }

        public static bool OnGetAttackShield(BattleEventArgs args)
        {
            if (args.Self.Shield.Value == 0)
            {
                return false;
            }

            var dmg = (long)args.Data - args.Self.Shield.Value;

            args.Self.Shield.Value -= (long)args.Data;
            args.Log($"{args.Target.Name} deals {(long)args.Data}dmg to {args.Self.Name}'s shields.");

            args.Data = dmg;

            if (dmg <= 0)
            {
                args.Target.OnAfterAttack.Raise(args.Reverse(true));
                return true;
            }

            return false;
        }

        public static bool OnGetAttack(BattleEventArgs args)
        {
            bool Dodge = args.Rng.NextDouble() < args.Self.DodgeChance;
            bool Block = args.Rng.NextDouble() < args.Self.BlockChance;

            if (Dodge)
            {
                args.Self.OnDodge.Raise(args);
                return true;
            }

            if (Block)
            {
                args.Self.OnBlock.Raise(args);
                if ((long)args.Data == 0)
                {
                    return true;
                }
                else
                {
                    args.Target.OnAfterAttack.Raise(args.Reverse(true));
                }
            }

            return false;
        }

        public static bool OnHealthChange(BattleEventArgs args)
        {
            if (args.Self.Health.Value == 0)
            {
                args.Self.OnDeath.Raise(args);
            }

            return true;
        }

        public static bool OnDodge(BattleEventArgs args)
        {
            args.Log($"{args.Self.Name} dodges {(long)args.Data}dmg from {args.Target.Name}.");
            args.Data = 0;
            args.Target.OnAfterAttack.Raise(args.Reverse(true));
            return true;
        }

        public static bool OnBlock(BattleEventArgs args)
        {
            var dmg = (long)((long)args.Data * args.Self.UnblockableDamage);
            args.Log($"{args.Self.Name} blocks {(long)args.Data - dmg}dmg from {args.Target.Name}.");
            args.Data = dmg;

            return true;
        }

        public static bool OnGetHeal(BattleEventArgs args)
        {
            if (args.Self.Health.Value == args.Self.Health.MaxValue && !args.Flag)
            {
                return true;
            }

            var hpNeeded = args.Self.Health.MaxValue - args.Self.Health.Value;

            if (hpNeeded > 0)
            {
                args.Self.Health.Value += (long)args.Data;
                args.Log($"{args.Target.Name} heals {(long)args.Data}hp to {args.Self.Name}.");
            }

            var overheal = (long)args.Data - hpNeeded;

            if (overheal > 0 && args.Flag)
            {
                args.Self.Shield.Value += overheal;
                args.Log($"{args.Target.Name} gives {(long)args.Data} shields to {args.Self.Name}.");
            }

            args.Self.OnHealthChange.Raise(args);

            return true;
        }

        public static bool OnDeath(BattleEventArgs args)
        {
            args.Self.Shield.Value = 0;
            args.Self.Health.Value = 0;
            args.Self.Buffs.RemoveRange(0, args.Self.Buffs.Count);
            args.Self.Ailments.RemoveRange(0, args.Self.Ailments.Count);
            args.Self.IsAlive = false;

            args.Log($"{args.Self.Name} was killed.");

            args.Target.OnKill.Raise(args.Reverse());

            return true;
        }

        public static bool OnRessurection(BattleEventArgs args)
        {
            args.Self.Health.Value = (long)args.Data;
            args.Self.IsAlive = true;

            args.Log($"{args.Self.Name} was ressurected.");

            return true;
        }
    }
}
