﻿**Naga** - bonus dex, luck; defense converted to evasion

**Elf** - bonus dex; applies poison or bleed on crit

**Undead** - bonus vit, double health (triple on 50); can't be healed by healers (can be shielded), can lifesteal

**Dwarf** - bonus vit, str; can't dodge, lowers crit chance, significantly increases crit dmg

**Demon** - bonus int; each kill gives you a soul, increasing damage permanently for dungeon

**Elemental** - bonus int, luck; damage aura; deals damage and heals allies on death

**Human** - bonus all stats; accuracy aura; also applies random self buff at each battle