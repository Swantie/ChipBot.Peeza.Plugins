﻿# Buffs
- Negative should be true if it counts as a debuff
- Amplify should be true if Value is not a flat value
- Buffs will apply in order they were placed (ailment (de)buffs will go after generic)

# Ailments
- Ailment handlers should return true if target should skip turn
- Ailment buffs will not be purged unless ailment itself is purged
- Ailment buffs will not expire unless ailment itself expires

## Bleed
- Bleed deals % damage to current health
- Cannot be lethal unless bleed power is 1
- Only the strongest will apply
- TurnsLeft counter will decrease for all
- Damage goes through shields

## Poison
- Poison deals flat damage
- Only the strongest will apply
- TurnsLeft counter will decrase only for strongest
- Damage goes through shields

## Stun
- Stun causes a target to skip a turn
- Stun can be blocked
- Stun power will decrease block chance
- Strongest stun will override previous (TurnsLeft counter will be overrided too)
- Stun will cause a debuff that nullifies dodge chance