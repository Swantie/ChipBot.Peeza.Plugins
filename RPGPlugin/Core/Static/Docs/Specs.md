﻿```
                        ┌─ Crusader
         ┌─ Guardian ───┼─ Vanguard
Warrior ─┤              └─ Paladin
         └─ Berserk ────┬─ Juggernaut
                        └─ Gladiator
                        ┌─ Death Knight
         ┌─ BattleMage ─┴─ Templar
Mage ────┼─ Wizard ───┬─── Necromancer
         └─ Cleric ─┐ └─── Arch Wizard
                    ├───── Enchanter
                    └───── Priest
                      ┌─── Hunter
Rogue ───┬─ Archer ───┴─── Ranger
         └─ Assassin ─┬─── NightBlade
                      └─── Cult Assassin
```

```
Warrior - More aggro
    Guardian - More aggro
        Crusader - Damages random enemy on heal
        Vanguard - Much more aggro
        Paladin - Tank with heal 
    Berserk - More damage
        Juggernaut - Enrages - while on low hp deals much more damage
        Gladiator - AoE

Mage - None
    BattleMage - Damage
        Death Knight - Reflects damage
        Templar - Can ressurect himself
    Wizard - Damage
        Necromancer - Ressurects dead enemies to fight by his side
        Arch Wizard - AoE
    Cleric - Heals, Less aggro
        Enchanter - Shields
        Priest - AoE Heals

Rogue - None
    Archer - Damage
        Hunter - Traps that disables enemies, trying to attack target with lowest aggro
        Ranger - Attacks multiple times
    Assassin - Can attack back on dodge
        Night Blade - Can backstab resulting in instant enemy death
        Cult Assassin - Gains health when kills enemies
```