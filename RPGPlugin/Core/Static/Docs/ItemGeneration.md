﻿There are 7 item types:
```cs
public enum ItemType
{
    Weapon,
    Helmet,
    Armor,
    Cape,
    Necklace,
    Ring,
    Trinket
}
```

And 7 item rarities:
```cs
[Flags]
public enum ItemRarity
{
    Common = 0,
    Uncommon = 1,
    Rare = 2,
    Unique = 4,
    Legendary = 8,
    Charged = 16,
    Legacy = 32
}
```

`Charged` is a rarity affix, which means that item have an ability.
`Legacy` items are presetted items, currently cannot be dropped.

Items are generated based on iLvl and drop %.
First roll is for item rarity.

* Common 70%
* Uncommon 20%
* Rare 5%
* Unique 4%
* Legendary 1%

Each % in drop rate will reduce chance of getting common item by 7%. Each % after 10 will reduce chance of getting uncommon item by 4%.

Second roll is for item ability.

* Common 0%
* Uncommon 2%
* Rare 5%
* Unique 10%
* Legendary 25%

Rarity also affects power of an ability and amount of stats.
* Common will reduce maximum value by 80%
* Uncommon will reduce maximum value by 60%
* Rare will reduce maximum value by 30%
* Unique will reduce maximum value by 15%

iLvl increases stats amount and ability power value.

Positive stats amount is 1-10.
Negative stats amount is 1-5.
(each negative stat point will also increase another stat by one point)

Chance of getting negative stats:

* Common 0%
* Uncommon 0%
* Rare 5%
* Unique 10%
* Legendary 25%