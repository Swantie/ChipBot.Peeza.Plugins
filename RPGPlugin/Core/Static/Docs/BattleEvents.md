﻿# ProcessAilments
- Flag - If true will not call OnPreAttack
- Data - Not used
- Will call OnPreAttack (if not stunned, killed, etc)
- Either way will call ProcessBuffs (after OnPreAttack)
- Call to ProcessAilments counts as a turn
# ProcessBuffs
- Flag - Not used
- Data - Not used
# OnPreAttack
- Flag - Not used
- Data - Not used
- Will call target's OnPreGetAttack
- Will call OnAttack if Accuracy check is passed
# OnAttack
- Flag - Not used
- Data - Not used
- Will call OnCrit if Crit check is passed
- Will call target's OnGetAttack
# OnAfterAttack
- Flag - If Crit
- Data - Real damage dealt
# OnPreGetAttack
- Flag - Not used
- Data - Not used
# OnGetAttack
- Flag - If Crit
- Data - Damage
- Will call attacker's OnAfterAttack
- Will call OnDodge if Dodge check is passed
- Will call OnBlock if Block check is passed
    ### OnGetAttackShield
    - Flag - If Crit
    - Data - Damage
    - Will reduce Data by shield capacity
    ### OnGetAttackHealth
    - Flag - If Crit
    - Data - Damage
    - Will call OnHealthChange
# OnHealthChange
- Flag - Not used
- Data - Previous health values
- Will call OnDeath if health is 0
# OnMiss
- Flag - Not used
- Data - Not used
# OnCrit
- Flag - Not used
- Data - Damage
- Will call target's OnGetAttack
# OnDodge
- Flag - If Crit
- Data - Damage
# OnBlock
- Flag - If Crit
- Data - Damage
# OnGetHeal
- Flag - If true then overheal will go to shields
- Data - How much to heal
- Will call OnHealthChange
# OnDeath
- Flag - If Crit
- Data - Damage
# OnRessurection
- Flag - Not used
- Data - How much health should be restored
# OnKill
- Flag - If Crit
- Data - Damage to health
# OnFightStart
- Flag - Not used
- Data - Not used
# OnFightEnd
- Flag - Not used
- Data - Not used