﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static
{
    public static class StatisticAggregatorAbilities
    {
        public static bool OnKill(BattleEventArgs args)
        {
            args.Self.Statistics.EnemiesKilled++;
            return false;
        }

        public static bool OnAfterAttack(BattleEventArgs args)
        {
            if ((long)args.Data > 0) {
                args.Self.Statistics.TotalDamageDealt += (ulong)args.Data;
            }
            return false;
        }

        public static bool OnGetAttack(BattleEventArgs args)
        {
            if ((long)args.Data > 0)
            {
                args.Self.Statistics.TotalDamageTaken += (ulong)args.Data;
            }
            return false;
        }

        public static bool OnDodge(BattleEventArgs args)
        {
            if ((long)args.Data > 0)
            {
                args.Self.Statistics.TotalDamageMitigated += (ulong)args.Data;
            }
            return false;
        }

        public static bool OnBlock(BattleEventArgs args)
        {
            if ((long)args.Data > 0)
            {
                args.Self.Statistics.TotalDamageMitigated += (ulong)args.Data;
            }
            return false;
        }

        public static bool OnGetHeal(BattleEventArgs args)
        {
            if ((long)args.Data > 0 && args.Target.Statistics != null)
            {
                args.Target.Statistics.TotalHealed += (ulong)args.Data;
            }              
            return false;
        }
    }
}
