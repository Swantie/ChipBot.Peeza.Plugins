﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Specs
{
    public class SpecsDescriptions
    {
        public static void PopulateList(Dictionary<string, SpecDescription> list)
        {
            // 6 bonus stats
            list.Add(SpecEnum.Warrior.ToString(), new SpecDescription(SpecEnum.Warrior, null,              
                "Typical warrior. Melee, physical. Relies on defense. High survivability, low damage.",
                "Typical warrior. Melee, physical. Relies on defense. High survivability, low damage.",
                null,
                new Stats(3, 0, 0, 2, 1, 0),
                typeof(Warrior)
            ));
            list.Add(SpecEnum.Rogue.ToString(), new SpecDescription(SpecEnum.Rogue, null,
                "Typical rogue. Melee, physical. Relies on evasion rather than defense. Medium survivability and damage.",
                "Typical rogue. Melee, physical. Relies on evasion rather than defense. Medium survivability and damage.",
                null,
                new Stats(1, 2, 0, 0, 1, 2),
                typeof(Rogue)
            ));
            list.Add(SpecEnum.Mage.ToString(), new SpecDescription(SpecEnum.Mage, null,
                "Typical mage. Ranged, magical. High damage, low survivability.",
                "Typical mage. Ranged, magical. High damage, low survivability.",
                null,
                new Stats(0, 0, 5, 0, 0, 1),
                typeof(Mage)
            ));

            // 10 bonus stats
            list.Add(SpecEnum.Guardian.ToString(), new SpecDescription(SpecEnum.Guardian, SpecEnum.Warrior,
                "",
                "",
                null,
                new Stats(5, 0, 0, 4, 1, 0),
                typeof(Guardian),
                12
            ));
            list.Add(SpecEnum.Berserk.ToString(), new SpecDescription(SpecEnum.Berserk, SpecEnum.Warrior,
                "",
                "",
                null,
                new Stats(4, 0, 0, 3, 2, 1),
                typeof(Berserk),
                12
            ));

            list.Add(SpecEnum.BattleMage.ToString(), new SpecDescription(SpecEnum.BattleMage, SpecEnum.Mage,
                "",
                "",
                null,
                new Stats(3, 0, 5, 1, 0, 1),
                typeof(BattleMage),
                12
            ));
            list.Add(SpecEnum.Wizard.ToString(), new SpecDescription(SpecEnum.Wizard, SpecEnum.Mage,
                "",
                "",
                null,
                new Stats(0, 0, 7, 0, 0, 3),
                typeof(Wizard),
                12
            ));
            list.Add(SpecEnum.Cleric.ToString(), new SpecDescription(SpecEnum.Cleric, SpecEnum.Mage,
                "",
                "",
                null,
                new Stats(0, 0, 7, 0, 0, 3),
                typeof(Cleric),
                12
            ));

            list.Add(SpecEnum.Archer.ToString(), new SpecDescription(SpecEnum.Archer, SpecEnum.Rogue,
                "",
                "",
                null,
                new Stats(1, 6, 0, 0, 1, 2),
                typeof(Archer),
                12
            ));
            list.Add(SpecEnum.Assassin.ToString(), new SpecDescription(SpecEnum.Assassin, SpecEnum.Rogue,
                "",
                "",
                null,
                new Stats(3, 3, 0, 1, 1, 2),
                typeof(Assassin),
                12
            ));

            // 15 bonus stats
            list.Add(SpecEnum.Crusader.ToString(), new SpecDescription(SpecEnum.Crusader, SpecEnum.Guardian,
                "",
                "",
                null,
                new Stats(8, 0, 0, 6, 1, 0),
                typeof(Crusader),
                25
            ));
            list.Add(SpecEnum.Vanguard.ToString(), new SpecDescription(SpecEnum.Vanguard, SpecEnum.Guardian,
                "",
                "",
                null,
                new Stats(6, 0, 0, 8, 1, 0),
                typeof(Vanguard),
                25
            ));
            list.Add(SpecEnum.Paladin.ToString(), new SpecDescription(SpecEnum.Paladin, SpecEnum.Guardian,
                "",
                "",
                null,
                new Stats(8, 0, 0, 6, 1, 0),
                typeof(Paladin),
                25
            ));

            list.Add(SpecEnum.Juggernaut.ToString(), new SpecDescription(SpecEnum.Juggernaut, SpecEnum.Berserk,
                "",
                "",
                null,
                new Stats(7, 0, 0, 5, 2, 1),
                typeof(Juggernaut),
                25
            ));
            list.Add(SpecEnum.Gladiator.ToString(), new SpecDescription(SpecEnum.Gladiator, SpecEnum.Berserk,
                "",
                "",
                null,
                new Stats(9, 0, 0, 3, 2, 1),
                typeof(Gladiator),
                25
            ));

            list.Add(SpecEnum.DeathKnight.ToString(), new SpecDescription(SpecEnum.DeathKnight, SpecEnum.BattleMage,
                "",
                "",
                null,
                new Stats(6, 0, 7, 1, 0, 1),
                typeof(DeathKnight),
                25
            ));
            list.Add(SpecEnum.Templar.ToString(), new SpecDescription(SpecEnum.Templar, SpecEnum.BattleMage,
                "",
                "",
                null,
                new Stats(5, 0, 8, 1, 0, 1),
                typeof(Templar),
                25
            ));
            list.Add(SpecEnum.Necromancer.ToString(), new SpecDescription(SpecEnum.Necromancer, SpecEnum.Wizard,
                "",
                "",
                null,
                new Stats(0, 0, 12, 0, 0, 3),
                typeof(Necromancer),
                25
            ));
            list.Add(SpecEnum.ArchWizard.ToString(), new SpecDescription(SpecEnum.ArchWizard, SpecEnum.Wizard,
                "",
                "",
                null,
                new Stats(0, 0, 12, 0, 0, 3),
                typeof(ArchWizard),
                25
            ));

            list.Add(SpecEnum.Enchanter.ToString(), new SpecDescription(SpecEnum.Enchanter, SpecEnum.Cleric,
                "",
                "",
                null,
                new Stats(0, 0, 10, 2, 0, 3),
                typeof(Enchanter),
                25
            ));
            list.Add(SpecEnum.Priest.ToString(), new SpecDescription(SpecEnum.Priest, SpecEnum.Cleric,
                "",
                "",
                null,
                new Stats(0, 0, 12, 0, 0, 3),
                typeof(Priest),
                25
            ));

            list.Add(SpecEnum.Hunter.ToString(), new SpecDescription(SpecEnum.Hunter, SpecEnum.Archer,
                "",
                "",
                null,
                new Stats(1, 8, 0, 0, 2, 4),
                typeof(Hunter),
                25
            ));
            list.Add(SpecEnum.Ranger.ToString(), new SpecDescription(SpecEnum.Ranger, SpecEnum.Archer,
                "",
                "",
                null,
                new Stats(1, 10, 0, 0, 1, 3),
                typeof(Ranger),
                25
            ));

            list.Add(SpecEnum.NightBlade.ToString(), new SpecDescription(SpecEnum.NightBlade, SpecEnum.Assassin,
                "",
                "",
                null,
                new Stats(5, 6, 0, 1, 1, 2),
                typeof(NightBlade),
                25
            ));
            list.Add(SpecEnum.CultAssassin.ToString(), new SpecDescription(SpecEnum.CultAssassin, SpecEnum.Assassin,
                "",
                "",
                null,
                new Stats(5, 5, 0, 2, 1, 2),
                typeof(CultAssassin),
                25
            ));
        }
    }
}
