﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public abstract class BaseClass : Spec
    {
        public override bool IsMelee()
        {
            return true;
        }

        public override int NumberOfAttacks(Stats stats, Hero hero)
        {
            return 1;
        }

        public override int NumberOfTargets(Stats stats, Hero hero)
        {
            return 1;
        }


        public override long Health(Stats stats, Hero hero)
        {
            return (stats.Vitality + hero.Level) * 2;
        }

        public override long Shield(Stats stats, Hero hero)
        {
            return 0;
        }

        public override double DodgeChance(Stats stats, Hero hero)
        {
            return (stats.Dexterity / 4.0) / 100;
        }

        public override long Defense(Stats stats, Hero hero)
        {
            return stats.Vitality * (stats.Strength / 10);
        }

        public override double Accuracy(Stats stats, Hero hero)
        {
            return (60 + stats.Technique) / 100.0;
        }

        public override double BlockChance(Stats stats, Hero hero)
        {
            return (stats.Technique / 4.0) / 100;
        }

        public override double UnblockableDamage(Stats stats, Hero hero)
        {
            return 0.6;
        }

        public override double CritChance(Stats stats, Hero hero)
        {
            return (stats.Luck * (stats.Dexterity / 10.0)) / 100;
        }

        public override double CritDamage(Stats stats, Hero hero)
        {
            return 2 + stats.Intellegence / 100.0;
        }

        public override double Aggro(Stats stats, Hero hero)
        {
            var val = 10;
            if (IsMelee()) val *= 2;
            return val;
        }
    }
}
