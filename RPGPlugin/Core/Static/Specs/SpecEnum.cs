﻿namespace RPGPlugin.Core.Static.Specs
{
    public enum SpecEnum
    {
        Warrior,
        Guardian,
        Crusader,
        Vanguard,
        Paladin,
        Berserk,
        Juggernaut,
        Gladiator,

        Mage,
        BattleMage,
        DeathKnight,
        Templar,
        Wizard,
        Necromancer,
        ArchWizard,
        Cleric,
        Enchanter,
        Priest,

        Rogue,
        Archer,
        Hunter,
        Ranger,
        Assassin,
        NightBlade,
        CultAssassin,
    }
}
