﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Guardian : Warrior
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (long)(base.Attack(stats, hero) * 1.5);
        }
        public override long Defense(Stats stats, Hero hero)
        {
            return stats.Vitality * (stats.Strength / 9);
        }

        public override double UnblockableDamage(Stats stats, Hero hero)
        {
            return 0.2;
        }

        public override double Aggro(Stats stats, Hero hero)
        {
            return base.Aggro(stats, hero) * 1.5;
        }
    }
}
