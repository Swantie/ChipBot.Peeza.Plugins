﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Berserk : Warrior
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return base.Attack(stats, hero) * 2;
        }
        public override double CritChance(Stats stats, Hero hero)
        {
            return (stats.Luck * (stats.Dexterity / 10.0)) / 100 + stats.Technique / 100.0;
        }
    }
}
