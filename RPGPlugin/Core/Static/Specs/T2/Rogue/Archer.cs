﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Archer : Rogue
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (long)(stats.Dexterity * 2.5);
        }
        public override double Accuracy(Stats stats, Hero hero)
        {
            return (70 + stats.Technique * 2) / 100.0;
        }

        public override bool IsMelee()
        {
            return false;
        }
    }
}
