﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Assassin : Rogue
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (stats.Strength + stats.Dexterity) * 2;
        }
        public override double DodgeChance(Stats stats, Hero hero)
        {
            return stats.Dexterity / 1.8 / 100;
        }

        private bool AssassinOnDodge(BattleEventArgs args)
        {
            if (args.Rng.NextDouble() < 0.5)
            {
                args.Log($"{args.Self.Name} dodges {(long)args.Data}dmg from {args.Target.Name} and attacks back.");
                
                args.Data = 0;
                args.Target.OnAfterAttack.Raise(args.Reverse(true));

                args.Self.OnPreAttack.Raise((BattleEventArgs)args.Clone());

                return true;
            }

            return false;
        }

        public override bool IsMelee()
        {
            return false;
        }

        public Assassin()
        {
            OnDodge = AssassinOnDodge;
        }
    }
}
