﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Wizard : Mage
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Intellegence * 3;
        }
    }
}
