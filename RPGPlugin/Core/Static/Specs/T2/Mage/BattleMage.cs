﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class BattleMage : Mage
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (stats.Intellegence + stats.Strength) * 2;
        }

        public override bool IsMelee()
        {
            return true;
        }
    }
}
