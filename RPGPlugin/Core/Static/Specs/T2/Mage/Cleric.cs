﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Extensions;
using System.Linq;

namespace RPGPlugin.Core.Static.Specs
{
    public class Cleric : Mage
    {
        protected int HealCooldown = 0;

        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Intellegence;
        }

        public override double Aggro(Stats stats, Hero hero)
        {
            return base.Aggro(stats, hero) / 2;
        }

        protected bool ResetHealCooldown(BattleEventArgs args)
        {
            HealCooldown = 0;
            return false;
        }

        protected bool HealOnPreAttack(BattleEventArgs args)
        {
            if (HealCooldown > 0)
            {
                args.Log($"{args.Self.Name}'s heal is still on cooldown {HealCooldown} turn(s) left.");
                HealCooldown--;
                return false;
            }

            var party = args.Party.Where(p => p.IsAlive);

            if (party.All(p => p.Health.Percentage >= 0.8))
            {
                args.Log($"{args.Self.Name}'s got nothing to heal.");
                return false;
            }

            HealCooldown = 2;
            bool crit = args.Rng.NextDouble() < args.Self.CritChance;
            long dmg = args.Self.Attack;
            if (crit)
            {
                args.Log($"{args.Self.Name} critically heals.");
                dmg *= (long)(dmg * args.Self.CritDamage);
            }

            var injured = party.Where(p => p.Health.Value < p.Health.MaxValue).Select(p => new
            {
                Creature = p,
                Weight = p.Aggro / p.Health.Percentage * (p == args.Self ? 2 : 1)
            });
            var target = injured.RandomByWeight(p => p.Weight, args.Rng);

            Heal(target.Creature, args, dmg);

            return true;
        }

        protected void Heal(Creature target, BattleEventArgs args, long heal, bool shields = false)
        {
            var newArgs = (BattleEventArgs)args.Clone();
            newArgs.Self = target;
            newArgs.Target = args.Self;
            newArgs.Data = heal;
            newArgs.Flag = shields;

            target.OnGetHeal.Raise(newArgs);
        }

        public Cleric()
        {
            OnFightStart = ResetHealCooldown;
            OnPreAttack = HealOnPreAttack;
        }
    }
}
