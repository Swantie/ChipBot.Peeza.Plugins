﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class NightBlade : Assassin
    {
        public override double DodgeChance(Stats stats, Hero hero)
        {
            return (stats.Dexterity / 1.6) / 100;
        }

        private bool NightBladeOnAfterAttack(BattleEventArgs args)
        {
            if ((long)args.Data <= 0) return false;
            if (args.Rng.NextDouble() >= 0.02) return false;

            if (args.Target.Flags.HasFlag(CreatureFlags.CantInstaKill))
            {
                args.Log($"{args.Target.Name} can't be instakilled!");
            }
            else
            {
                args.Log($"{args.Self.Name} leaves a critical wound on {args.Target.Name}!");
                args.Target.Health.Value = args.Target.Health.MinValue;
                args.Target.OnDeath.Raise(args.Reverse());
            }

            return false;
        }

        public NightBlade() : base()
        {
            OnAfterAttack = NightBladeOnAfterAttack;
        }
    }
}
