﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class CultAssassin : Assassin
    {       
        private bool CultOnKill(BattleEventArgs args)
        {
            var heal = args.Self.Health.MaxValue / 3;
            if (args.Target.Health.MaxValue < heal) heal = args.Target.Health.MaxValue;

            var newArgs = (BattleEventArgs)args.Clone();
            newArgs.Target = args.Self;
            newArgs.Data = heal;

            args.Self.OnGetHeal.Raise(newArgs);

            return false;
        }

        public CultAssassin() : base()
        {
            OnKill = CultOnKill;
        }
    }
}
