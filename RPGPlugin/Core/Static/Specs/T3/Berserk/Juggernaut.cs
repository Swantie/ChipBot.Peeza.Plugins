﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Specs
{
    public class Juggernaut : Berserk
    {
        public override long Defense(Stats stats, Hero hero)
        {
            return stats.Vitality * (stats.Strength / 9);
        }

        public bool ApplyJuggernautBuff(BattleEventArgs args)
        {
            if (args.Self.Ailments.Exists(a => a.Type == AilmentType.JuggernautSpecPassive)) return false;
            args.Self.Ailments.Add(new Ailment()
            {
                Type = AilmentType.JuggernautSpecPassive,
                Inflicter = args.Self,
                TurnsLeft = 1,
                Power = 1,
                Buffs = new List<Buff>()
            });
            return false;
        }

        public Juggernaut()
        {
            ProcessAilments = ApplyJuggernautBuff;   
        }
    }
}
