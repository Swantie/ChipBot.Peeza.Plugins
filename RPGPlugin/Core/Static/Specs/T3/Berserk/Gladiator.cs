﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Gladiator : Berserk
    {
        public override int NumberOfTargets(Stats stats, Hero hero)
        {
            return stats.Strength / 10;
        }
    }
}
