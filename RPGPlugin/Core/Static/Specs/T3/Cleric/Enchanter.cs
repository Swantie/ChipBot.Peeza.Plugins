﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Extensions;
using System;
using System.Linq;

namespace RPGPlugin.Core.Static.Specs
{
    public class Enchanter : Cleric
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (long)(stats.Intellegence * 1.5);
        }

        protected bool ShieldHealOnPreAttack(BattleEventArgs args)
        {
            if (HealCooldown > 0)
            {
                args.Log($"{args.Self.Name}'s heal is still on cooldown {HealCooldown} turn(s) left.");
                HealCooldown--;
                return false;
            }

            var party = args.Party.Where(p => p.IsAlive);

            if (party.All(p => p.Health.Percentage >= 0.8 && p.Shield.Percentage >= 0.8))
            {
                args.Log($"{args.Self.Name}'s got nothing to heal.");
                return false;
            }

            HealCooldown = 2;
            bool crit = args.Rng.NextDouble() < args.Self.CritChance;
            long dmg = args.Self.Attack;
            if (crit)
            {
                args.Log($"{args.Self.Name} critically heals.");
                dmg *= (long)(dmg * args.Self.CritDamage);
            }

            var injured = party.Where(p => p.Health.Value < p.Health.MaxValue).Select(p => new Tuple<Creature, double>(p, p.Aggro / p.Health.Percentage * (p == args.Self ? 2 : 1)));

            if (!injured.Any())
            {
                injured = party.Where(p => p.Shield.Value < p.Shield.MaxValue).Select(p => new Tuple<Creature, double>(p, p.Aggro / p.Shield.Percentage * (p == args.Self ? 2 : 1)));
            }

            Heal(injured.RandomByWeight(p => p.Item2).Item1, args, dmg, true);

            return true;
        }

        public Enchanter()
        {
            OnFightStart = ResetHealCooldown;
            OnPreAttack = ShieldHealOnPreAttack;
        }
    }
}
