﻿using RPGPlugin.Core.Classes;
using System.Linq;

namespace RPGPlugin.Core.Static.Specs
{
    public class Priest : Cleric
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (long)(stats.Intellegence * 1.5);
        }

        protected bool PartyHealOnPreAttack(BattleEventArgs args)
        {
            if (HealCooldown > 0)
            {
                args.Log($"{args.Self.Name}'s heal is still on cooldown {HealCooldown} turn(s) left.");
                HealCooldown--;
                return false;
            }

            var party = args.Party.Where(p => p.IsAlive);

            if (party.All(p => p.Health.Percentage >= 0.8))
            {
                args.Log($"{args.Self.Name}'s got nothing to heal.");
                return false;
            }

            HealCooldown = 2;
            bool crit = args.Rng.NextDouble() < args.Self.CritChance;
            long dmg = args.Self.Attack;
            if (crit)
            {
                args.Log($"{args.Self.Name} critically heals.");
                dmg *= (long)(dmg * args.Self.CritDamage);
            }

            var injured = party.Where(p => p.Health.Value < p.Health.MaxValue).Select(p => new
            {
                Creature = p,
                Weight = p.Aggro / p.Health.Percentage * (p == args.Self ? 2 : 1)
            });

            if (injured.Count() > 2)
            {
                HealCooldown++;
                foreach (var target in injured.ToList())
                {
                    Heal(target.Creature, args, dmg);
                }
            }
            else
            {
                Heal(injured.First().Creature, args, dmg);
            }

            return true;
        }

        public Priest()
        {
            OnFightStart = ResetHealCooldown;
            OnPreAttack = PartyHealOnPreAttack;
        }
    }
}
