﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Extensions;
using System.Linq;

namespace RPGPlugin.Core.Static.Specs
{
    public class Crusader : Guardian
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Strength * 2;
        }

        public override double Aggro(Stats stats, Hero hero)
        {
            return base.Aggro(stats, hero) * 1.2;
        }

        private bool CrusaderOnHeal(BattleEventArgs args)
        {
            var target = args.Enemies.Where(e => e.IsAlive).RandomByWeight(e => e.Aggro, args.Rng);
            args.Log($"{args.Self.Name} deals {(long)args.Data}hp damage to {target.Name} by using powers of light.");

            var clone = (BattleEventArgs)args.Clone();
            clone.Self = target;
            clone.Data = target.Health;
            target.Health.Value -= (long)args.Data;
            clone.Target = args.Self;

            target.OnHealthChange.Raise(clone);

            return false;
        }

        public Crusader()
        {
            OnGetHeal = CrusaderOnHeal;
        }
    }
}
