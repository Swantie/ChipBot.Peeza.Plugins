﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Extensions;
using System.Linq;

namespace RPGPlugin.Core.Static.Specs
{
    public class Paladin : Guardian
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (long)(base.Attack(stats, hero) * 1.5);
        }

        protected int HealCooldown = 0;
        protected bool ResetHealCooldown(BattleEventArgs args)
        {
            HealCooldown = 0;
            return false;
        }

        protected bool HealOnPreAttack(BattleEventArgs args)
        {
            if (HealCooldown > 0)
            {
                args.Log($"{args.Self.Name}'s heal is still on cooldown {HealCooldown} turn(s) left.");
                HealCooldown--;
                return false;
            }

            var party = args.Party.Where(p => p.IsAlive);

            if (party.All(p => p.Health.Percentage >= 0.8))
            {
                args.Log($"{args.Self.Name}'s got nothing to heal.");
                return false;
            }

            HealCooldown = 3;
            long dmg = args.Self.Attack;

            var injured = party.Where(p => p.Health.Value < p.Health.MaxValue).Select(p => new
            {
                Creature = p,
                Weight = p.Aggro / p.Health.Percentage * (p == args.Self ? 2 : 1)
            });
            var target = injured.RandomByWeight(p => p.Weight, args.Rng);

            var newArgs = (BattleEventArgs)args.Clone();
            newArgs.Self = target.Creature;
            newArgs.Target = args.Self;
            newArgs.Data = dmg;
            newArgs.Flag = false;

            target.Creature.OnGetHeal.Raise(newArgs);

            return true;
        }

        public Paladin()
        {
            OnFightStart = ResetHealCooldown;
            OnPreAttack = HealOnPreAttack;
        }
    }
}
