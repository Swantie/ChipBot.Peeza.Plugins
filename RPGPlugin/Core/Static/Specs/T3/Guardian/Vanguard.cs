﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Vanguard : Guardian
    {
        public override long Defense(Stats stats, Hero hero)
        {
            return stats.Vitality * (stats.Strength / 8);
        }

        public override double Aggro(Stats stats, Hero hero)
        {
            return base.Aggro(stats, hero) * 2;
        }
    }
}
