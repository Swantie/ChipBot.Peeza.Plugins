﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class DeathKnight : BattleMage
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (long)((stats.Intellegence + stats.Strength) * 2.5);
        }

        private bool ReflectOnGetAttack(BattleEventArgs args)
        {
            args.Log($"{args.Self.Name} reflects {(long)args.Data}dmg back to {args.Target.Name}.");
            var newArgs = args.Reverse();
            newArgs.Data = args.Target.Health.Value;
            args.Target.Health.Value -= (long)args.Data;
            args.Target.OnHealthChange.Raise(newArgs);

            return false;
        }

        public DeathKnight()
        {
            OnGetAttack = ReflectOnGetAttack;
        }
    }
}
