﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Templar : BattleMage
    {
        private int RevivesCount = 0;
        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Intellegence * 2 + (long)(stats.Strength * 2.5);
        }

        public override long Defense(Stats stats, Hero hero)
        {
            return stats.Vitality * (stats.Strength / 9);
        }

        private bool TemplarOnDungeonStart(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            RevivesCount = (int)((hero.HeroReference.Stats.Strength / 2.0 + hero.HeroReference.Stats.Intellegence * 2.0) / 30.0);

            return false;
        }

        private bool TemplarOnDeath(BattleEventArgs args)
        {
            if (RevivesCount <= 0) return false;

            args.Log($"{args.Self.Name} used his inner light to avoid death.");
            RevivesCount--;
            args.Self.Health.Value = args.Self.Health.MaxValue;
            return true;
        }

        public Templar()
        {
            OnDungeonStart = TemplarOnDungeonStart;
            OnDeath = TemplarOnDeath;
        }
    }
}
