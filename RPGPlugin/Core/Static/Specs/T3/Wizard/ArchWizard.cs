﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class ArchWizard : Wizard
    {
        public override int NumberOfTargets(Stats stats, Hero hero)
        {
            return stats.Intellegence / 20;
        }

        public override double DodgeChance(Stats stats, Hero hero)
        {
            return 0;
        }
    }
}
