﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static.Specs
{
    public class Necromancer : Wizard
    {
        private readonly List<LesserSummon> Summons = new List<LesserSummon>();
        public IEnumerable<LesserSummon> AliveSummons => Summons.Where(s => s.IsAlive);

        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Intellegence * 2;
        }

        private int SummonsLimit(Stats stats)
        {
            return (stats.Intellegence / 5) + 4;
        }

        private bool NecromancerClearSummons(BattleEventArgs args)
        {
            Summons.Clear();
            return false;
        }

        private bool NecromancerOnKill(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            if (args.Target.Flags.HasFlag(CreatureFlags.CantRessurect))
            {
                args.Log($"{args.Target.Name} can't be ressurected");
            }
            else
            {
                args.Log($"{args.Self.Name} ressurects {args.Target.Name} as his servant.");
                Summons.Add(new LesserSummon(args.Target, SummonsLimit(hero.HeroReference.Stats), args.Self.Name));
            }

            return false;
        }

        private bool NecromancerProcessAilments(BattleEventArgs args)
        {
            foreach (var sum in AliveSummons)
            {
                var cloned = (BattleEventArgs)args.Clone();
                cloned.Flag = true;
                cloned.Self = sum;

                sum.ProcessAilments.Raise(cloned);
            }

            Summons.RemoveAll(s => !s.IsAlive);

            return false;
        }

        private bool NecromancerOnGetAttack(BattleEventArgs args)
        {
            if (AliveSummons.Count() <= 0) return false;

            var target = AliveSummons.RandomByWeight(s => s.Aggro, args.Rng);
            args.Log($"{args.Self.Name} uses {target.Name} to shield himself from incoming attack.");

            args.Self = target;
            target.OnGetAttack.Raise(args);

            return true;
        }

        private bool NecromancerOnPreAttack(BattleEventArgs args)
        {
            if (!AliveSummons.Any()) return false;

            args.Log($"{args.Self.Name} uses his servants to attack.");
            foreach (var sum in AliveSummons.ToList())
            {
                var cloned = (BattleEventArgs)args.Clone();
                cloned.Self = sum;

                sum.OnPreAttack.Raise(cloned);
            }

            return false;
        }

        public Necromancer()
        {
            OnDungeonStart = NecromancerClearSummons;
            OnDungeonEnd = NecromancerClearSummons;

            OnKill = NecromancerOnKill;
            ProcessAilments = NecromancerProcessAilments;
            OnGetAttack = NecromancerOnGetAttack;
            OnPreAttack = NecromancerOnPreAttack;
        }
    }

    public class LesserSummon : Creature
    {
        public int TimeLeft = 0;

        private bool LesserSummonProcessAilments(BattleEventArgs args)
        {
            TimeLeft--;

            if (TimeLeft <= 0)
            {
                var hp = args.Self.Health.Value;
                args.Self.Health.Value = 0;
                args.Target = args.Self;
                args.Data = hp;
                args.Log($"{args.Self.Name} vanishes.");
                args.Self.OnHealthChange.Raise(args);
            }

            return false;
        }

        public LesserSummon(Creature creature, int timeLeft, string name) : base()
        {
            Name = $"{name}'s summoned {creature.Name}";

            NumberOfAttacks = creature.NumberOfAttacks;
            NumberOfTargets = creature.NumberOfTargets;
            IsMelee = creature.IsMelee;

            Attack = creature.Attack / 2;
            Defense = creature.Attack / 2;

            Accuracy = creature.Accuracy;
            CritChance = creature.CritChance / 2;
            CritDamage = creature.CritDamage / 2;
            DodgeChance = creature.DodgeChance / 2;
            BlockChance = creature.BlockChance / 2;

            Aggro = creature.Aggro;

            IsAlive = true;
            Health = new Bar(creature.Health.MaxValue / 2);
            Shield = new Bar(creature.Shield.MaxValue, 0, 0);

            TimeLeft = timeLeft;

            ProcessAilments.Event += LesserSummonProcessAilments;
        }
    }
}
