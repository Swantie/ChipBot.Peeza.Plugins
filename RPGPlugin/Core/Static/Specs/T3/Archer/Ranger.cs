﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Ranger : Archer
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Dexterity * 3;
        }
        public override int NumberOfAttacks(Stats stats, Hero hero)
        {
            return 1 + stats.Dexterity / 40;
        }
    }
}
