﻿using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Static.Specs
{
    public class Hunter : Archer
    {
        public override long Defense(Stats stats, Hero hero)
        {
            return (stats.Vitality + 2) * stats.Strength / 10;
        }

        private Creature HunterPickTarget(BattleEventArgs args)
        {
            var alive = args.Enemies.Where(e => e.IsAlive);
            if (alive.Count() <= 0) return null;

            return alive.RandomByWeight(e => 1 / e.Aggro, args.Rng);
        }

        private bool HunterOnFightStart(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            if (args.Rng.NextDouble() < 0.1)
            {
                args.Log($"{args.Self.Name} stuns the whole party!");
                args.Enemies.Where(p => p.IsAlive).ToList()
                    .ForEach(p => p.Ailments.Add(GenerateStun(hero.HeroReference.Stats, args.Self)));
            }

            return false;
        }

        private bool HunterOnPreAttack(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            var enemies = args.Enemies.Where(p => p.IsAlive);

            if (enemies.Any() && args.Rng.NextDouble() < 0.2)
            {
                var target = enemies.RandomByWeight(p => p.Aggro, args.Rng);

                args.Log($"{args.Self.Name} stuns {target.Name}.");
                target.Ailments.Add(GenerateStun(hero.HeroReference.Stats, args.Self));
            }

            return false;
        }

        private Ailment GenerateStun(Stats stats, Creature inflicter)
        {
            return new Ailment()
            {
                Buffs = new List<Buff>()
                        {
                            new Buff() { Amplify = true, Negative = true, TurnsLeft = 1, Type = BuffType.DodgeChance, Value = 0 }
                        },
                Inflicter = inflicter,
                Power = 0.5 + (stats.Technique + stats.Luck) / 100.0,
                TurnsLeft = stats.Technique / 3,
                Type = AilmentType.Stun
            };
        }

        public Hunter()
        {
            PickTarget = HunterPickTarget;
            OnFightStart = HunterOnFightStart;
            OnPreAttack = HunterOnPreAttack;
        }
    }
}
