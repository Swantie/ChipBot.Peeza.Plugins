﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Mage : BaseClass
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Intellegence * 2;
        }

        public override bool IsMelee()
        {
            return false;
        }
    }
}
