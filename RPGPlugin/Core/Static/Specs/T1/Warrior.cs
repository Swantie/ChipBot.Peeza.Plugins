﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Warrior : BaseClass
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return stats.Strength;
        }
        public override double CritDamage(Stats stats, Hero hero)
        {
            return 2.5 + stats.Intellegence / 100.0;
        }

        public override double UnblockableDamage(Stats stats, Hero hero)
        {
            return 0.4;
        }

        public override double Aggro(Stats stats, Hero hero)
        {
            return base.Aggro(stats, hero) * 1.5;
        }
    }
}
