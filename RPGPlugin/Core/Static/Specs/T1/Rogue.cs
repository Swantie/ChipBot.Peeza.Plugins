﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Specs
{
    public class Rogue : BaseClass
    {
        public override long Attack(Stats stats, Hero hero)
        {
            return (int)((stats.Strength + stats.Dexterity) * 1.5);
        }

        public override double DodgeChance(Stats stats, Hero hero)
        {
            return (stats.Dexterity / 2.0) / 100;
        }
    }
}
