﻿using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static RPGPlugin.Core.Static.ItemBattleAbilities;

namespace RPGPlugin.Core.Static
{
    public class ItemData
    {
        public class Range
        {
            public double Min { get; set; } = 0;
            public double Max { get; set; } = 0;

            public bool Flat { get; set; } = false;

            public double MaxByRarity(ItemRarity rarity)
            {
                var val = Max * StatsPercentageByRarity[rarity];
                if (Flat) val = Math.Floor(val);
                return val;
            }
        }

        public class ItemStat
        {
            public Range Power { get; set; } = new Range();
            public Range Duration { get; set; } = new Range();
            public Range Chance { get; set; } = new Range();
        }

        public readonly static Dictionary<ItemRarity, double> StatsPercentageByRarity = new Dictionary<ItemRarity, double>()
        {
            { ItemRarity.Common, 0.2 },
            { ItemRarity.Uncommon, 0.4 },
            { ItemRarity.Rare, 0.7 },
            { ItemRarity.Unique, 0.85 },
            { ItemRarity.Legendary, 1 },
        };

        public readonly static Dictionary<AbilityType, Func<double, double, double, BattleEventHandler>> Generators = new Dictionary<AbilityType, Func<double, double, double, BattleEventHandler>>()
        {
            { AbilityType.Poison, PoisonGen },
            { AbilityType.Bleed, BleedGen },
            { AbilityType.Lifesteal, LifestealGen },
            { AbilityType.PoisonResistance, PoisonResistGen },
            { AbilityType.BleedResistance, BleedResistGen },
            { AbilityType.StunResistance, StunResistGen },
            { AbilityType.Block, BlockGen },
            { AbilityType.HealAmplifier, HealGen },
            { AbilityType.ShieldCharger, ShieldChargerGen },
            { AbilityType.Repeater, RepeaterGen }
        };

        public readonly static Dictionary<ItemType, List<AbilityType>> AbilitiesByItemType = new Dictionary<ItemType, List<AbilityType>>()
        {
            { ItemType.Weapon, new List<AbilityType> { AbilityType.Bleed, AbilityType.Poison, AbilityType.Repeater, AbilityType.Lifesteal } },
            { ItemType.Helmet, new List<AbilityType> { AbilityType.Block, AbilityType.ShieldCharger } },
            { ItemType.Armor, new List<AbilityType> { AbilityType.Block, AbilityType.ShieldCharger, AbilityType.HealAmplifier } },
            { ItemType.Cape, new List<AbilityType> { AbilityType.ShieldCharger, AbilityType.HealAmplifier, AbilityType.StunResistance } },
            { ItemType.Necklace, new List<AbilityType> { AbilityType.PoisonResistance, AbilityType.BleedResistance, AbilityType.HealAmplifier, AbilityType.StunResistance, } },
            { ItemType.Ring, new List<AbilityType> { AbilityType.PoisonResistance, AbilityType.BleedResistance } },
            { ItemType.Trinket, new List<AbilityType> { AbilityType.Repeater, AbilityType.Poison, AbilityType.Bleed } },
        };

        public readonly static Dictionary<AbilityType, StackingType> AbilitesStacking = new Dictionary<AbilityType, StackingType>()
        {
            { AbilityType.Poison, StackingType.Power },
            { AbilityType.Bleed, StackingType.Duration },
            { AbilityType.Lifesteal, StackingType.None },

            { AbilityType.PoisonResistance, StackingType.None },
            { AbilityType.BleedResistance, StackingType.None },
            { AbilityType.StunResistance, StackingType.None },

            { AbilityType.Block, StackingType.Chance },

            { AbilityType.HealAmplifier, StackingType.Power },
            { AbilityType.ShieldCharger, StackingType.Power },

            { AbilityType.Repeater, StackingType.None },
        };

        public readonly static Dictionary<AbilityType, ItemStat> AbilitesStats = new Dictionary<AbilityType, ItemStat>()
        {
            { AbilityType.Poison, new ItemStat { Power = new Range { Min = 1, Max = 600, Flat = true }, Chance = new Range { Min = 0.1, Max = 0.9 }, Duration = new Range { Min = 2, Max = 5, Flat = true } } },
            { AbilityType.Bleed, new ItemStat { Power = new Range { Min = 0.02, Max = 0.2 }, Chance = new Range { Min = 0.1, Max = 0.9 }, Duration = new Range { Min = 2, Max = 5, Flat = true } } },
            { AbilityType.Lifesteal, new ItemStat { Power = new Range { Min = 0.05, Max = 0.25 } } },

            { AbilityType.PoisonResistance, new ItemStat() },
            { AbilityType.BleedResistance, new ItemStat() },
            { AbilityType.StunResistance, new ItemStat { Chance = new Range { Min = 0.1, Max = 0.6 } } },

            { AbilityType.Block, new ItemStat { Power = new Range { Min = 1, Max = 600, Flat = true }, Chance = new Range { Min = 0.1, Max = 0.5 } } },

            { AbilityType.HealAmplifier, new ItemStat { Power = new Range { Min = 0.1, Max = 0.6 } } },
            { AbilityType.ShieldCharger, new ItemStat { Power = new Range { Min = 5, Max = 700, Flat = true } } },

            { AbilityType.Repeater, new ItemStat { Chance = new Range { Min = 0.1, Max = 0.9 } } },
        };

        public readonly static Range ItemsPositiveStatsRange = new Range() { Min = 1, Max = 15, Flat = true };
        public readonly static Range ItemsNegativeStatsRange = new Range() { Min = 1, Max = 5, Flat = true };

        public readonly static Dictionary<AbilityType, FieldInfo> AbilitiesBindings = new Dictionary<AbilityType, FieldInfo>()
        {
            { AbilityType.Poison, typeof(Creature).GetField("OnAfterAttack") },
            { AbilityType.Bleed, typeof(Creature).GetField("OnAfterAttack") },
            { AbilityType.Lifesteal, typeof(Creature).GetField("OnAfterAttack") },

            { AbilityType.PoisonResistance, typeof(Creature).GetField("ProcessAilments") },
            { AbilityType.BleedResistance, typeof(Creature).GetField("ProcessAilments") },
            { AbilityType.StunResistance, typeof(Creature).GetField("ProcessAilments") },

            { AbilityType.Block, typeof(Creature).GetField("OnGetAttack") },

            { AbilityType.HealAmplifier, typeof(Creature).GetField("OnGetHeal") },
            { AbilityType.ShieldCharger, typeof(Creature).GetField("OnPreAttack") },

            { AbilityType.Repeater, typeof(Creature).GetField("OnMiss") },
        };
        public static BattleEventHandler GenerateItemAbility(AbilityType ability, double power, double chance, double duration)
        {
            return Generators[ability].Invoke(power, chance, duration);
        }
        public static Dictionary<AbilityType, BattleEventHandler> GenerateAbilities(IEnumerable<Item> items)
        {
            var grouped = items.Where(i => i.Ability.HasValue).GroupBy(i => i.Ability);

            var dict = new Dictionary<AbilityType, BattleEventHandler>();

            foreach (var gr in grouped)
            {
                Item item = null;
                switch (AbilitesStacking[gr.Key.Value])
                {
                    case StackingType.None:
                        item = gr
                            .OrderByDescending(a => a.AbilityDuration)
                            .OrderByDescending(a => a.AbilityChance)
                            .OrderByDescending(a => a.AbilityPower)
                            .First();

                        dict.Add(gr.Key.Value, GenerateItemAbility(item.Ability.Value, item.AbilityPower, item.AbilityChance, item.AbilityDuration));
                        break;

                    case StackingType.Power:
                        item = gr.OrderByDescending(a => a.AbilityPower).First();

                        dict.Add(gr.Key.Value, GenerateItemAbility(item.Ability.Value, gr.Sum(i => i.AbilityPower), item.AbilityChance, item.AbilityDuration));
                        break;

                    case StackingType.Chance:
                        item = gr.OrderByDescending(a => a.AbilityChance).First();

                        dict.Add(gr.Key.Value, GenerateItemAbility(item.Ability.Value, item.AbilityPower, gr.Sum(i => i.AbilityChance), item.AbilityDuration));
                        break;

                    case StackingType.Duration:
                        item = gr.OrderByDescending(a => a.AbilityDuration).First();

                        dict.Add(gr.Key.Value, GenerateItemAbility(item.Ability.Value, item.AbilityPower, item.AbilityChance, gr.Sum(i => i.AbilityDuration)));
                        break;
                }
            }

            return dict;
        }
    }
}
