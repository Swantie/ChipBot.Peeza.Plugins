﻿using RPGPlugin.Core.Classes;
using System.Linq;

namespace RPGPlugin.Core.Static
{
    public static class ItemBattleAbilities
    {
        public static BattleEventHandler PoisonGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                if (args.Rng.NextDouble() < chance)
                {
                    var ailment = new Ailment
                    {
                        Inflicter = args.Self,
                        Type = AilmentType.Poison,
                        Power = power,
                        TurnsLeft = (int)duration
                    };
                    args.Log($"{args.Self.Name} inflicts poison on {args.Target.Name}.");
                    args.Target.Ailments.Add(ailment);
                }

                return false;
            };
        }
        public static BattleEventHandler BleedGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                if (args.Rng.NextDouble() < chance)
                {
                    var ailment = new Ailment
                    {
                        Inflicter = args.Self,
                        Type = AilmentType.Bleed,
                        Power = power,
                        TurnsLeft = (int)duration
                    };
                    args.Log($"{args.Self.Name} inflicts bleed on {args.Target.Name}.");
                    args.Target.Ailments.Add(ailment);
                }

                return false;
            };
        }
        public static BattleEventHandler LifestealGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                var heal = (long)args.Data * power;
                args.Log($"{args.Self.Name} lifesteals for {heal}hp.");

                args.Self.Health.Value += (long)heal;

                args.Self.OnHealthChange.Raise(args);

                return false;
            };
        }

        public static BattleEventHandler PoisonResistGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                var ailments = args.Self.Ailments.Where(a => a.Type == AilmentType.Poison);

                if (ailments.Any())
                {
                    args.Self.Ailments.RemoveAll(a => a.Type == AilmentType.Poison);
                    args.Log($"{args.Self.Name} resisted poison.");
                }

                return false;
            };
        }
        public static BattleEventHandler BleedResistGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                var ailments = args.Self.Ailments.Where(a => a.Type == AilmentType.Bleed);

                if (ailments.Any())
                {
                    args.Self.Ailments.RemoveAll(a => a.Type == AilmentType.Bleed);
                    args.Log($"{args.Self.Name} resisted bleed.");
                }

                return false;
            };
        }
        public static BattleEventHandler StunResistGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                var ailments = args.Self.Ailments.Where(a => a.Type == AilmentType.Stun);

                if (ailments.Any())
                {
                    if (args.Rng.NextDouble() < chance)
                    {
                        args.Self.Ailments.RemoveAll(a => a.Type == AilmentType.Stun);
                        args.Log($"{args.Self.Name} resisted stun.");
                    }
                }

                return false;
            };
        }

        public static BattleEventHandler BlockGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                if (args.Rng.NextDouble() < chance)
                {
                    args.Log($"{args.Self.Name} mitigates {(long)power}dmg.");
                    var dmg = (long)args.Data - power;
                    args.Data = dmg < 0 ? 1 : dmg;
                }

                return false;
            };
        }

        public static BattleEventHandler HealGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                args.Data = (long)args.Data * power;

                args.Log($"{args.Self.Name} amplifies heal on {power * 100:0.00}%.");

                return false;
            };
        }
        public static BattleEventHandler ShieldChargerGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                args.Self.Shield.Value += (long)power;

                args.Log($"{args.Self.Name} generates {(long)power} shields.");

                return false;
            };
        }

        public static BattleEventHandler RepeaterGen(double power, double chance, double duration)
        {
            return (BattleEventArgs args) =>
            {
                if (args.Rng.NextDouble() < chance)
                {
                    args.Log($"{args.Self.Name} misses but is quick enough to try attack again.");
                    args.Self.OnPreAttack.Raise(args);
                }
                return true;
            };
        }
    }
}
