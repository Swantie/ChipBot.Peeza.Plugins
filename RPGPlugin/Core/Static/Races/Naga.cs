﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Races
{
    public class Naga : Race
    {
        public bool ApplyNagaBuff(BattleEventArgs args)
        {
            if (args.Self.Ailments.Exists(a => a.Type == AilmentType.NagaRacePassive)) return false;
            args.Self.Ailments.Add(new Ailment()
            {
                Type = AilmentType.NagaRacePassive,
                Inflicter = args.Self,
                TurnsLeft = 1,
                Power = 1,
                Buffs = new List<Buff>()
            });
            return false;
        }

        public Naga()
        {
            ProcessAilments = ApplyNagaBuff;
        }
    }
}
