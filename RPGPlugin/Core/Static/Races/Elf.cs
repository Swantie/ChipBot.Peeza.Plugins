﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Races
{
    public class Elf : Race
    {
        public bool ElfOnAfterAttack(BattleEventArgs args)
        {
            if (!args.Flag) return false;
            if (!(args.Self is DungeonHero)) return false;

            DungeonHero hero = (DungeonHero)args.Self;

            AilmentType ailment;
            double power;

            if (args.Rng.NextDouble() < 0.5)
            {
                ailment = AilmentType.Bleed;
                power = hero.HeroReference.Level * 0.005;
            }
            else
            {
                ailment = AilmentType.Poison;
                power = hero.HeroReference.Level * 2 + 10;
            }

            args.Target.Ailments.Add(new Ailment() {
                Buffs = new List<Buff>(),
                Inflicter = hero,
                TurnsLeft = 3,
                Power = power,
                Type = ailment
            });

            return false;
        }

        public Elf()
        {
            OnAfterAttack = ElfOnAfterAttack;
        }
    }
}
