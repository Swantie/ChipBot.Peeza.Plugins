﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Races
{
    public class Dwarf : Race
    {
        public bool ApplyDwarfBuff(BattleEventArgs args)
        {
            if (args.Self.Ailments.Exists(a => a.Type == AilmentType.DwarfRacePassive)) return false;
            args.Self.Ailments.Add(new Ailment()
            {
                Type = AilmentType.DwarfRacePassive,
                Inflicter = args.Self,
                TurnsLeft = 1,
                Power = 1,
                Buffs = new List<Buff>()
            });
            return false;
        }

        public Dwarf()
        {
            ProcessAilments = ApplyDwarfBuff;
        }
    }
}
