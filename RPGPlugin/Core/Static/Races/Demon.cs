﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Races
{
    public class Demon : Race
    {
        private int souls = 0;
        public bool DemonOnDungeonStart(BattleEventArgs args)
        {
            souls = 0;
            return false;
        }

        public bool DemonProccessAilments(BattleEventArgs args)
        {
            if (souls == 0) return false;
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            args.Self.Buffs.Add(new Buff() {
                Amplify = false,
                Negative = false,
                TurnsLeft = 1,
                Type = BuffType.Attack,
                Value = souls + souls * hero.HeroReference.Level / 50
            });
            return false;
        }

        public bool DemonOnKill(BattleEventArgs args)
        {
            souls++;
            return false;
        }

        public Demon()
        {
            OnDungeonStart = DemonOnDungeonStart;
            ProcessAilments = DemonProccessAilments;
            OnKill = DemonOnKill;
        }
    }
}
