﻿namespace RPGPlugin.Core.Static.Races
{
    public enum RacesEnum
    {
        Naga,
        Elf,

        Undead,
        Dwarf,

        Demon,
        Elemental,

        Human,
    }
}
