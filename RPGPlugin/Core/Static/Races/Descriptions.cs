﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Races
{
    public class RaceDescriptions
    {
        public static void PopulateList(Dictionary<string, RaceDescription> list)
        {
            list.Add(RacesEnum.Naga.ToString(), new RaceDescription(RacesEnum.Naga, "",
                new Stats(0, 4, 0, 0, 0, 2),
                typeof(Naga)
            ));
            list.Add(RacesEnum.Elf.ToString(), new RaceDescription(RacesEnum.Elf, "",
                new Stats(0, 6, 0, 0, 0, 0),
                typeof(Elf)
            ));

            list.Add(RacesEnum.Undead.ToString(), new RaceDescription(RacesEnum.Undead, "",
                new Stats(0, 0, 0, 6, 0, 0),
                typeof(Undead)
            ));
            list.Add(RacesEnum.Dwarf.ToString(), new RaceDescription(RacesEnum.Dwarf, "",
                new Stats(3, 0, 0, 3, 0, 0),
                typeof(Dwarf)
            ));

            list.Add(RacesEnum.Demon.ToString(), new RaceDescription(RacesEnum.Demon, "",
                new Stats(0, 0, 6, 0, 0, 0),
                typeof(Demon)
            ));
            list.Add(RacesEnum.Elemental.ToString(), new RaceDescription(RacesEnum.Elemental, "",
                new Stats(0, 0, 3, 0, 0, 3),
                typeof(Elemental)
            ));

            list.Add(RacesEnum.Human.ToString(), new RaceDescription(RacesEnum.Human, "",
                new Stats(1, 1, 1, 1, 1, 1),
                typeof(Human)
            ));
        }
    }
}
