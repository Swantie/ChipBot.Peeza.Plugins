﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Races
{
    public class Human : Race
    {
        public bool ApplyHumanAura(BattleEventArgs args)
        {
            if (args.Self.Ailments.Exists(a => a.Type == AilmentType.HumanRaceAura)) return false;
            args.Self.Ailments.Add(new Ailment()
            {
                Type = AilmentType.HumanRaceAura,
                Inflicter = args.Self,
                TurnsLeft = 1,
                Power = 1,
                Buffs = new List<Buff>()
            });
            return false;
        }

        public bool ApplyHumanBuff(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            BuffType type;
            double power;
            if (args.Rng.NextDouble() < 0.5)
            {
                type = args.Rng.NextDouble() < 0.5 ? BuffType.Attack : BuffType.Defense;
                power = hero.HeroReference.Level * 2;
            }
            else
            {
                type = args.Rng.NextDouble() < 0.5 ? BuffType.CritChance : BuffType.DodgeChance;
                power = hero.HeroReference.Level * 0.005;
            }

            args.Self.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 2 + (int)(hero.HeroReference.Level / 10), Type = type, Value = power });

            return false;
        }

        public Human()
        {
            ProcessAilments = ApplyHumanAura;
            OnFightStart = ApplyHumanBuff;
        }
    }
}
