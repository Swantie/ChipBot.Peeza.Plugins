﻿using RPGPlugin.Core.Classes;

namespace RPGPlugin.Core.Static.Races
{
    public class Undead : Race
    {
        public bool UndeadOnHeal(BattleEventArgs args)
        {
            if (!args.Flag)
            {
                args.Log($"{args.Target.Name} can't heal {args.Self.Name} because Undead cannot be healed.");
                return true;
            }

            args.Log($"{args.Target.Name} gives {(long)args.Data} shields to {args.Self.Name}.");
            args.Self.Shield.Value += (long)args.Data;

            return true;
        }

        public override long Health(Hero hero)
        {
            return base.Health(hero) * (2 + hero.Level / 50);
        }

        public Undead()
        {
            OnGetHeal = UndeadOnHeal;
        }
    }
}
