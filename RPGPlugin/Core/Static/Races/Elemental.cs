﻿using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGPlugin.Core.Static.Races
{
    public class Elemental : Race
    {
        public bool ElementalOnDeath(BattleEventArgs args)
        {
            if (!(args.Self is DungeonHero)) return false;
            DungeonHero hero = (DungeonHero)args.Self;

            long damage = hero.HeroReference.Level * (2 + hero.HeroReference.RebornLevel);

            args.Log($"{args.Self.Name} is healing party for {damage}hp with his last elemental spark.");
            foreach(var p in args.Party)
            {
                if (!p.IsAlive) continue;
                
                var clone = (BattleEventArgs)args.Clone();
                clone.Self = p;
                clone.Data = p.Health.Value;
                p.Health.Value += damage;
                clone.Target = args.Self;
                p.OnHealthChange.Raise(clone);
            }

            args.Log($"{args.Self.Name} is damaging enemies for {damage}hp with his last elemental spark.");
            foreach (var p in args.Enemies)
            {
                if (!p.IsAlive) continue;

                var clone = (BattleEventArgs)args.Clone();
                clone.Self = p;
                clone.Data = p.Health.Value;
                p.Health.Value -= damage;
                clone.Target = args.Self;
                p.OnHealthChange.Raise(clone);
            }

            return false;
        }

        public bool ApplyElementalAura(BattleEventArgs args)
        {
            if (args.Self.Ailments.Exists(a => a.Type == AilmentType.ElementalRaceAura)) return false;
            args.Self.Ailments.Add(new Ailment()
            {
                Type = AilmentType.ElementalRaceAura,
                Inflicter = args.Self,
                TurnsLeft = 1,
                Power = 1,
                Buffs = new List<Buff>()
            });
            return false;
        }

        public Elemental()
        {
            OnDeath = ElementalOnDeath;
            ProcessAilments = ApplyElementalAura;
        }
    }
}
