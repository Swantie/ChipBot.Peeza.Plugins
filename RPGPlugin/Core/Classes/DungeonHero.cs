﻿using RPGPlugin.Core.Static;

namespace RPGPlugin.Core.Classes
{
    public class DungeonHero : Creature
    {
        public Hero HeroReference { get; private set; }
        public Spec SpecReference { get; private set; }
        public Race RaceReference { get; private set; }

        public override Creature PickTarget(BattleEventArgs args)
        {
            if (SpecReference.PickTarget == null) return base.PickTarget(args);
            return SpecReference.PickTarget.Invoke(args);
        }

        public DungeonHero(Hero hero) : base()
        {
            HeroReference = hero;
            SpecReference = hero.SpecReference;
            RaceReference = hero.RaceReference;

            Name = hero.Name;
            Statistics = hero.Statistics;

            NumberOfAttacks = RaceReference.NumberOfAttacks(hero);
            NumberOfTargets = RaceReference.NumberOfTargets(hero);
            IsMelee = SpecReference.IsMelee();

            Attack = RaceReference.Attack(hero);
            Defense = RaceReference.Defense(hero);

            Accuracy = RaceReference.Accuracy(hero);
            CritChance = RaceReference.CritChance(hero);
            CritDamage = RaceReference.CritDamage(hero);
            DodgeChance = RaceReference.DodgeChance(hero);
            BlockChance = RaceReference.BlockChance(hero);

            Aggro = RaceReference.Aggro(hero);

            IsAlive = true;
            Health = new Bar(RaceReference.Health(hero));
            Shield = new Bar(Health.MaxValue * 2, RaceReference.Shield(hero), 0);

            BindAbilities(SpecReference);
            BindAbilities(RaceReference);
            BindItemSkills();
        }

        private void BindItemSkills()
        {
            var abilities = ItemData.GenerateAbilities(HeroReference.Equip.Values);
            foreach (var ability in abilities)
            {
                var bind = ItemData.AbilitiesBindings[ability.Key];
                ((BattleEvent)bind.GetValue(this)).Event += ability.Value;
            }
        }

        public override string ToString()
        {
            return $"{HeroReference.ToString()} [{Health.Value}/{Health.MaxValue} | {Shield.Value}/{Shield.MaxValue}]";
        }
    }
}
