﻿using System.Collections.Generic;

namespace RPGPlugin.Core.Classes
{
    public enum AilmentType
    {
        Stun,

        Bleed,
        Poison,

        NagaRacePassive,
        DwarfRacePassive,
        ElementalRaceAura,
        HumanRaceAura,

        JuggernautSpecPassive
    }

    public class Ailment
    {
        public AilmentType Type;
        public double Power;
        public int TurnsLeft;
        public Creature Inflicter;
        public List<Buff> Buffs;
    }
}
