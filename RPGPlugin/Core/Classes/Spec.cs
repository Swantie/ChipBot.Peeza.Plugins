﻿using System;

namespace RPGPlugin.Core.Classes
{
    public abstract class Spec : BattleAbilities
    {
        public abstract int NumberOfAttacks(Stats stats, Hero hero);
        public abstract int NumberOfTargets(Stats stats, Hero hero);
        public abstract long Attack(Stats stats, Hero hero);
        public abstract long Defense(Stats stats, Hero hero);
        public abstract long Health(Stats stats, Hero hero);
        public abstract long Shield(Stats stats, Hero hero);
        public abstract double Accuracy(Stats stats, Hero hero);
        public abstract double CritChance(Stats stats, Hero hero);
        public abstract double CritDamage(Stats stats, Hero hero);
        public abstract double DodgeChance(Stats stats, Hero hero);
        public abstract double BlockChance(Stats stats, Hero hero);
        public abstract double UnblockableDamage(Stats stats, Hero hero);
        public abstract double Aggro(Stats stats, Hero hero);
        public abstract bool IsMelee();

        public Func<BattleEventArgs, Creature> PickTarget = null;
    }
}