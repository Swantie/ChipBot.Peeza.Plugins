﻿using ChipBot.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Classes
{
    public class Fight
    {
        private readonly List<Creature> _party;
        private readonly List<Creature> _enemies;
        private readonly Random _random;
        private readonly Action<string> _log;
        private bool ListContainsAlive(IEnumerable<Creature> creatures) => creatures.Any(c => c.IsAlive);
        private bool ShouldFight => PartyAlive && EnemiesAlive;

        public bool PartyAlive => ListContainsAlive(_party);
        public bool EnemiesAlive => ListContainsAlive(_enemies);

        public Fight(IEnumerable<Creature> party, IEnumerable<Creature> enemies, Action<string> log, Random random)
        {
            _party = new List<Creature>(party);
            _enemies = new List<Creature>(enemies);
            FillPartyRef(_party, _enemies);
            FillPartyRef(_enemies, _party);
            _random = random;
            _log = log;
        }

        public bool DoFight()
        {
            var order = _party.Concat(_enemies)
                .OrderRandomly(_random).ToList();

            OnFight(order, true);

            while (true)
            {
                if (!ShouldFight) break;

                foreach (var creature in order)
                {
                    if (!creature.IsAlive) continue;

                    var args = new BattleEventArgs()
                    {
                        Data = 0,
                        Flag = false,
                        Log = _log,
                        Rng = _random,
                        Self = creature,
                        Target = null,
                        Party = creature.Party,
                        Enemies = creature.Enemies
                    };
                    args.Target = creature.PickTarget(args);

                    creature.ProcessAilments.Raise(args);
                }
            }

            OnFight(order, false);

            return PartyAlive;
        }

        private void FillPartyRef(List<Creature> party, List<Creature> enemies)
        {
            party.ForEach(c => { c.Party = party; c.Enemies = enemies; });
        }

        private void OnFight(List<Creature> creatures, bool start)
        {
            foreach (var creature in creatures.Where(c => c.IsAlive))
            {
                var args = new BattleEventArgs()
                {
                    Data = 0,
                    Flag = false,
                    Log = _log,
                    Rng = _random,
                    Self = creature,
                    Target = null,
                    Party = creature.Party,
                    Enemies = creature.Enemies
                };

                (start ? creature.OnFightStart : creature.OnFightEnd).Raise(args);
            }
        }
    }
}
