﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Classes
{
    public class BattleEventArgs : EventArgs, ICloneable
    {
        public IEnumerable<Creature> Party;
        public IEnumerable<Creature> Enemies;
        public Creature Self;
        public Creature Target;

        public Action<string> Log;
        public Random Rng;

        public bool Flag;
        public object Data;

        public object Clone()
        {
            return new BattleEventArgs()
            {
                Party = Party,
                Enemies = Enemies,
                Self = Self,
                Target = Target,
                Log = Log,
                Rng = Rng,
                Flag = Flag,
                Data = Data
            };
        }

        public BattleEventArgs Reverse(bool preserveData = false)
        {
            var a = new BattleEventArgs
            {
                Party = Enemies,
                Enemies = Party,

                Self = Target,
                Target = Self,

                Log = Log,
                Rng = Rng
            };

            if (preserveData)
            {
                a.Flag = Flag;
                a.Data = Data;
            }

            return a;
        }
    }

    public delegate bool BattleEventHandler(BattleEventArgs args);

    public class BattleEvent
    {
        public BattleEvent()
        {
            Handlers = new List<BattleEventHandler>();
        }

        private List<BattleEventHandler> Handlers;
        public event BattleEventHandler Event
        {
            add { Handlers.Add(value); }
            remove { Handlers.Remove(value); }
        }
        public void Raise(BattleEventArgs args)
        {
            foreach (var e in Handlers.AsEnumerable().Reverse())
            {
                var res = e(args);
                if (res)
                {
                    break;
                }
            }
        }
    }
}
