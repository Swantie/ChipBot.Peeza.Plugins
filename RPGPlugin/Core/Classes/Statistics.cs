﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGPlugin.Core.Classes
{
    public class Statistics
    {
        [JsonProperty("dungeonsCleared")]
        public ulong DungeonsCleared { get; set; }

        [JsonProperty("dungeonsFailed")]
        public ulong DungeonsFailed { get; set; }

        [JsonProperty("enemiesKilled")]
        public ulong EnemiesKilled { get; set; }

        [JsonProperty("totalDamageDealt")]
        public ulong TotalDamageDealt { get; set; }

        [JsonProperty("totalDamageTaken")]
        public ulong TotalDamageTaken { get; set; }

        [JsonProperty("totalHealed")]
        public ulong TotalHealed { get; set; }

        [JsonProperty("totalDamageMitigated")]
        public ulong TotalDamageMitigated { get; set; }

        [JsonProperty("totalTimeInDungeons")]
        public TimeSpan TotalTimeInDungeons { get; set; }

        [JsonProperty("bossesKilled")]
        public ulong BossesKilled { get; set; }

        [JsonProperty("itemsCollected")]
        public ulong ItemsCollected { get; set; }

        [JsonProperty("classSpecific")]
        public ulong ClassSpecific { get; set; }
    }
}
