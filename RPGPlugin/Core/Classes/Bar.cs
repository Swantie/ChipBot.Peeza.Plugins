﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGPlugin.Core.Classes
{
    public class Bar
    {
        private long _value;
        public long Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;

                if (_value > _maxValue)
                {
                    _value = _maxValue;
                }
                if (_value < MinValue)
                {
                    _value = _minValue;
                }
            }
        }
        private long _maxValue;
        public long MaxValue
        {
            get
            {
                return _maxValue;
            }
        }
        private long _minValue;
        public long MinValue
        {
            get
            {
                return _minValue;
            }
        }

        public double Percentage
        {
            get
            {
                return (_value - _minValue) * 1.0 / (_maxValue - _minValue);
            }
        }

        public Bar(long max, long min = 0, bool setToMax = true)
        {
            _maxValue = max;
            _minValue = min;

            if (setToMax)
            {
                _value = _maxValue;
            }
            else
            {
                _value = _minValue;
            }
        }

        public Bar(long max, long val, long min = 0)
        {
            _maxValue = max;
            _minValue = min;
            Value = val;
        }
    }
}
