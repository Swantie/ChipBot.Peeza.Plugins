﻿using RPGPlugin.Core.Extensions;
using RPGPlugin.Core.Static;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Classes
{
    [Flags]
    public enum CreatureFlags
    {
        None = 0,
        CantRessurect = 1,
        CantInstaKill = 2,
    }

    public abstract class Creature
    {
        public string Name;

        public int NumberOfAttacks;
        public int NumberOfTargets;

        private long _attack;
        public long Attack
        {
            get
            {
                return ApplyBuffs(_attack, BuffType.Attack);
            }
            set
            {
                _attack = value;
            }
        }
        private long _defense;
        public long Defense
        {
            get
            {
                return ApplyBuffs(_defense, BuffType.Defense);
            }
            set
            {
                _defense = value;
            }
        }

        private double _accuracy;
        public double Accuracy
        {
            get
            {
                return ApplyBuffs(_accuracy, BuffType.Accuracy);
            }
            set
            {
                _accuracy = value;
            }
        }
        private double _critChance;
        public double CritChance
        {
            get
            {
                return ApplyBuffs(_critChance, BuffType.CritChance);
            }
            set
            {
                _critChance = value;
            }
        }
        private double _critDamage;
        public double CritDamage
        {
            get
            {
                return ApplyBuffs(_critDamage, BuffType.CritDamage);
            }
            set
            {
                _critDamage = value;
            }
        }
        private double _dodgeChance;
        public double DodgeChance
        {
            get
            {
                return ApplyBuffs(_dodgeChance, BuffType.DodgeChance);
            }
            set
            {
                _dodgeChance = value;
            }
        }
        private double _blockChance;
        public double BlockChance
        {
            get
            {
                return ApplyBuffs(_blockChance, BuffType.BlockChance);
            }
            set
            {
                _blockChance = value;
            }
        }
        private double _unblockableDamage;
        public double UnblockableDamage
        {
            get
            {
                return _unblockableDamage;
            }
            set
            {
                _unblockableDamage = value;
            }
        }

        private double _aggro;
        public double Aggro
        {
            get
            {
                return ApplyBuffs(_aggro, BuffType.Aggro);
            }
            set
            {
                _aggro = value;
            }
        }

        public bool IsMelee { get; protected set; }

        public CreatureFlags Flags { get; set; }

        public List<Buff> Buffs;
        public List<Ailment> Ailments;

        public Bar Health;
        public Bar Shield;

        public bool IsAlive;

        public Statistics Statistics;

        public BattleEvent ProcessAilments;
        public BattleEvent ProcessBuffs;
        public BattleEvent OnPreAttack;
        public BattleEvent OnAttack;
        public BattleEvent OnAfterAttack;
        public BattleEvent OnPreGetAttack;
        public BattleEvent OnGetAttack;
        public BattleEvent OnHealthChange;
        public BattleEvent OnMiss;
        public BattleEvent OnCrit;
        public BattleEvent OnDodge;
        public BattleEvent OnBlock;
        public BattleEvent OnGetHeal;
        public BattleEvent OnDeath;
        public BattleEvent OnRessurection;
        public BattleEvent OnKill;
        public BattleEvent OnFightStart;
        public BattleEvent OnFightEnd;
        public BattleEvent OnDungeonStart;
        public BattleEvent OnDungeonEnd;

        public List<Creature> Party { get; set; }
        public List<Creature> Enemies { get; set; }

        public virtual Creature PickTarget(BattleEventArgs args)
        {
            var alive = args.Enemies.Where(e => e.IsAlive);
            if (alive.Count() <= 0) return null;

            return alive.RandomByWeight(e => e.Aggro, args.Rng);
        }

        public Creature()
        {
            Buffs = new List<Buff>();
            Ailments = new List<Ailment>();

            ProcessAilments = new BattleEvent();
            ProcessAilments.Event += AilmentsHandler.MainHandler;

            ProcessBuffs = new BattleEvent();
            ProcessBuffs.Event += BasicBattleAbilities.ProcessBuffs;

            OnPreAttack = new BattleEvent();
            OnPreAttack.Event += BasicBattleAbilities.OnPreAttack;
            OnPreAttack.Event += BasicBattleAbilities.OnPreAttackCheckTarget;

            OnAttack = new BattleEvent();
            OnAttack.Event += BasicBattleAbilities.OnAttack;

            OnAfterAttack = new BattleEvent();

            OnPreGetAttack = new BattleEvent();

            OnGetAttack = new BattleEvent();
            OnGetAttack.Event += BasicBattleAbilities.OnGetAttackHealth;
            OnGetAttack.Event += BasicBattleAbilities.OnGetAttackShield;
            OnGetAttack.Event += BasicBattleAbilities.OnGetAttack;

            OnHealthChange = new BattleEvent();
            OnHealthChange.Event += BasicBattleAbilities.OnHealthChange;

            OnMiss = new BattleEvent();
            OnMiss.Event += BasicBattleAbilities.OnMiss;

            OnCrit = new BattleEvent();
            OnCrit.Event += BasicBattleAbilities.OnCrit;

            OnDodge = new BattleEvent();
            OnDodge.Event += BasicBattleAbilities.OnDodge;

            OnBlock = new BattleEvent();
            OnBlock.Event += BasicBattleAbilities.OnBlock;

            OnGetHeal = new BattleEvent();
            OnGetHeal.Event += BasicBattleAbilities.OnGetHeal;

            OnDeath = new BattleEvent();
            OnDeath.Event += BasicBattleAbilities.OnDeath;

            OnRessurection = new BattleEvent();
            OnRessurection.Event += BasicBattleAbilities.OnRessurection;

            OnKill = new BattleEvent();

            OnFightStart = new BattleEvent();

            OnFightEnd = new BattleEvent();

            OnDungeonStart = new BattleEvent();

            OnDungeonEnd = new BattleEvent();
        }

        protected void BindAbilities(BattleAbilities abilities)
        {
            if (abilities.ProcessAilments != null) ProcessAilments.Event += abilities.ProcessAilments;
            if (abilities.ProcessBuffs != null) ProcessBuffs.Event += abilities.ProcessBuffs;
            if (abilities.OnPreAttack != null) OnPreAttack.Event += abilities.OnPreAttack;
            if (abilities.OnAttack != null) OnAttack.Event += abilities.OnAttack;
            if (abilities.OnAfterAttack != null) OnAfterAttack.Event += abilities.OnAfterAttack;
            if (abilities.OnPreGetAttack != null) OnPreGetAttack.Event += abilities.OnPreGetAttack;
            if (abilities.OnGetAttack != null) OnGetAttack.Event += abilities.OnGetAttack;
            if (abilities.OnHealthChange != null) OnHealthChange.Event += abilities.OnHealthChange;
            if (abilities.OnMiss != null) OnMiss.Event += abilities.OnMiss;
            if (abilities.OnCrit != null) OnCrit.Event += abilities.OnCrit;
            if (abilities.OnDodge != null) OnDodge.Event += abilities.OnDodge;
            if (abilities.OnBlock != null) OnBlock.Event += abilities.OnBlock;
            if (abilities.OnGetHeal != null) OnGetHeal.Event += abilities.OnGetHeal;
            if (abilities.OnDeath != null) OnDeath.Event += abilities.OnDeath;
            if (abilities.OnRessurection != null) OnRessurection.Event += abilities.OnRessurection;
            if (abilities.OnKill != null) OnKill.Event += abilities.OnKill;
            if (abilities.OnFightStart != null) OnFightStart.Event += abilities.OnFightStart;
            if (abilities.OnFightEnd != null) OnFightEnd.Event += abilities.OnFightEnd;
            if (abilities.OnDungeonStart != null) OnDungeonStart.Event += abilities.OnDungeonStart;
            if (abilities.OnDungeonEnd != null) OnDungeonEnd.Event += abilities.OnDungeonEnd;
        }

        private long ApplyBuffs(long value, BuffType type)
            => (long)ApplyBuffs((double)value, type);
        private double ApplyBuffs(double value, BuffType type)
        {
            var buffs = Buffs.Concat(Ailments.SelectMany(a => a.Buffs));

            foreach (var b in buffs)
            {
                if (b.Type != type) continue;
                if (b.Amplify)
                {
                    value *= b.Value;
                }
                else
                {
                    value += b.Value;
                }
            }

            return value;
        }
    }
}
