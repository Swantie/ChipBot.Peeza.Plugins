﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Classes
{
    public class Stats
    {
        [JsonProperty("str")]
        public int Strength { get; set; }
        [JsonProperty("dex")]
        public int Dexterity { get; set; }
        [JsonProperty("int")]
        public int Intellegence { get; set; }
        [JsonProperty("vit")]
        public int Vitality { get; set; }
        [JsonProperty("tec")]
        public int Technique { get; set; }
        [JsonProperty("luk")]
        public int Luck { get; set; }

        public int Sum()
        {
            return Strength + Dexterity + Intellegence + Vitality + Technique + Luck;
        }

        public Stats(int Strength, int Dexterity, int Intellegence, int Vitality, int Technique, int Luck, bool canBeZero = true)
        {
            this.Strength = Strength;
            this.Dexterity = Dexterity;
            this.Intellegence = Intellegence;
            this.Vitality = Vitality;
            this.Technique = Technique;
            this.Luck = Luck;

            if (!canBeZero)
            {
                if (this.Strength <= 0) this.Strength = 1;
                if (this.Dexterity <= 0) this.Dexterity = 1;
                if (this.Intellegence <= 0) this.Intellegence = 1;
                if (this.Vitality <= 0) this.Vitality = 1;
                if (this.Technique <= 0) this.Technique = 1;
                if (this.Luck <= 0) this.Luck = 1;
            }
        }

        public Stats(int[] stats)
        {
            Strength = stats[0];
            Dexterity = stats[1];
            Intellegence = stats[2];
            Vitality = stats[3];
            Technique = stats[4];
            Luck = stats[5];
        }

        public static implicit operator int[] (Stats S)
        {
            return new int[6] { S.Strength, S.Dexterity, S.Intellegence, S.Vitality, S.Technique, S.Luck };
        }

        public static Stats operator +(Stats S1, Stats S2)
        {
            return new Stats(S1.Strength + S2.Strength, S1.Dexterity + S2.Dexterity, S1.Intellegence + S2.Intellegence, S1.Vitality + S2.Vitality, S1.Technique + S2.Technique, S1.Luck + S2.Luck, false);
        }

        public static Stats operator +(Stats S1, IEnumerable<Stats> S2)
        {
            return new Stats(
                S1.Strength + S2.Select(x => x.Strength).Sum(),
                S1.Dexterity + S2.Select(x => x.Dexterity).Sum(),
                S1.Intellegence + S2.Select(x => x.Intellegence).Sum(),
                S1.Vitality + S2.Select(x => x.Vitality).Sum(),
                S1.Technique + S2.Select(x => x.Technique).Sum(),
                S1.Luck + S2.Select(x => x.Luck).Sum(),
                false);
        }

        public static Stats operator -(Stats S1, Stats S2)
        {
            return new Stats(S1.Strength - S2.Strength, S1.Dexterity - S2.Dexterity, S1.Intellegence - S2.Intellegence, S1.Vitality - S2.Vitality, S1.Technique - S2.Technique, S1.Luck - S2.Luck, false);
        }

        public override string ToString()
        {
            return $"STR: {Strength}\nDEX: {Dexterity}\nINT: {Intellegence}\nVIT: {Vitality}\nTEC: {Technique}\nLUK: {Luck}";
        }

        public string ToStringShort(string separator)
        {
            return string.Join(separator, new string[] {
                    Strength == 0 ? null : $"STR{Strength.ToString("+0;-#")}",
                    Dexterity == 0 ? null : $"DEX{Dexterity.ToString("+0;-#")}",
                    Intellegence == 0 ? null : $"INT{Intellegence.ToString("+0;-#")}",
                    Vitality == 0 ? null : $"VIT{Vitality.ToString("+0;-#")}",
                    Technique == 0 ? null : $"TEC{Technique.ToString("+0;-#")}",
                    Luck == 0 ? null : $"LUK{Luck.ToString("+0;-#")}"
                }.Where(s => s != null));
        }

        public string ToStringLong(Stats s)
        {
            return string.Join("\n",
                $"STR: {Strength - s.Strength,-3} { (s.Strength == 0 ? null : s.Strength.ToString("+0;-#")) }",
                $"DEX: {Dexterity - s.Dexterity,-3} { (s.Dexterity == 0 ? null : s.Dexterity.ToString("+0;-#")) }",
                $"INT: {Intellegence - s.Intellegence,-3} { (s.Intellegence == 0 ? null : s.Intellegence.ToString("+0;-#")) }",
                $"VIT: {Vitality - s.Vitality,-3} { (s.Vitality == 0 ? null : s.Vitality.ToString("+0;-#")) }",
                $"TEC: {Technique - s.Technique,-3} { (s.Technique == 0 ? null : s.Technique.ToString("+0;-#")) }",
                $"LUK: {Luck - s.Luck,-3} { (s.Luck == 0 ? null : s.Luck.ToString("+0;-#")) }"
            );
        }
    }
}