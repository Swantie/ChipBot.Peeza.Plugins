﻿using RPGPlugin.Core.Static.Specs;
using System;

namespace RPGPlugin.Core.Classes
{
    public class SpecDescription
    {
        public SpecEnum Name { get; private set; }
        public bool BaseClass { get; }
        public SpecEnum? AdvancedOf { get; private set; }
        public string Description { get; private set; }
        public string ShortDescription { get; private set; }
        public string ClassStatistic { get; private set; }
        public Stats StatsBonuses { get; private set; }
        public Type Spec { get; private set; }
        public int LevelNeeded { get; private set; }
        public SpecDescription(SpecEnum name, SpecEnum? advancedOf, string description, string shortDescription, string classStat, Stats bonusStats, Type spec, int levelNeeded = 0)
        {
            Name = name;
            BaseClass = !advancedOf.HasValue;
            AdvancedOf = advancedOf;
            Description = description;
            ShortDescription = shortDescription;
            ClassStatistic = classStat;
            StatsBonuses = bonusStats;
            Spec = spec;
            LevelNeeded = levelNeeded;
        }
        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
