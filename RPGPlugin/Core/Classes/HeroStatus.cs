﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace RPGPlugin.Core.Classes
{
    public class HeroStatus
    {
        [JsonProperty("partyLeader")]
        public ulong PartyLeader { get; set; }

        [JsonProperty("party")]
        public List<ulong> Party { get; set; }

        [JsonProperty("status")]
        public HeroStatusEnum Status { get; set; }
    }

    [Flags]
    public enum HeroStatusEnum
    {
        Idling = 0,
        InDungeon = 1,
    }
}
