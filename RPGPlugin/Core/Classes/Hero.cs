﻿using Newtonsoft.Json;
using RPGPlugin.Core.Static.Races;
using RPGPlugin.Core.Static.Specs;
using RPGPlugin.Services;
using System.Collections.Generic;
using System.Linq;

namespace RPGPlugin.Core.Classes
{
    public class Hero
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("exp")]
        public ulong Experience { get; set; }
        [JsonIgnore]
        public ulong ExperienceNeeded { get; set; }
        [JsonIgnore]
        public uint Level { get; set; }
        [JsonProperty("attrPts")]
        public uint AttributePoints { get; set; }

        [JsonProperty("rebornTokens")]
        public uint RebornTokens { get; set; }
        [JsonProperty("rebornLvl")]
        public uint RebornLevel { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }
        [JsonProperty("usrId")]
        public ulong UserID { get; set; }

        [JsonProperty("race")]
        public RacesEnum Race { get; set; }
        [JsonIgnore]
        public Race RaceReference { get; set; }
        private RaceDescription _raceDescription;
        [JsonIgnore]
        public RaceDescription RaceDescription { get { return _raceDescription; } set { _raceDescription = value; Race = _raceDescription.Name; } }

        [JsonProperty("class")]
        public SpecEnum Class => SpecDescription.Name;
        [JsonIgnore]
        public Spec SpecReference { get; set; }
        [JsonIgnore]
        public SpecDescription SpecDescription { get; set; }

        [JsonIgnore]
        private Stats HeroBaseStats { get; set; }
        [JsonProperty("stats")]
        public Stats HeroStats { get; private set; }

        [JsonProperty("statistics")]
        public Statistics Statistics { get; set; }

        [JsonProperty("status")]
        public HeroStatus Status { get; set; }

        [JsonProperty("equip")]
        public Dictionary<ItemType, Item> Equip { get; set; }

        [JsonProperty("inventory")]
        public List<Item> Inventory { get; set; }

        [JsonIgnore]
        public Stats Stats
            => HeroBaseStats + HeroStats
            + SpecDescription.StatsBonuses
            + RaceDescription.StatsBonuses
            + Equip.Values.Select(i => i.Stats);

        public override string ToString()
        {
            return $"{Name} - {Race} {Class} {Level}{(RebornLevel > 0 ? $" ({RebornLevel}r)" : "")} lvl";
        }

        public Hero()
        {
            Level = 1;
            HeroBaseStats = new Stats(2, 2, 2, 2, 2, 2);
            HeroStats = new Stats(0, 0, 0, 0, 0, 0);
            Statistics = new Statistics();
            Equip = new Dictionary<ItemType, Item>();
            Inventory = new List<Item>();
            Status = new HeroStatus() { Party = new List<ulong>(), Status = HeroStatusEnum.Idling };
        }
    }
}