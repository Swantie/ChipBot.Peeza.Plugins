﻿namespace RPGPlugin.Core.Classes
{
    public enum BuffType
    {
        Attack,
        Defense,

        Accuracy,
        CritChance,
        CritDamage,
        DodgeChance,
        BlockChance,

        Aggro
    }

    public class Buff
    {
        public bool Negative;
        public bool Amplify;

        public BuffType Type;
        public double Value;
        public int TurnsLeft;
    }
}
