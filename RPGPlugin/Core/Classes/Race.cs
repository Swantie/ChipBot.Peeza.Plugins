﻿namespace RPGPlugin.Core.Classes
{
    public abstract class Race : BattleAbilities
    {
        public virtual int NumberOfAttacks(Hero hero)
        {
            return hero.SpecReference.NumberOfAttacks(hero.Stats, hero);
        }
        public virtual int NumberOfTargets(Hero hero)
        {
            return hero.SpecReference.NumberOfTargets(hero.Stats, hero);
        }
        public virtual long Attack(Hero hero)
        {
            return hero.SpecReference.Attack(hero.Stats, hero);
        }
        public virtual long Defense(Hero hero)
        {
            return hero.SpecReference.Defense(hero.Stats, hero);
        }
        public virtual long Health(Hero hero)
        {
            return hero.SpecReference.Health(hero.Stats, hero);
        }
        public virtual long Shield(Hero hero)
        {
            return hero.SpecReference.Shield(hero.Stats, hero);
        }
        public virtual double Accuracy(Hero hero)
        {
            return hero.SpecReference.Accuracy(hero.Stats, hero);
        }
        public virtual double CritChance(Hero hero)
        {
            return hero.SpecReference.CritChance(hero.Stats, hero);
        }
        public virtual double CritDamage(Hero hero)
        {
            return hero.SpecReference.CritDamage(hero.Stats, hero);
        }
        public virtual double DodgeChance(Hero hero)
        {
            return hero.SpecReference.DodgeChance(hero.Stats, hero);
        }
        public virtual double BlockChance(Hero hero)
        {
            return hero.SpecReference.BlockChance(hero.Stats, hero);
        }
        public virtual double UnblockableDamage(Hero hero)
        {
            return hero.SpecReference.UnblockableDamage(hero.Stats, hero);
        }

        public virtual double Aggro(Hero hero)
        {
            return hero.SpecReference.Aggro(hero.Stats, hero);
        }
    }
}