﻿using RPGPlugin.Core.Static.Races;
using System;

namespace RPGPlugin.Core.Classes
{
    public class RaceDescription
    {
        public RacesEnum Name { get; private set; }
        public string Description { get; private set; }
        public Stats StatsBonuses { get; private set; }
        public Type Race { get; private set; }

        public RaceDescription(RacesEnum name, string description, Stats bonusStats, Type race)
        {
            Name = name;
            Description = description;
            StatsBonuses = bonusStats;
            Race = race;
        }
        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
