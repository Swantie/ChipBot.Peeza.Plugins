﻿using Newtonsoft.Json;

namespace RPGPlugin.Core.Classes
{
    public abstract class BattleAbilities
    {
        [JsonIgnore]
        public BattleEventHandler ProcessAilments { get; set; }
        [JsonIgnore]
        public BattleEventHandler ProcessBuffs { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnPreAttack { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnAttack { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnAfterAttack { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnPreGetAttack { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnGetAttack { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnHealthChange { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnMiss { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnCrit { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnDodge { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnBlock { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnGetHeal { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnDeath { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnRessurection { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnKill { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnFightStart { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnFightEnd { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnDungeonStart { get; set; }
        [JsonIgnore]
        public BattleEventHandler OnDungeonEnd { get; set; }
    }
}
