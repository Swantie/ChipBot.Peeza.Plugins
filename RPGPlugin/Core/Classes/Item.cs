﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGPlugin.Core.Classes
{
    public enum ItemType
    {
        Weapon,
        Helmet,
        Armor,
        Cape,
        Necklace,
        Ring,
        Trinket
    }

    [Flags]
    public enum ItemRarity
    {
        Common = 0,
        Uncommon = 1,
        Rare = 2,
        Unique = 4,
        Legendary = 8,
        Charged = 16,
        Legacy = 32
    }

    public enum AbilityType
    {
        Poison,
        Bleed,
        Lifesteal,

        PoisonResistance,
        BleedResistance,
        StunResistance,

        Block,

        HealAmplifier,
        ShieldCharger,

        Repeater
    }

    public enum StackingType
    {
        None,
        Power,
        Chance,
        Duration
    }

    public class Item
    {
        [JsonProperty("type")]
        public ItemType Type { get; set; }
        [JsonProperty("stats")]
        public Stats Stats { get; set; }
        [JsonProperty("rarity")]
        public ItemRarity Rarity { get; set; }
        [JsonProperty("itemLevel")]
        public int ItemLevel { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }
        [JsonProperty("postfix")]
        public string Postfix { get; set; }

        [JsonProperty("abilityPower")]
        public double AbilityPower { get; set; }
        [JsonProperty("abilityChance")]
        public double AbilityChance { get; set; }
        [JsonProperty("abilityDuration")]
        public double AbilityDuration { get; set; }
        [JsonProperty("ability")]
        public AbilityType? Ability { get; set; } 

        public override string ToString()
        {
            string ItemName = "";

            if (!string.IsNullOrEmpty(Prefix))
                ItemName += Prefix + " ";

            ItemName += Type;

            if (!string.IsNullOrEmpty(Postfix))
                ItemName += " of " + Postfix;

            return $"[{ItemName}][{Stats.ToStringShort(", ")}]<{Rarity}>{(Ability.HasValue ? "⚡" : "")}";
        }
    }
}
