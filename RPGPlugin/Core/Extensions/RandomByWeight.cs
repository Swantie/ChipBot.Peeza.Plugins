﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace RPGPlugin.Core.Extensions
{
    public static class IEnumerableExtensions
    {
        public static T RandomByWeight<T>(this IEnumerable<T> sequence, Func<T, double> weightSelector, Random rng = null)
        {
            if (rng == null) rng = new Random();

            double totalWeight = sequence.Sum(weightSelector);
            double itemWeightIndex = rng.NextDouble() * totalWeight;
            double currentWeightIndex = 0;

            foreach (var item in sequence)
            {
                currentWeightIndex += weightSelector(item);
                if (currentWeightIndex >= itemWeightIndex) return item;
            }

            return default;
        }
    }
}
