﻿using Newtonsoft.Json;

namespace RPGPlugin.Core.Config
{
    public class RPGCoreConfiguration
    {
        [JsonProperty("baseDropChanceBonus")]
        public double BaseDropChanceBonus { get; set; } = 0;

        [JsonProperty("dropChancePerReborn")]
        public double DropChancePerReborn { get; set; } = 0.01;


        [JsonProperty("expNeeded")]
        public ulong ExpNeeded { get; set; } = 50;

        [JsonProperty("expCurve")]
        public double ExpCurve { get; set; } = 1.1;

        [JsonProperty("attributesPerLevel")]
        public uint AttributesPerLevel { get; set; } = 2;

        [JsonProperty("maxLevel")]
        public uint MaxLevel { get; set; } = 50;

        [JsonProperty("baseExpMultiplier")]
        public double BaseExpMultiplier { get; set; } = 1;

        [JsonProperty("expPerReborn")]
        public double ExpPerReborn { get; set; } = 0.03;


        [JsonProperty("maxPartySize")]
        public uint MaxPartySize { get; set; } = 4;

        [JsonProperty("minLevelForParty")]
        public uint MinLevelForParty { get; set; } = 5;
    }

}
