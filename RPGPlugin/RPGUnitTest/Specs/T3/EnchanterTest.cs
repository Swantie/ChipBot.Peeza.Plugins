﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class EnchanterTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroCleric;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroCleric = Static.CreateDungeonHero("healer", "elemental", "enchanter");

            heroWarrior = Static.CreateDungeonHero("someRandomGuy", "human", "warrior");

            TestContext.WriteLine($"{heroCleric.HeroReference.ToString()} : {heroCleric.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestHeal()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroWarrior },
                Party = new List<Creature>() { heroCleric },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroCleric,
                Target = heroWarrior
            };

            heroCleric.ProcessAilments.Raise(args);

            Assert.IsTrue(heroCleric.Shield.Value > 1);
        }
    }
}
