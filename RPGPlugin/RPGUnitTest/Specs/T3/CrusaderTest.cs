﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class CrusaderTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroCleric;
        private DungeonHero heroCr;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroCleric = Static.CreateDungeonHero("healer", "elemental", "cleric");
            heroCr = Static.CreateDungeonHero("healRedirecter", "human", "crusader");
            heroCr.Health.Value = 1;
            heroWarrior = Static.CreateDungeonHero("someRandomGuy", "human", "warrior");
            heroWarrior.Health.Value = 1;
        }

        [TestMethod]
        public void TestHealRedirect()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroWarrior },
                Party = new List<Creature>() { heroCr, heroCleric },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroCleric,
                Target = heroWarrior
            };

            heroCleric.ProcessAilments.Raise(args);

            Assert.IsTrue(!heroWarrior.IsAlive);
        }
    }
}
