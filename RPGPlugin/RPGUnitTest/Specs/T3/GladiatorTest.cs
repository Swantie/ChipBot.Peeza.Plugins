﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;
using System.Linq;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class GladiatorTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroGlad;

        private List<Creature> enemies;

        [TestInitialize]
        public void TestConstr()
        {
            heroGlad = Static.CreateDungeonHero("valakas", "human", "gladiator");
            heroGlad.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.Accuracy, Value = 1 });
            heroGlad.HeroReference.HeroStats.Strength += 20;
            heroGlad.NumberOfTargets = heroGlad.RaceReference.NumberOfTargets(heroGlad.HeroReference);

            enemies = new List<Creature>();

            for (var i = 0; i < 3; i++)
            {
                var e = Static.CreateDungeonHero($"someRandomGuy#{i}", "human", "warrior");
                e.Health.Value = 1;
                e.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.DodgeChance, Value = -2 });
                e.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.BlockChance, Value = -2 });
                enemies.Add(e);
            }
        }

        [TestMethod]
        public void TestAoeAttack()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = enemies,
                Party = new List<Creature>() { heroGlad },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroGlad,
                Target = null
            };

            args.Target = heroGlad.PickTarget(args);
            heroGlad.ProcessAilments.Raise(args);

            Assert.IsTrue(enemies.All(e => !e.IsAlive));
        }
    }
}
