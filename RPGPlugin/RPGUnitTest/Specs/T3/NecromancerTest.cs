﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Static.Specs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class NecromancerTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroNecr;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroNecr = Static.CreateDungeonHero("necr", "human", "necromancer");
            heroNecr.Health = new Bar(2);

            heroWarrior = Static.CreateDungeonHero("monkaS", "dummy", "warrior");
            heroWarrior.Health = new Bar(2);

            TestContext.WriteLine($"{heroNecr.HeroReference.ToString()} : {heroNecr.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestSummon()
        {
            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroNecr };
            var e = new List<Creature> { heroWarrior };

            heroNecr.Buffs.Clear();
            heroWarrior.Buffs.Clear();
            heroWarrior.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroWarrior.Health.Value = heroWarrior.Health.MaxValue;
            heroWarrior.IsAlive = true;

            heroNecr.OnDungeonStart.Raise(new BattleEventArgs { Data = null, Flag = false, Rng = Static.Random, Enemies = e, Party = p, Log = log, Self = heroNecr, Target = heroWarrior });
            var f = new Fight(p, e, (string t) => TestContext.WriteLine(t), Static.Random);
            f.DoFight();

            Assert.IsTrue(((Necromancer)heroNecr.SpecReference).AliveSummons.Count() > 0);
        }

        [TestMethod]
        public void TestRespectFlag()
        {
            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroNecr };
            var e = new List<Creature> { heroWarrior };

            heroNecr.Buffs.Clear();
            heroWarrior.Buffs.Clear();
            heroWarrior.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroWarrior.Health.Value = heroWarrior.Health.MaxValue;
            heroWarrior.IsAlive = true;
            heroWarrior.Flags |= CreatureFlags.CantRessurect;

            heroNecr.OnDungeonStart.Raise(new BattleEventArgs { Data = null, Flag = false, Rng = Static.Random, Enemies = e, Party = p, Log = log, Self = heroNecr, Target = heroWarrior });
            var f = new Fight(p, e, (string t) => TestContext.WriteLine(t), Static.Random);
            f.DoFight();

            Assert.IsTrue(((Necromancer)heroNecr.SpecReference).AliveSummons.Count() == 0);
            Assert.IsTrue(!heroWarrior.IsAlive);
        }

        [TestMethod]
        public void TestSummonBlock()
        {
            TestSummon();

            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroNecr };
            var e = new List<Creature> { heroWarrior };

            heroNecr.Buffs.Clear();
            heroWarrior.Buffs.Clear();
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Accuracy, Value = 2 });
            heroWarrior.Health.Value = heroWarrior.Health.MaxValue;
            heroWarrior.IsAlive = true;

            heroWarrior.ProcessAilments.Raise(new BattleEventArgs() { Data = null, Flag = false, Enemies = p, Party = e, Log = log, Rng = Static.Random, Self = heroWarrior, Target = heroNecr });

            Assert.IsTrue(((Necromancer)heroNecr.SpecReference).AliveSummons.Count() == 0 && heroNecr.IsAlive);
        }

        [TestMethod]
        public void TestSummonAttack()
        {
            TestSummon();

            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroNecr };
            var e = new List<Creature> { heroWarrior };

            heroNecr.Buffs.Clear();
            heroNecr.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroWarrior.Buffs.Clear();
            heroWarrior.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroWarrior.Health.Value = heroWarrior.Health.MaxValue;
            heroWarrior.IsAlive = true;

            var f = new Fight(p, e, (string t) => TestContext.WriteLine(t), Static.Random);
            f.DoFight();

            Assert.IsTrue(!heroWarrior.IsAlive);
        }
    }
}
