﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;
using System.Linq;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class ArchWizardTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroWz;

        private List<Creature> enemies;
        
        [TestInitialize]
        public void TestConstr()
        {
            heroWz = Static.CreateDungeonHero("gandalf", "human", "archwizard");
            heroWz.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.Accuracy, Value = 1 });
            heroWz.HeroReference.HeroStats.Intellegence += 60;
            heroWz.NumberOfTargets = heroWz.RaceReference.NumberOfTargets(heroWz.HeroReference);

            enemies = new List<Creature>();

            for (var i = 0; i < 3; i++)
            {
                var e = Static.CreateDungeonHero($"someRandomGuy#{i}", "human", "warrior");
                e.Health.Value = 1;
                e.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.DodgeChance, Value = -2 });
                e.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.BlockChance, Value = -2 });
                enemies.Add(e);
            }
        }

        [TestMethod]
        public void TestAoeAttack()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = enemies,
                Party = new List<Creature>() { heroWz },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroWz,
                Target = null
            };

            args.Target = heroWz.PickTarget(args);
            heroWz.ProcessAilments.Raise(args);

            Assert.IsTrue(enemies.All(e => !e.IsAlive));
        }
    }
}
