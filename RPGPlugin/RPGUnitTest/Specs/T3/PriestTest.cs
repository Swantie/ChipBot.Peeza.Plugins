﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class PriestTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroCleric;
        private DungeonHero heroRandomGuy;
        private DungeonHero heroRandomGuy2;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroCleric = Static.CreateDungeonHero("healer", "elemental", "priest");
            heroRandomGuy = Static.CreateDungeonHero("someDude", "human", "warrior");
            heroRandomGuy2 = Static.CreateDungeonHero("someDude", "human", "warrior");
            heroCleric.Health.Value = 1;
            heroRandomGuy.Health.Value = 1;
            heroRandomGuy2.Health.Value = 1;

            heroWarrior = Static.CreateDungeonHero("someRandomGuy", "human", "warrior");

            TestContext.WriteLine($"{heroCleric.HeroReference.ToString()} : {heroCleric.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestHeal()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroWarrior },
                Party = new List<Creature>() { heroCleric, heroRandomGuy, heroRandomGuy2 },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroCleric,
                Target = heroWarrior
            };

            heroCleric.ProcessAilments.Raise(args);

            Assert.IsTrue(heroCleric.Health.Value > 1);
            Assert.IsTrue(heroRandomGuy.Health.Value > 1);
            Assert.IsTrue(heroRandomGuy2.Health.Value > 1);
        }
    }
}
