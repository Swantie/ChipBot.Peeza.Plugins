﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class CultAssassinTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroAssassin;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroAssassin = Static.CreateDungeonHero("shade", "human", "cultassassin");

            heroWarrior = Static.CreateDungeonHero("monkaS", "human", "warrior");

            TestContext.WriteLine($"{heroAssassin.HeroReference.ToString()} : {heroAssassin.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestHealOnKill()
        {
            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroAssassin };
            var e = new List<Creature> { heroWarrior };

            heroWarrior.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Accuracy, Value = -2 });
            heroWarrior.Health.Value = 1;
            heroWarrior.IsAlive = true;

            heroAssassin.Health.Value = 1;

            heroAssassin.OnDungeonStart.Raise(new BattleEventArgs { Data = null, Flag = false, Rng = Static.Random, Enemies = e, Party = p, Log = log, Self = heroAssassin, Target = heroWarrior });
            var f = new Fight(p, e, (string t) => TestContext.WriteLine(t), Static.Random);
            f.DoFight();

            Assert.IsTrue(heroAssassin.Health.Value > 1);
        }
    }
}
