﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class TemplarTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroTemplar;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroTemplar = Static.CreateDungeonHero("shiny", "human", "templar");
            heroTemplar.HeroReference.HeroStats.Intellegence = 15;
            heroTemplar.Health = new Bar(1);
            heroTemplar.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.BlockChance, Value = -1 });
            heroTemplar.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.DodgeChance, Value = -1 });

            heroWarrior = Static.CreateDungeonHero("monkaS", "human", "warrior");
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.BlockChance, Value = -1 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.DodgeChance, Value = -1 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Accuracy, Value = 1 });

            TestContext.WriteLine($"{heroTemplar.HeroReference.ToString()} : {heroTemplar.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestRevive()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroTemplar },
                Party = new List<Creature>() { heroWarrior },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroWarrior,
                Target = heroTemplar
            };

            heroTemplar.OnDungeonStart.Raise(args.Reverse());
            heroWarrior.ProcessAilments.Raise(args);

            Assert.IsTrue(heroTemplar.IsAlive);
        }
    }
}
