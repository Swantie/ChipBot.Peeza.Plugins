﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class DeathKnightTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroDK;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroDK = Static.CreateDungeonHero("shiny", "human", "deathknight");
            heroDK.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.BlockChance, Value = -1 });
            heroDK.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.DodgeChance, Value = -1 });

            heroWarrior = Static.CreateDungeonHero("monkaS", "human", "warrior");
            heroWarrior.Health = new Bar(1);
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.BlockChance, Value = -1 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.DodgeChance, Value = -1 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Accuracy, Value = 1 });

            TestContext.WriteLine($"{heroDK.HeroReference.ToString()} : {heroDK.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestReflect()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroDK },
                Party = new List<Creature>() { heroWarrior },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroWarrior,
                Target = heroDK
            };

            for (var i = 0; i < 100 && heroWarrior.IsAlive; i++) {
                args.Data = null;
                args.Flag = false;
                heroWarrior.ProcessAilments.Raise(args);
            }

            Assert.IsTrue(!heroWarrior.IsAlive);
        }
    }
}
