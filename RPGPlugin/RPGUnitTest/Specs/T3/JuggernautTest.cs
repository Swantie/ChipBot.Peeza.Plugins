﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class JuggernautTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroJug;

        [TestInitialize]
        public void TestConstr()
        {
            heroJug = Static.CreateDungeonHero("jugg", "human", "juggernaut");
        }

        [TestMethod]
        public void TestJuggernautPassive()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = true,
                Enemies = null,
                Party = new List<Creature>() { heroJug },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroJug,
                Target = null
            };

            var atk = heroJug.Attack;
            heroJug.ProcessAilments.Raise(args);
            Assert.IsTrue(atk == heroJug.Attack, "atk is not the same on full hp");

            heroJug.Health.Value = 1;
            heroJug.ProcessAilments.Raise(args);
            Assert.IsTrue(atk < heroJug.Attack, "atk is not buffed");
        }
    }
}
