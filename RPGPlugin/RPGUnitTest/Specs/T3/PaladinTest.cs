﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T3
{
    [TestClass]
    public class PaladinTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroPal;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroPal = Static.CreateDungeonHero("pal", "elemental", "paladin");
            heroPal.Health.Value = 1;

            heroWarrior = Static.CreateDungeonHero("someRandomGuy", "human", "warrior");

            TestContext.WriteLine($"{heroPal.HeroReference.ToString()} : {heroPal.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestHeal()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroWarrior },
                Party = new List<Creature>() { heroPal },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroPal,
                Target = heroWarrior
            };

            heroPal.ProcessAilments.Raise(args);

            Assert.IsTrue(heroPal.Health.Value > 1);
        }
    }
}
