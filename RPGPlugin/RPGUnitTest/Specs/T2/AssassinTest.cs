﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T2
{
    [TestClass]
    public class AssassinTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroRogue;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroRogue = Static.CreateDungeonHero("dodgeIt", "naga", "assassin");
            heroRogue.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.BlockChance, Value = -1 });
            heroRogue.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.DodgeChance, Value = 1 });
            heroRogue.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Accuracy, Value = 1 });

            heroWarrior = Static.CreateDungeonHero("monkaS", "human", "warrior");
            heroWarrior.Health = new Bar(1);
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.BlockChance, Value = -1 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.DodgeChance, Value = -1 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Accuracy, Value = 1 });

            TestContext.WriteLine($"{heroRogue.HeroReference.ToString()} : {heroRogue.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestEvasionHit()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroRogue },
                Party = new List<Creature>() { heroWarrior },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroWarrior,
                Target = heroRogue
            };

            for (var i = 0; i < 100 && heroWarrior.IsAlive; i++) {
                args.Data = null;
                args.Flag = false;
                heroWarrior.ProcessAilments.Raise(args);
            }

            Assert.IsTrue(!heroWarrior.IsAlive);
        }
    }
}
