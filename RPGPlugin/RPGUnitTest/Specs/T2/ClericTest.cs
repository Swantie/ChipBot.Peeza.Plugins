﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Specs.T2
{
    [TestClass]
    public class ClericTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroCleric;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroCleric = Static.CreateDungeonHero("healer", "elemental", "cleric");
            heroCleric.Health.Value = 1;

            heroWarrior = Static.CreateDungeonHero("someRandomGuy", "human", "warrior");

            TestContext.WriteLine($"{heroCleric.HeroReference.ToString()} : {heroCleric.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestHeal()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroWarrior },
                Party = new List<Creature>() { heroCleric },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroCleric,
                Target = heroWarrior
            };

            heroCleric.ProcessAilments.Raise(args);

            Assert.IsTrue(heroCleric.Health.Value > 1);
        }

        [TestMethod]
        public void TestCooldown()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = false,
                Enemies = new List<Creature>() { heroWarrior },
                Party = new List<Creature>() { heroCleric },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroCleric,
                Target = heroWarrior
            };

            heroCleric.Health.Value = 1;
            heroCleric.ProcessAilments.Raise(args);
            Assert.IsTrue(heroCleric.Health.Value > 1);

            heroCleric.Health.Value = 1;
            heroCleric.ProcessAilments.Raise(args);
            Assert.IsTrue(heroCleric.Health.Value == 1);
        }
    }
}
