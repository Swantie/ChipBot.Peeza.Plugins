﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest
{
    [TestClass]
    public class HeroBasicTest
    {
        private Hero hero;
        private DungeonHero dngHero;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestConstructor()
        {
            hero = new Hero();
            Assert.AreEqual((uint)1, hero.Level, "Hero() level is not 1");
            Assert.IsNotNull(hero.Inventory, "Hero() inv is null");
            Assert.IsNotNull(hero.Equip, "Hero() equip is null");
            Assert.IsNotNull(hero.Statistics, "Hero() statistics is null");
            Assert.IsNotNull(hero.Status, "Hero() status is null");
            Assert.IsNotNull(hero.Status.Party, "Hero() status.party is null");
            Assert.AreEqual(HeroStatusEnum.Idling, hero.Status.Status, "Hero() status.status is not idling");

            Assert.IsTrue(Static.Races.Count > 0, "Races list is not populated");
            Assert.IsTrue(Static.Classes.Count > 0, "Races list is not populated");

            hero.RaceDescription = Static.Races["human"];
            hero.RaceReference = (Race)Activator.CreateInstance(hero.RaceDescription.Race);

            hero.SpecDescription = Static.Classes["WarRioR"];
            hero.SpecReference = (Spec)Activator.CreateInstance(hero.SpecDescription.Spec);

            Assert.IsTrue(hero.Stats.Strength > 2, "Hero() stats");

            dngHero = new DungeonHero(hero);
        }

        [TestMethod]
        public void TestBuffs()
        {
            dngHero.Ailments.Clear();
            dngHero.Buffs.Clear();

            var atk = dngHero.Attack;
            dngHero.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.Attack, Value = 2 });
            Assert.AreEqual(atk + 2, dngHero.Attack, "not amplify buff is not applying");
            dngHero.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 1, Type = BuffType.Attack, Value = 3 });
            Assert.AreEqual((atk + 2) * 3, dngHero.Attack, "amplify buff is not applying");
            dngHero.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.Attack, Value = 2 });
            Assert.AreEqual(((atk + 2) * 3) + 2, dngHero.Attack, "buffs are not stacking properly");
        }

        [TestMethod]
        public void TestAilmentsBuffs()
        {
            dngHero.Ailments.Clear();
            dngHero.Buffs.Clear();

            var atk = dngHero.Attack;
            dngHero.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 1, Type = BuffType.Attack, Value = 2 });
            dngHero.Ailments.Add(new Ailment() { Buffs = new List<Buff>() { new Buff() { Amplify = true, Negative = false, TurnsLeft = 1, Type = BuffType.Attack, Value = 3 } }, Inflicter = dngHero, Power = 1, TurnsLeft = 1, Type = AilmentType.HumanRaceAura });
            Assert.AreEqual((atk + 2) * 3, dngHero.Attack, "ailments buff are not applying");
        }

        [TestMethod]
        public void TestAilments()
        {
            dngHero.Ailments.Clear();
            dngHero.Buffs.Clear();
            dngHero.Health.Value = dngHero.Health.MaxValue;

            dngHero.Ailments.Add(new Ailment() { Buffs = new List<Buff>(), Inflicter = dngHero, Power = 0.1, TurnsLeft = 1, Type = AilmentType.Bleed });
            dngHero.Ailments.Add(new Ailment() { Buffs = new List<Buff>(), Inflicter = dngHero, Power = 1, TurnsLeft = 1, Type = AilmentType.Poison});

            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = true,
                Enemies = null,
                Party = new List<Creature>() { dngHero, dngHero },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = dngHero,
                Target = null
            };

            dngHero.ProcessAilments.Raise(args);


            Assert.IsTrue(dngHero.Health.Value < dngHero.Health.MaxValue);
        }
    }
}
