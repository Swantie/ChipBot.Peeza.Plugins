using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Extensions;
using System;
using System.Collections.Generic;

namespace RPGUnitTest
{
    [TestClass]
    public class CoreTest
    {
        [TestMethod]
        public void TestBar()
        {
            var bar = new Bar(100);
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");
            bar.Value -= 999;
            Assert.AreEqual(0, bar.Percentage, "Empty bar percentage is wrong");
            Assert.AreEqual(0, bar.MinValue, "Bar value is below min value");
            bar.Value += 999;
            Assert.AreEqual(0, bar.MinValue, "Bar value is above max value");

            var bar2 = new Bar(110, 90, 10);
            Assert.AreEqual(0.8, bar2.Percentage, "Bar perc is wrong");
        }

        [TestMethod]
        public void TestBattleEvent()
        {
            var i = 1;
            var b = true;
            var ev = new BattleEvent();

            ev.Event += (BattleEventArgs args) => { i += 1; return false; };
            ev.Event += (BattleEventArgs args) => { i += 2; return b; };
            ev.Event += (BattleEventArgs args) => { i *= 3; return false; };

            ev.Raise(null);

            Assert.AreEqual(5, i, "Battle event is broken");

            i = 1;
            b = false;

            ev.Raise(null);

            Assert.AreEqual(6, i, "Battle event is broken");
        }

        [TestMethod]
        public void TestStats()
        {
            var stats = new Stats(1, 2, 3, 4, 5, 6);
            var stats2 = new Stats(1, 1, 1, 1, 1, 1);
            Assert.AreEqual(21, stats.Sum(), "Stats.Sum() is broken");
            Assert.AreEqual(16, (stats - stats2).Sum(), "Stats - is broken");
            Assert.AreEqual(1, (stats - stats2).Strength, "Stats - is broken");
            Assert.AreEqual(2, (stats - stats2).Intellegence, "Stats - is broken");
            Assert.AreEqual(27, (stats + stats2).Sum(), "Stats + is broken");
            Assert.AreEqual(2, (stats + stats2).Strength, "Stats + is broken");
            Assert.AreNotSame(stats, stats + stats2, "Stats is broken");
        }

        [TestMethod]
        public void TestWeightedRandom()
        {
            var t1 = new WeightTest() { Weight = 100 };
            var t2 = new WeightTest() { Weight = 50 };
            var t3 = new WeightTest() { Weight = 10 };
            var l = new[] { t1, t3, t2 };

            for (var i = 0; i < 10000; i++)
            {
                var t = l.RandomByWeight(t => t.Weight, Static.Random);
                t.Count++;
            }

            Assert.IsTrue(t1.Count > t2.Count);
            Assert.IsTrue(t2.Count > t3.Count);
        }

        private class WeightTest
        {
            public int Weight { get; set; }
            public int Count { get; set; } = 0;
        }
    }
}
