﻿using ChipBot.Core;
using ChipBot.Core.Services;
using RPGPlugin.Core.Classes;
using RPGPlugin.Core.Static.Races;
using RPGPlugin.Core.Static.Specs;
using RPGPlugin.Services;
using System;
using System.Collections.Generic;

namespace RPGUnitTest
{
    public class Static
    {
        public static Dictionary<string, SpecDescription> Classes { get; private set; }
        public static Dictionary<string, RaceDescription> Races { get; private set; }
        public static ConfigurationProvider ConfigurationProvider { get; private set; }

        public static LoggingService Log { get; private set; }
        public static RpgCoreService CoreService { get; private set; }
        public static HeroLevelingService HeroLeveling { get; private set; }
        public static Random Random { get; private set; }

        private class Dummy : Race { }

        static Static()
        {
            Classes = new Dictionary<string, SpecDescription>(StringComparer.InvariantCultureIgnoreCase);
            Races = new Dictionary<string, RaceDescription>(StringComparer.InvariantCultureIgnoreCase);
            RaceDescriptions.PopulateList(Races);
            SpecsDescriptions.PopulateList(Classes);
            Races.Add("dummy", new RaceDescription(RacesEnum.Human, "Dummy race", new Stats(1, 1, 1, 1, 1, 1), typeof(Dummy)));

            ConfigurationProvider = new ConfigurationProvider() { Silent = true };
            Log = new LoggingService(ConfigurationProvider);
            CoreService = new RpgCoreService(Log, ConfigurationProvider);
            HeroLeveling = new HeroLevelingService(CoreService, Log, ConfigurationProvider);
            Random = new Random();
        }

        public static DungeonHero CreateDungeonHero(string name, string race, string spec)
        {
            var r = Races[race];
            var s = Classes[spec];
            return new DungeonHero(new Hero()
            {
                Name = name,
                RaceDescription = r,
                RaceReference = (Race)Activator.CreateInstance(r.Race),
                SpecDescription = s,
                SpecReference = (Spec)Activator.CreateInstance(s.Spec)
            });
        }

        public static DungeonHero CreateDungeonHeroWithItem(Dictionary<ItemType, Item> equip)
        {
            var r = Races["dummy"];
            var s = Classes["warrior"];
            return new DungeonHero(new Hero()
            {
                Name = "hero",
                RaceDescription = r,
                RaceReference = (Race)Activator.CreateInstance(r.Race),
                SpecDescription = s,
                SpecReference = (Spec)Activator.CreateInstance(s.Spec),
                Equip = equip
            });
        }
    }
}
