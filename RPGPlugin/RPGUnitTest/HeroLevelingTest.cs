﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;

namespace RPGUnitTest
{
    [TestClass]
    public class HeroLevelingTest
    {
        [TestMethod]
        public void TestGetExp()
        {
            var hero = new Hero();
            hero.ExperienceNeeded = Static.CoreService.ExpNeeded;

            Static.HeroLeveling.GiveExp(hero, Static.CoreService.ExpNeeded);
            Assert.AreEqual((uint)2, hero.Level, "level is not correct");
            Assert.AreEqual(Static.CoreService.AttributesPerLevel, hero.AttributePoints, "attributes points is not given properly");
        }

        [TestMethod]
        public void TestRefreshLvl()
        {
            var hero = new Hero();
            hero.ExperienceNeeded = Static.CoreService.ExpNeeded;

            hero.Experience = Static.CoreService.ExpNeeded;
            Static.HeroLeveling.RefreshLevel(hero);
            Assert.AreEqual((uint)2, hero.Level, "level is not correct");
            Assert.AreEqual((uint)0, hero.AttributePoints, "attributes points is not given properly");
        }

        [TestMethod]
        public void TestGetTonExp()
        {
            var hero = new Hero();
            hero.ExperienceNeeded = Static.CoreService.ExpNeeded;

            Static.HeroLeveling.GiveExp(hero, ulong.MaxValue);
            Assert.AreEqual(Static.CoreService.MaxLevel, hero.Level, "level is not correct");
        }
    }
}
