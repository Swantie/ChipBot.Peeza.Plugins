﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RPGUnitTest
{
    [TestClass]
    public class BattleTest
    {
        private DungeonHero heroRogue;
        private DungeonHero heroWarrior;

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestConstructor()
        {
            heroRogue = Static.CreateDungeonHero("pepe", "naga", "rogue");
            heroRogue.Health = new Bar(heroRogue.Health.MaxValue * 4);

            heroWarrior = Static.CreateDungeonHero("Clap", "human", "warrior");
            heroWarrior.Health = new Bar(heroWarrior.Health.MaxValue * 4);

            TestContext.WriteLine($"{heroRogue.HeroReference.ToString()} : {heroRogue.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestFight()
        {
            var f = new Fight(new List<Creature>() { heroRogue }, new List<Creature>() { heroWarrior }, (string t) => TestContext.WriteLine(t), Static.Random);
            f.DoFight();
        }
    }
}
