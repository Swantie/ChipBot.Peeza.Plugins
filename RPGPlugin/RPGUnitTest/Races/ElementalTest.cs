﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGUnitTest.Races
{
    [TestClass]
    public class ElementalTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroEl;
        private DungeonHero heroWarrior;
        private DungeonHero heroWarrior2;

        [TestInitialize]
        public void TestConstr()
        {
            heroEl = Static.CreateDungeonHero("healer", "elemental", "cleric");
            heroEl.Health.Value = 1;

            heroWarrior = Static.CreateDungeonHero("someRandomGuy", "dummy", "warrior");
            heroWarrior2 = Static.CreateDungeonHero("someBadGuy", "dummy", "warrior");

            TestContext.WriteLine($"{heroEl.HeroReference.ToString()} : {heroEl.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior2.HeroReference.ToString()} : {heroWarrior2.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestBuffAndOnDeathHeal()
        {
            var args = new BattleEventArgs()
            {
                Data = null,
                Flag = true,
                Enemies = new List<Creature>() { heroWarrior2 },
                Party = new List<Creature>() { heroEl, heroWarrior },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroEl,
                Target = heroWarrior
            };

            heroEl.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroEl.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 2 });
            heroWarrior.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 2 });

            heroWarrior2.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });

            Assert.IsTrue(heroEl.Attack == 2 && heroWarrior.Attack == 2);

            heroEl.ProcessAilments.Raise(args);

            Assert.IsTrue(heroEl.Attack > 2 && heroWarrior.Attack > 2);

            heroWarrior2.Health.Value = 1;
            Assert.IsTrue(heroWarrior2.IsAlive);

            heroWarrior.Health.Value = 1;
            Assert.IsTrue(heroWarrior.Health.Value == 1);

            heroEl.OnDeath.Raise(args);

            Assert.IsTrue(!heroWarrior2.IsAlive);
            Assert.IsTrue(heroWarrior.Health.Value > 1);
        }
    }
}
