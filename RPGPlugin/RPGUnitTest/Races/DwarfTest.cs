﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Races
{
    [TestClass]
    public class DwarfTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroDwarf;

        [TestInitialize]
        public void TestConstr()
        {
            heroDwarf = Static.CreateDungeonHero("dwarf", "dwarf", "rogue");

            TestContext.WriteLine($"{heroDwarf.HeroReference.ToString()} : {heroDwarf.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestDwarfBuff()
        {
            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroDwarf };
            var e = new List<Creature> { };

            heroDwarf.ProcessAilments.Raise(new BattleEventArgs() { Self = heroDwarf, Target = null, Flag = true, Party = p, Enemies = e, Data = null, Log = log, Rng = Static.Random });

            Assert.IsTrue(heroDwarf.DodgeChance <= 0);
            Assert.IsTrue(heroDwarf.CritDamage > 2);
        }
    }
}
