﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Races
{
    [TestClass]
    public class NagaTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroNaga;

        [TestInitialize]
        public void TestConstr()
        {
            heroNaga = Static.CreateDungeonHero("naga", "naga", "rogue");

            TestContext.WriteLine($"{heroNaga.HeroReference.ToString()} : {heroNaga.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestNagaBuff()
        {
            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroNaga };
            var e = new List<Creature> { };

            var dgc = heroNaga.DodgeChance;

            heroNaga.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.Defense, Value = 10 });

            heroNaga.ProcessAilments.Raise(new BattleEventArgs() { Self = heroNaga, Target = null, Flag = true, Party = p, Enemies = e, Data = null, Log = log, Rng = Static.Random });

            Assert.IsTrue(heroNaga.Defense == 0);
            Assert.IsTrue(heroNaga.DodgeChance > dgc);
        }
    }
}
