﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System.Collections.Generic;

namespace RPGUnitTest.Races
{
    [TestClass]
    public class UndeadTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroUn;

        [TestInitialize]
        public void TestConstr()
        {
            heroUn = Static.CreateDungeonHero("zombie", "undead", "warrior");
            heroUn.Health.Value = 1;
            heroUn.Shield.Value = 0;

            TestContext.WriteLine($"{heroUn.HeroReference.ToString()} : {heroUn.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestUndeadHeal()
        {
            var args = new BattleEventArgs()
            {
                Data = (long)10,
                Flag = false,
                Enemies = new List<Creature>() { },
                Party = new List<Creature>() { heroUn },
                Log = (s) => TestContext.WriteLine(s),
                Rng = Static.Random,
                Self = heroUn,
                Target = heroUn
            };

            Assert.IsTrue(heroUn.Health.Value == 1 && heroUn.Shield.Value == 0);

            heroUn.OnGetHeal.Raise(args);

            Assert.IsTrue(heroUn.Health.Value == 1 && heroUn.Shield.Value == 0);

            args.Flag = true;

            heroUn.OnGetHeal.Raise(args);

            Assert.IsTrue(heroUn.Health.Value == 1 && heroUn.Shield.Value > 0);
        }
    }
}
