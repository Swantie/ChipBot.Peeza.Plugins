﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Races
{
    [TestClass]
    public class ElfTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero heroAssassin;
        private DungeonHero heroWarrior;

        [TestInitialize]
        public void TestConstr()
        {
            heroAssassin = Static.CreateDungeonHero("elf", "elf", "cultassassin");
            heroWarrior = Static.CreateDungeonHero("monkaS", "dummy", "warrior");

            TestContext.WriteLine($"{heroAssassin.HeroReference.ToString()} : {heroAssassin.HeroReference.Stats.ToStringShort(" ")}");
            TestContext.WriteLine($"{heroWarrior.HeroReference.ToString()} : {heroWarrior.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestOnCritEffect()
        {
            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { heroAssassin };
            var e = new List<Creature> { heroWarrior };

            heroAssassin.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroAssassin.Buffs.Add(new Buff() { Amplify = false, Negative = false, TurnsLeft = 99999, Type = BuffType.CritChance, Value = 2 });

            heroWarrior.Buffs.Add(new Buff() { Amplify = true, Negative = false, TurnsLeft = 99999, Type = BuffType.Attack, Value = 0 });
            heroWarrior.Health.Value = 1;
            heroWarrior.IsAlive = true;

            heroAssassin.Health.Value = 1;

            heroAssassin.OnDungeonStart.Raise(new BattleEventArgs { Data = null, Flag = false, Rng = Static.Random, Enemies = e, Party = p, Log = log, Self = heroAssassin, Target = heroWarrior });
            var f = new Fight(p, e, (string t) => TestContext.WriteLine(t), Static.Random);
            f.DoFight();

            Assert.IsTrue(!heroWarrior.IsAlive);
        }
    }
}
