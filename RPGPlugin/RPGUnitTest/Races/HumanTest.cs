﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGPlugin.Core.Classes;
using System;
using System.Collections.Generic;

namespace RPGUnitTest.Races
{
    [TestClass]
    public class HumanTest
    {
        public TestContext TestContext { get; set; }
        private DungeonHero herohm;

        [TestInitialize]
        public void TestConstr()
        {
            herohm = Static.CreateDungeonHero("ivan", "human", "rogue");

            TestContext.WriteLine($"{herohm.HeroReference.ToString()} : {herohm.HeroReference.Stats.ToStringShort(" ")}");
        }

        [TestMethod]
        public void TestHumanBuff()
        {
            Action<string> log = (string t) => TestContext.WriteLine(t);
            var p = new List<Creature> { herohm };
            var e = new List<Creature> { };

            var acc = herohm.Accuracy;
            var atk = herohm.Attack;
            var def = herohm.Defense;
            var crc = herohm.CritChance;
            var dgc = herohm.DodgeChance;

            herohm.OnFightStart.Raise(new BattleEventArgs() { Self = herohm, Target = null, Flag = true, Party = p, Enemies = e, Data = null, Log = log, Rng = Static.Random });
            herohm.ProcessAilments.Raise(new BattleEventArgs() { Self = herohm, Target = null, Flag = true, Party = p, Enemies = e, Data = null, Log = log, Rng = Static.Random });

            Assert.IsTrue(herohm.Accuracy > acc);
            Assert.IsTrue(
                herohm.Attack > atk ||
                herohm.Defense > def ||
                herohm.CritChance > crc ||
                herohm.DodgeChance > dgc);
        }
    }
}
