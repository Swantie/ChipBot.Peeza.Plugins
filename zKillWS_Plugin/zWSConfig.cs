﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace zKillWS_Plugin
{
    public class ZWSConfig
    {
        [JsonProperty("uri")]
        [JsonRequired]
        public string Uri { get; set; } = "wss://zkillboard.com:2096";

        [JsonProperty("subs")]
        public List<string> Subs { get; set; } = new List<string>();
    }
}
