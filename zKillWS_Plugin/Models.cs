﻿using Newtonsoft.Json;
using System;

namespace zKillWS_Plugin
{
    public partial class TqStatus
    {
        private string status;

        [JsonProperty("tqStatus")]
        public string Status { get { return status.Substring(0, 1).ToUpper() + status.Substring(1).ToLower(); } set { status = value; } }

        [JsonProperty("tqCount")]
        public string Online { get; set; } = "Unkown";

        [JsonProperty("kills")]
        public string Kills { get; set; } = "Unkown";
    }

    public partial class BigKill
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("iskStr")]
        public string IskStr { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }
    }

    public partial class SmallKill
    {
        [JsonProperty("killID")]
        public long KillId { get; set; }

        [JsonProperty("character_id")]
        public long CharacterId { get; set; }

        [JsonProperty("corporation_id")]
        public long CorporationId { get; set; }

        [JsonProperty("alliance_id")]
        public long AllianceId { get; set; }

        [JsonProperty("ship_type_id")]
        public long ShipTypeId { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }
    }
}
