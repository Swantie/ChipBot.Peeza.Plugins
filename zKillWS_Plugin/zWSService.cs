﻿using ChipBot.Core;
using ChipBot.Core.Services;
using Discord;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Websocket.Client;

namespace zKillWS_Plugin.Services
{
    public class ZWSService : BackgroundService, IConfigurableService<ZWSConfig>
    {
        public override bool IsWorking => wsClient != null;
        public ServiceConfigurationProvider<ZWSConfig> ServiceConfigProvider { get; set; }

        private readonly MemoryCache _cache;

        private WebsocketClient wsClient;
        private TqStatus tqStatus;

        public event Func<TqStatus, Task> TqStatusChanged;
        public event Func<SmallKill, Task> SmallKillReceived;
        public event Func<BigKill, Task> BigKillReceived;

        public ZWSService(ConfigurationProvider provider, LoggingService log, MemoryCache cache) : base(log, provider)
        {
            _cache = cache;
            ServiceConfigProvider = ServiceConfigurationProvider<ZWSConfig>.GetInstance(provider);
        }

        public override async Task StartAsync()
        {
            Log(LogSeverity.Verbose, $"Starting");

            if (wsClient != null) await StopAsync();

            wsClient = new WebsocketClient(new Uri(ServiceConfigProvider.Config.Uri));

            wsClient.ReconnectionHappened.Subscribe(async type => { Log(LogSeverity.Info, $"Reconnection happened, type: {type}"); await SubscribeAll(); });
            wsClient.DisconnectionHappened.Subscribe(type => Log(LogSeverity.Info, $"Disconnection happened, type: {type}"));
            wsClient.MessageReceived.Subscribe(msg => Log(LogSeverity.Info, $"Message received: {msg}"));
            wsClient.MessageReceived.Where(msg => msg.StartsWith("{")).Subscribe(ParseMessage);

            await wsClient.Start();
        }

        public override async Task StopAsync()
        {
            Log(LogSeverity.Verbose, $"Stopping");
            wsClient.Dispose();
            wsClient = null;
            tqStatus = null;
            await Task.CompletedTask;
        }

        public async Task Subscribe(string channel, bool saveToSettings = true)
        {
            if (saveToSettings)
            {
                ServiceConfigProvider.Config.Subs.Add(channel);
                ServiceConfigProvider.Save();
            }

            await SendMessage($"{{\"action\":\"sub\",\"channel\":\"{channel}\"}}");
        }

        public TqStatus GetStatus()
        {
            return tqStatus;
        }

        private async Task SendMessage(string message)
        {
            if (wsClient != null && wsClient.IsRunning)
            {
                Log(LogSeverity.Verbose, $"Sending Message: {message}");
                await wsClient.Send(message);
            }
        }

        private async Task SubscribeAll()
        {
            foreach (var sub in ServiceConfigProvider.Config.Subs)
            {
                await Subscribe(sub, false);
            }
        }

        private void ParseMessage(string message)
        {
            try
            {
                var data = JsonConvert.DeserializeObject<JToken>(message);
                if (!data.HasValues) return;

                var action = data.Value<string>("action");

                if (string.IsNullOrEmpty(action)) return;

                Log(LogSeverity.Verbose, $"Got {action}");

                switch (action)
                {
                    case "tqStatus":
                        Log(LogSeverity.Debug, "Invoking tqStatus");
                        tqStatus = data.ToObject<TqStatus>();
                        TqStatusChanged?.Invoke(tqStatus);
                        break;
                    case "littlekill":
                        if (data["killID"] == null) return;

                        var id = data["killID"].ToObject<uint>();
                        var cached = _cache.Get($"zk{id}");
                        if (cached != null)
                        {
                            Log(LogSeverity.Info, $"Already invoked littlekill id:{id}");
                            return;
                        }

                        _cache.Set($"zk{id}", id, DateTimeOffset.Now.AddMinutes(1));

                        Log(LogSeverity.Debug, $"Invoking littlekill id:{id}");
                        SmallKillReceived?.Invoke(data.ToObject<SmallKill>());
                        break;
                    case "bigKill":
                        Log(LogSeverity.Debug, "Invoking bigKill");
                        BigKillReceived?.Invoke(data.ToObject<BigKill>());
                        break;
                }
            }
            catch (Exception ex)
            {
                Log(LogSeverity.Error, $"Error: {ex.Message} while processing {message}");
            }
        }
    }
}
